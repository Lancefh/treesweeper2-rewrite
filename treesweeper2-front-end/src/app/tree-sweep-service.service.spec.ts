import { TestBed } from '@angular/core/testing';

import { TreeSweepService } from './tree-sweep-service.service';

describe('TreeSweepServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TreeSweepService = TestBed.get(TreeSweepService);
    expect(service).toBeTruthy();
  });
});
