import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LastSweepSummaryComponent } from './last-sweep-summary.component';

describe('LastSweepSummaryComponent', () => {
  let component: LastSweepSummaryComponent;
  let fixture: ComponentFixture<LastSweepSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LastSweepSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LastSweepSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
