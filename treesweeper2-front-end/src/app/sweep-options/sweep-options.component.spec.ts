import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SweepOptionsComponent } from './sweep-options.component';

describe('SweepOptionsComponent', () => {
  let component: SweepOptionsComponent;
  let fixture: ComponentFixture<SweepOptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SweepOptionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SweepOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
