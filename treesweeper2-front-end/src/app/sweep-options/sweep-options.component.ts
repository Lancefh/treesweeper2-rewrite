import { Component, OnInit } from '@angular/core';
import { ConstraintCategory } from '../constraintCategory';
import { ImportOptions } from '../importOptions';
import { FsService } from '../fs.service';
import { DataStoreService } from '../data-store.service';

@Component({
  selector: 'app-sweep-options',
  templateUrl: './sweep-options.component.html',
  styleUrls: ['./sweep-options.component.scss']
})
export class SweepOptionsComponent implements OnInit {
  public generationOptions: Array<string> = ["1", "2", "3", "4", "5", "6", "7"];
  public numGenerations: string = "3";
  //This may need to be changed to its new name at some point to make the code more readable
  public constraintGroup: string = "No Research";
  public isCustom: boolean = false;
  public constraintCategories: Array<ConstraintCategory>;

  constructor(public fsService: FsService, public dataService: DataStoreService) { }

  ngOnInit() {
    // This would call a static method if I wanted to take the time to figure it out.
    this.constraintCategories = new ConstraintCategory("").loadDefaults();
    this.selectAllButResearch();
  }

  // Starts a new tree sweep using the options currently specified
  public startSweep(): void {
    // Generate constraint options object
    let importOptions = new ImportOptions();
    importOptions.constraintGroup = this.constraintGroup;
    importOptions.numGenerations = parseInt(this.numGenerations);
    if(this.dataService.useCurrentUser){
      // Use current user's pid as root
      importOptions.rootPid = this.fsService.fsSession.fs_user.pid;
    } else {
      // Use manual pid as root
      importOptions.rootPid = this.dataService.rootPid.trim();;
    }
    importOptions.token = this.fsService.fsSession.fs_access_token;

    // Determine which constraints are disabled
    let disabledConstraints: Array<string> = [];
    this.constraintCategories.forEach(
      function(category){
        category.constraints.forEach(
          function(constraint){
            if(!constraint.isEnabled()) disabledConstraints.push(constraint.getId());
          }
        );
      }
    );
    importOptions.setDisabledConstraints(disabledConstraints);

    // Start sweep
    this.dataService.newSweep(importOptions);
  }

  // Returns a description of the current constraint category
  public getDescription(): string {
    switch(this.constraintGroup){
      case "All": return "Running all constraints gives you the best results, but also takes the longest.";
      case "Impossible": return "Running impossible constraints tells you only the worst errors. If you get results, something is wrong.";
      case "Research": return "Running research constraints shows you places that could use some more research. There might be nothing wrong, but it's worth looking into.";
      case "Custom": return "Running custom constraints lets you check only what matters to you.";
      case "No Research": return "Running no research constraints will check only the things that might be wrong.";
      default: return "";
    }
  }

  // Enables all constraints
  public enableConstraints(): void {
    this.constraintCategories.forEach(
      function(category){
        category.constraints.forEach(
          function(constraint){ constraint.setEnabled(true); }
        );
      }
    );
  }

  // Disables all constraints
  public disableConstraints(): void {
    this.constraintCategories.forEach(
      function(category){
        category.constraints.forEach(
          function(constraint){ constraint.setEnabled(false); }
        );
      }
    );
  }

  // Enables a specific constraint
  public enableConstraint(categoryName: string, constraintName: string): void {
    this.constraintCategories.forEach(
      function(category){
        if(category.name === categoryName) category.constraints.forEach(
          function(constraint){
            if(constraint.getId() === constraintName) constraint.setEnabled(true);
          }
        );
      }
    );
  }

  // Disables a specific constraint
  public disableConstraint(categoryName: string, constraintName: string): void {
    this.constraintCategories.forEach(
      function(category){
        if(category.name === categoryName) category.constraints.forEach(
          function(constraint){
            if(constraint.getId() === constraintName) constraint.setEnabled(false);
          }
        );
      }
    );
  }

  // Enables all constraints and sets the constraint group to "All"
  public selectAllConstraints(): void {
    this.constraintGroup = "All";
    this.isCustom = false;
    this.enableConstraints();
  }

  public selectAllButResearch(): void {
    this.constraintGroup = "No Research";
    this.isCustom = false;
    this.enableConstraints();

    // Disable research constraints
    this.disableConstraint("Research", "Person has sources");
    // this.disableConstraint("Research", "Person is not badly merged");
    this.disableConstraint("Research", "Couple is not missing children at beginning of their marriage");
    this.disableConstraint("Research", "Couple is not missing children at the end of mothers child bearing years");
    this.disableConstraint("Research", "Couple is not missing middle children");
    this.disableConstraint("Research", "Person is not missing a spouse");
    this.disableConstraint("Research", "Person has a known gender");
  }

  // Enables only impossibility constraints and sets the constraint group to "Impossible"
  public selectImpossibleConstraints(): void {
    this.constraintGroup = "Impossible";
    this.isCustom = false;
    this.disableConstraints();

    // Enable constraints that return impossibilities
    this.enableConstraint("Christening", "Person was christened after they were born");
    this.enableConstraint("Christening", "Person was christened before they died");
    this.enableConstraint("Birth", "Parent is born before their children");
    this.enableConstraint("Birth", "Parent dies after children are born");
    this.enableConstraint("Marriage", "Person was alive at marriage");
    this.enableConstraint("Marriage", "Person was born at marriage");
    this.enableConstraint("Research", "Person is not their own ancestor");
    this.enableConstraint("Lifespan", "Person died after they were born");
  }

  // Enables only research constraints and sets the constraint group to "Research"
  public selectResearchConstraints(): void {
    this.constraintGroup = "Research";
    this.isCustom = false;
    this.disableConstraints();

    // Enable constraints that return research opportunities
    this.enableConstraint("Research", "Person has sources");
    // this.enableConstraint("Research", "Person is not badly merged");
    this.enableConstraint("Research", "Person is not their own ancestor");
    this.enableConstraint("Research", "Couple is not missing children at beginning of their marriage");
    this.enableConstraint("Research", "Couple is not missing children at the end of mothers child bearing years");
    this.enableConstraint("Research", "Couple is not missing middle children");
    this.enableConstraint("Research", "Person is not missing a spouse");
    this.enableConstraint("Research", "Person has a known gender");
  }

  // Disables all constraints, sets the constraint group to "Custom", and exposes constraints for manual selection
  public selectCustomConstraints(): void {
    this.constraintGroup = "Custom";
    this.isCustom = true;
    this.disableConstraints();
  }
}
