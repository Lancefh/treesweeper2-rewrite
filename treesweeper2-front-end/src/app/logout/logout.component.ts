import { Component, OnInit } from '@angular/core';
import { FsService } from '../fs.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit {

  constructor(private fsService: FsService) {
    this.fsService.logOut();
  }

  ngOnInit() {
  }

}
