import { Constraint } from './constraint';

export class ConstraintCategory {
    public name: string;
    public constraints: Array<Constraint>;

    constructor(name: string){
        this.name = name;
        this.constraints = [];
    }

    public add(id: string): void {
        let constraint: Constraint = new Constraint(id);
        this.constraints.push(constraint);
    }

    // This method loads with all the currently-defined constraints.
    public loadDefaults(): Array<ConstraintCategory> {
        let constraintCategories: Array<ConstraintCategory> = [];

        // Initialize constraint categories
        let category: ConstraintCategory = new ConstraintCategory("Birth");
        category.add("Father has realistic age at birth of first child");
        category.add("Father has realistic age at birth of last child");
        category.add("Mother has realistic age at birth of first child");
        category.add("Mother has realistic age at birth of last child");
        category.add("Parent is born before their children");
        category.add("Parent dies after children are born");
        constraintCategories.push(category);

        category = new ConstraintCategory("Christening");
        category.add("Person was christened after they were born");
        category.add("Person was christened before they died");
        category.add("Person was christened at a reasonable age");
        constraintCategories.push(category);

        category = new ConstraintCategory("Lifespan");
        category.add("Person died after they were born");
        category.add("Woman has a reasonable lifespan");
        category.add("Man has a reasonable lifespan");
        constraintCategories.push(category);

        category = new ConstraintCategory("Marriage");
        category.add("Husband has realistic age at marriage");
        category.add("Wife has realistic age at marriage");
        category.add("Person was alive at marriage");
        category.add("Person was born at marriage");
        category.add("Couple has been married a reasonable amount of time before first child");
        category.add("Couple has been married a reasonable amount of time before last child");
        constraintCategories.push(category);

        category = new ConstraintCategory("Research");
        category.add("Person has sources");
        // category.add("Person is not badly merged");
        category.add("Person is not their own ancestor");
        category.add("Couple is not missing children at beginning of their marriage");
        category.add("Couple is not missing children at the end of mothers child bearing years");
        category.add("Couple is not missing middle children");
        category.add("Person is not missing a spouse");
        category.add("Person has a known gender");
        constraintCategories.push(category);

        return constraintCategories;
    }
}