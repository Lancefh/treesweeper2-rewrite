import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { LoaderComponent } from './loader/loader.component';
import { Overlay } from '@angular/cdk/overlay';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  constructor(private dialog: MatDialog, private overlay: Overlay) { }

  displayLoader(message?: string, autoclose = true, duration = 2000): MatDialogRef<LoaderComponent, any> {
    let dialogRef = this.dialog.open(LoaderComponent, {
      data: { message: message },
      minHeight: '80px',
      minWidth: '230px',
      scrollStrategy: this.overlay.scrollStrategies.block()
    });

    if(autoclose) {
      setTimeout(() => {
        dialogRef.close();
      }, duration)
    } else {
      return dialogRef; 
    }
  }
}
