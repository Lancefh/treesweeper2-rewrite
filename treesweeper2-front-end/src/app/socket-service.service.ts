import { Injectable } from '@angular/core';
import { ImportOptions } from './importOptions';
import { Status } from './status';
import { TreeSweepSocket } from './socket';
import { Update } from './updates';
import { DataStoreService } from './data-store.service';

@Injectable({
  providedIn: 'root'
})
// This service creates and manages the socket to the server
export class SocketServiceService {
  public status:Status;
  private socket:TreeSweepSocket

  constructor(private dataService:DataStoreService) { }

  private setLoading(value: boolean){ 
    this.status.loading = value; 
    this.status.mode = "determinate";
  }
  private setWaiting(value: boolean){ 
    if(value) this.status.mode = "buffer";
    else this.status.mode = "determinate";
  }

  public startSweep(options: ImportOptions): void {
    // Start sweep!
    console.log("SocketService: starting sweep!");
    this.status = new Status(100);
    this.status.setValue(0);
    this.status.loading = true;

    // Open socket connection
    // this.socket = new WebSocket("ws://" + window.location.host + "/import");
    this.socket = new TreeSweepSocket(null);
  }

  public handleUpdate(update:Update){
    console.log(JSON.stringify(update,null,2));
  }
}
