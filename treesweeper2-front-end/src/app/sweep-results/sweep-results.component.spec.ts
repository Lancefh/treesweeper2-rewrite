import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SweepResultsComponent } from './sweep-results.component';

describe('SweepResultsComponent', () => {
  let component: SweepResultsComponent;
  let fixture: ComponentFixture<SweepResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SweepResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SweepResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
