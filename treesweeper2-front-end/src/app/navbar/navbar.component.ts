import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LinkService } from '../link.service';
import { FsService } from '../fs.service';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  public title: string;

  constructor(
    private fsService: FsService, 
    private router: Router, 
    public link: LinkService,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer) 
  {
    this.title = link.APP_NAME; //Set the title for the page
    this.matIconRegistry.addSvgIcon(
      "tree_sweeper_logo", this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/treesweeper.svg")
    );
  }

  ngOnInit() {
    let loggedIn = this.fsService.checkLoginStatus();

    if (loggedIn) {
      this.goHome();
    }
  }

  goHome() {
    this.router.navigate([this.link.getHome()]);
  }

  goToRoute(link: string) {
    if (link.includes('http')) {
      window.open(link, '_blank');
    } else if (link === 'home') {
      this.goHome();
    } else {
      this.router.navigate([link]);
    }
  }

}
