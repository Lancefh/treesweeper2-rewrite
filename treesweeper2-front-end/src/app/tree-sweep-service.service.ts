import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { ImportOptions } from './importOptions';
import { Update, ResultUpdate } from './updates';
import { DataStoreService } from './data-store.service';
import { FlaggedResult } from './flaggedResult';
import { interval, Subscription } from 'rxjs';

var uuidv1 = require('uuid/v1');

@Injectable({
  providedIn: 'root'
})
export class TreeSweepService {
  private request;
  private sub;
  constructor(private http: HttpClient) { }
  //This function sends the back-end the importOptions object with the token, number
  //of generations, etc
  public sendName(url: string, options: ImportOptions, dataService: DataStoreService){
      options.uuid = uuidv1();
      console.log(options.uuid);
      let header = { headers: new HttpHeaders()
        .set("Authorization", "Bearer " + options.token)
        .set("options", JSON.stringify(options)) }
      this.request = this.http.get(url, header);
      this.request.subscribe((message:any) => {
        try {
          if (message) {
              console.log("data was returned", JSON.stringify(message));
          }
          let resultsList = message;
          try {
            resultsList.forEach((result) => {
            console.log(result);
            let update:Update = Update.typeOfUpdate(result);
            dataService.handleUpdate(update);
              })
          }
          catch(err) {
                // Something went wrong while deserializing and classifying the object
                console.error(err);
            }
          }
          catch(err) {
              // Not an object
              console.log(err);
          } (error) => {
        console.log("An Error happened");
        console.log(error);
      }
  });
  this.updateCall(750, options.uuid);
}

  public updateCall(timer, uuid){
    var count = 0
    this.sub = interval(timer).subscribe((x)=>{
      if(count == 3) {
        this.sub.unsubscribe();
      }
      else {
        count++;
      }
      console.log("ping");
      let header = { headers: new HttpHeaders()
      .set("count", count.toString())
      .set("uuid", uuid.toString())
      };
      let req = this.http.get('http://localhost:8080/update', header);
      req.subscribe((data:any) => {
      console.log(data);
      return data;
    })
    })
  }
}
