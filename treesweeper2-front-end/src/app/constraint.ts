export class Constraint {
    public id: string;
    public enabled: boolean;

    constructor(id: string){
        this.id = id;
        this.enabled = true;
    }

    public isEnabled(): boolean {
        return this.enabled;
    }

    public setEnabled(enabled: boolean): void {
        this.enabled = enabled;
    }

    public getId(): string {
        return this.id;
    }
}