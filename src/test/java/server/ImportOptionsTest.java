package server;

import org.junit.Test;

import static org.junit.Assert.*;

public class ImportOptionsTest {

    /**
     * Ensures that toString() works correctly
     */
    @Test
    public void toStringTest() {
        ImportOptions test = new ImportOptions();
        String s = test.toString();
        assertTrue(s.equals("{ constraints:[], numGenerations: -1, authorization: null, pid: null}"));
    }
}