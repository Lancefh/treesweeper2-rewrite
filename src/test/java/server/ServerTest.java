package server;

import dummy.TestClient;
import injector.DependencyInjector;
import org.eclipse.jetty.util.log.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import serialization.Serializer;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

import static org.junit.Assert.*;

public class ServerTest {
    /**
     * Starts up an instance of the server on port 1342, with logging disabled.
     */
    @BeforeClass
    public static void startupServer() throws Exception{
        // Disable logging output
        org.eclipse.jetty.util.log.Log.setLog(new Logger() {
            @Override
            public String getName() { return "no"; }

            @Override
            public void warn(String s, Object... objects) {}

            @Override
            public void warn(Throwable throwable) {}

            @Override
            public void warn(String s, Throwable throwable) {}

            @Override
            public void info(String s, Object... objects) {}

            @Override
            public void info(Throwable throwable) {}

            @Override
            public void info(String s, Throwable throwable) {}

            @Override
            public boolean isDebugEnabled() {return false;}

            @Override
            public void setDebugEnabled(boolean b) {}

            @Override
            public void debug(String s, Object... objects) {}

            @Override
            public void debug(String s, long l) {}

            @Override
            public void debug(Throwable throwable) {}

            @Override
            public void debug(String s, Throwable throwable) {}

            @Override
            public Logger getLogger(String s) { return this; }

            @Override
            public void ignore(Throwable throwable) {}
        });

        // Start the server on port 1342
        ServerCommunicator.setPortNumber(1342);
        assertTrue(ServerCommunicator.getPortNumber() == 1342);
        ServerCommunicator.main(new String[]{"1342", "-testing"});

        // enable testing mode
        DependencyInjector.get().setProductionMode(false);
    }

    /**
     * Shuts down the server that's running.
     */
    @AfterClass
    public static void shutdownServer() throws Exception{
        // disable testing mode
        DependencyInjector.get().setProductionMode(true);
        assertTrue(DependencyInjector.get().isProductionMode());

        // Shut down the server
        ServerCommunicator.shutdown();
    }

    /**
     * Tests that the default servlet is working.
     */
    @Test
    public void defaultTest() throws Exception{
        URL test = new URL("http://localhost:1342/");
        HttpURLConnection connection = (HttpURLConnection)test.openConnection();
        connection.connect();
        assertTrue(connection.getResponseCode() == 200);
        InputStream content = connection.getInputStream();
        Scanner scanner = new Scanner(content).useDelimiter("\\A");
        String response = scanner.hasNext() ? scanner.next() : null;
        assertNotNull(response);

        test = new URL("http://localhost:1342/home");
        connection = (HttpURLConnection)test.openConnection();
        connection.connect();
        assertTrue(connection.getResponseCode() == 200);
        content = connection.getInputStream();
        scanner = new Scanner(content).useDelimiter("\\A");
        response = scanner.hasNext() ? scanner.next() : null;
        assertNotNull(response);

        test = new URL("http://localhost:1342/help");
        connection = (HttpURLConnection)test.openConnection();
        connection.connect();
        assertTrue(connection.getResponseCode() == 200);
        content = connection.getInputStream();
        scanner = new Scanner(content).useDelimiter("\\A");
        response = scanner.hasNext() ? scanner.next() : null;
        assertNotNull(response);
    }

    /**
     * Ensures that stop() doesn't break when the server isn't running
     */
    @Test
    public void notRunningShutdown() throws Exception{
        JettyServer test = new JettyServer();
        test.stop();
    }

    /**
     * Ensures that a 404 response is returned after a request for an unknown page
     */
    @Test
    public void testing404() throws Exception{
        URL test = new URL("http://localhost:1342/pig");
        HttpURLConnection connection = (HttpURLConnection)test.openConnection();
        connection.connect();
        assertTrue(connection.getResponseCode() == 404);
    }

    /**
     * Tests that a connection can be established.
     */
    @Test
    public void socketTest() throws Exception{
        TestClient testClient = new TestClient();
        testClient.connect("ws://localhost:1342/import");
        assertTrue(testClient.isConnected());

        testClient.send("hello world");
        testClient.send("");
        testClient.send(Serializer.get().serialize(generateOptions("pid")));
        testClient.send(Serializer.get().serialize(generateOptions("broken_import")));

        pause(1000);
        testClient.terminate();
    }

    /**
     * Tests that a bad import can be handled by the server.
     */
    @Test
    public void badImportTest() throws Exception{
        TestClient testClient = new TestClient();
        testClient.connect("ws://localhost:1342/import");
        assertTrue(testClient.isConnected());

        testClient.send(Serializer.get().serialize(generateOptions("broken_import")));

        pause(1000);
        testClient.terminate();
    }

    /**
     * Ensures that the server can handle a bad model file
\     */
    @Test
    public void badModelTest() throws Exception{
        TestClient testClient = new TestClient();
        testClient.connect("ws://localhost:1342/import");
        assertTrue(testClient.isConnected());

        ImporterSocket.setModelFile("monkey.txt");
        testClient.send(Serializer.get().serialize(generateOptions("pid")));

        pause(1000);
        testClient.terminate();
        ImporterSocket.resetModelFile();
    }

    /**
     * Ensures that the server can handle a bad configuration file
     */
    @Test
    public void badConfigTest() throws Exception{
        TestClient testClient = new TestClient();
        testClient.connect("ws://localhost:1342/import");
        assertTrue(testClient.isConnected());

        ImporterSocket.setConfigurationFile("monkey.txt");
        testClient.send(Serializer.get().serialize(generateOptions("pid")));

        pause(1000);
        testClient.terminate();
        ImporterSocket.resetConfigurationFile();
    }

    /**
     * Ensures that the server can handle a missing plugin
     */
    @Test
    public void badPluginTest() throws Exception{
        TestClient testClient = new TestClient();
        testClient.connect("ws://localhost:1342/import");
        assertTrue(testClient.isConnected());

        testClient.send(Serializer.get().serialize(generateOptions("bad_plugin")));

        pause(1000);
        testClient.terminate();
    }

    /**
     * Ensures that the server can handle it when an ancestry is missing
     */
    @Test
    public void missingAncestryTest() throws Exception{
        TestClient testClient = new TestClient();
        testClient.connect("ws://localhost:1342/import");
        assertTrue(testClient.isConnected());

        testClient.send(Serializer.get().serialize(generateOptions("missing_ancestry")));

        pause(1000);
        testClient.terminate();
    }

    /**
     * Ensures that the server can handle an invalid distribution
     */
    @Test
    public void badDistribution() throws Exception{
        TestClient testClient = new TestClient();
        testClient.connect("ws://localhost:1342/import");
        assertTrue(testClient.isConnected());

        testClient.send(Serializer.get().serialize(generateOptions("bad_distribution")));

        pause(1000);
        testClient.terminate();
    }

    /**
     * Ensures that the server can handle it when a connection is lost and an error is encountered.
     */
    @Test
    public void brokenDifficulties() throws Exception{
        TestClient testClient = new TestClient();
        testClient.connect("ws://localhost:1342/import");
        assertTrue(testClient.isConnected());

        ImporterSocket.setConfigurationFile("monkey.txt");
        TestWrapper.enableFail();
        testClient.send(Serializer.get().serialize(generateOptions("pid")));

        pause(1000);
        testClient.terminate();
        ImporterSocket.resetConfigurationFile();
        TestWrapper.disableFail();
    }

    /**
     * Ensures that the server can handle it when an ancestry is missing and the connection is lost.
     */
    @Test
    public void brokenMissingAncestry() throws Exception{
        TestClient testClient = new TestClient();
        testClient.connect("ws://localhost:1342/import");
        assertTrue(testClient.isConnected());

        TestWrapper.enableFail();
        testClient.send(Serializer.get().serialize(generateOptions("missing_ancestry")));

        pause(1000);
        testClient.terminate();
        TestWrapper.disableFail();
    }

    /**
     * Ensures that the server can handle it when trying to send a message to the client but the connection is lost.
     */
    @Test
    public void brokenMessageUpdate() throws Exception{
        TestClient testClient = new TestClient();
        testClient.connect("ws://localhost:1342/import");
        assertTrue(testClient.isConnected());

        TestWrapper.enableFail();
        testClient.send(Serializer.get().serialize(generateOptions("pid")));

        pause(1000);
        testClient.terminate();
        TestWrapper.disableFail();
    }

    /**
     * Ensures that the server can handle it when trying to send a message and the errors all fail to get through.
     */
    @Test
    public void brokenBadMessage() throws Exception{
        TestClient testClient = new TestClient();
        testClient.connect("ws://localhost:1342/import");
        assertTrue(testClient.isConnected());

        TestWrapper.enableFail();
        testClient.send("trigger");

        pause(1000);
        testClient.terminate();
        TestWrapper.disableFail();
    }

    /**
     * Ensures that the server can handle it when an unexpected error occurs.
     */
    @Test
    public void miscellaneousError() throws Exception{
        TestClient testClient = new TestClient();
        testClient.connect("ws://localhost:1342/import");
        assertTrue(testClient.isConnected());

        testClient.send(Serializer.get().serialize(generateOptions("null_error")));

        pause(1000);
        testClient.terminate();
    }

    /**
     * Ensures that the server can handle it when a constraint throws an error.
     */
    @Test
    public void constraintError() throws Exception{
        TestClient testClient = new TestClient();
        testClient.connect("ws://localhost:1342/import");
        assertTrue(testClient.isConnected());

        testClient.send(Serializer.get().serialize(generateOptions("constraint_exception")));

        pause(1000);
        testClient.terminate();
    }

    /**
     * Pauses for a moment without making the thread sleep.
     * Usually used to wait for a command to finish.
     */
    private void pause(long duration){
        long endTime = System.currentTimeMillis() + duration;
        while(System.currentTimeMillis() < endTime){
            // wait for it...
        }
    }

    /**
     * Generates options for testing purposes.
     */
    private ImportOptions generateOptions(String pid){
        ImportOptions options = new ImportOptions();
        options.setPid(pid);
        options.setNumGenerations(7);
        options.setAuthorization(null);
        options.setConstraints(new String[]{});
        return options;
    }
}
