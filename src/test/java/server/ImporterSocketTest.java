package server;

import checkerImplementations.handlers.results.FlaggedResult;
import injector.DependencyInjector;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class ImporterSocketTest {

    /**
     * Ensures that onBinary() doesn't break.
     */
    @Test
    public void onBinary() throws Exception{
        new ImporterSocket().onWebSocketBinary(null, 0, 0);
    }

    /**
     * Ensures that an IOException is thrown when a progress update is sent after the connection is broken.
     */
    @Test
    public void brokenProgressUpdate() throws Exception{
        try {
            new ImporterSocket().sendProgressUpdate(null, null, 0, 0);
            fail();
        }catch(IOException e){
            assertTrue(e.getMessage().equals("Socket closed."));
        }
    }

    /**
     * Ensures that nothing goes wrong in a simple sendProgressUpdate()
     * @throws Exception
     */
    @Test
    public void goodProgressUpdate() throws Exception{
        DependencyInjector.get().setProductionMode(false);

        ImporterSocket test = new ImporterSocket();
        test.onWebSocketConnect(null);
        test.sendProgressUpdate(null, null, 0,0);

        DependencyInjector.get().setProductionMode(true);
        assertTrue(DependencyInjector.get().isProductionMode());
    }

    /**
     * Ensures that nothing goes wrong in a simple sendPersonResults()
     * @throws Exception
     */
    @Test
    public void sendPersonResults() throws Exception{
        DependencyInjector.get().setProductionMode(false);

        ImporterSocket test = new ImporterSocket();
        test.onWebSocketConnect(null);
        test.sendPersonResults(new FlaggedResult(0.0,"pid","tim",2));

        DependencyInjector.get().setProductionMode(true);
        assertTrue(DependencyInjector.get().isProductionMode());
    }

    /**
     * Ensures that nothing goes wrong in a simple onError()
     * @throws Exception
     */
    @Test
    public void onError() throws Exception{
        DependencyInjector.get().setProductionMode(false);

        ImporterSocket test = new ImporterSocket();
        test.onWebSocketConnect(null);
        test.onWebSocketError(null);

        DependencyInjector.get().setProductionMode(true);
        assertTrue(DependencyInjector.get().isProductionMode());
    }

    /**
     * This is just to make sure a certain line is used in testing.
     * The class referenced has no methods and only exists so that some
     * constants can be shared.
     */
    @Test
    public void thisIsDumb(){
        new SharedConstants();
    }
}