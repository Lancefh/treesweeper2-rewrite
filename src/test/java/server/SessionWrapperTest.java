package server;

import org.junit.Test;

import static org.junit.Assert.*;

public class SessionWrapperTest {

    /**
     * Ensures that isOpen() works
     */
    @Test
    public void isOpen() {
        SessionWrapper test = new SessionWrapper(null);
        assertTrue(!test.isOpen());
    }

    /**
     * Ensures that close() works
     */
    @Test
    public void close() {
        SessionWrapper test = new SessionWrapper(null);
        test.close();
    }

    /**
     * Ensures that send works when everything is null.
     */
    @Test
    public void send() throws Exception{
        SessionWrapper test = new SessionWrapper(null);
        test.send(null);
    }
}