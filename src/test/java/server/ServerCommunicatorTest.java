package server;

import org.junit.Test;

import static org.junit.Assert.*;

public class ServerCommunicatorTest {

    /**
     * Ensures that the server throws an exception when given an invalid port number
     */
    @Test
    public void mainTest() throws Exception {
        try {
            ServerCommunicator.main(new String[]{"tim"});
            fail();
        }catch(Exception e){
            assertTrue(e.getMessage().equals("Invalid port number"));
        }
        ServerCommunicator.shutdown();
    }
}