package dates;

import org.junit.Test;

import static org.junit.Assert.*;

public class DateRangeTest {

    @Test
    public void sameness() {
        // Test 1: disjoint
        DateRange a = new DateRange(5);
        DateRange b = new DateRange(6);
        assertTrue(a.sameness(b) == 0.0);
        assertTrue(b.sameness(a) == 0.0);

        // Test 2: equal, size = 1
        b = new DateRange(5);
        assertTrue(a.sameness(b) == 1.0);
        assertTrue(b.sameness(a) == 1.0);

        // Test 3: equal, size = 2
        a = new DateRange(5,6);
        b = new DateRange(5,6);
        assertTrue(a.sameness(b) == 1.0);
        assertTrue(b.sameness(a) == 1.0);

        // Test 4: equal, size = 3
        a = new DateRange(5,7);
        b = new DateRange(5,7);
        assertTrue(a.sameness(b) == 1.0);
        assertTrue(b.sameness(a) == 1.0);

        // Test 5: subset, large size = 2, small size = 1
        a = new DateRange(5,6);
        b = new DateRange(5);
        assertTrue(a.sameness(b) == 1.0);
        assertTrue(b.sameness(a) == 1.0);

        // Test 6: subset, large size = 3, small size = 1
        a = new DateRange(5,7);
        assertTrue(a.sameness(b) == 1.0);
        assertTrue(b.sameness(a) == 1.0);

        // Test 7: subset, large size = 365, small size = 1
        a = new DateRange(1,365);
        assertTrue(a.sameness(b) == 1.0);
        assertTrue(b.sameness(a) == 1.0);

        // Test 8: subset, large size = 3, small size = 2
        a = new DateRange(5,7);
        b = new DateRange(5,6);
        assertTrue(a.sameness(b) == 1.0);
        assertTrue(b.sameness(a) == 1.0);

        // Test 9: subset, large size = 365, small size = 2
        a = new DateRange(1,365);
        assertTrue(a.sameness(b) == 1.0);
        assertTrue(b.sameness(a) == 1.0);

        // Test 10: subset, large size = 365, small size = 3
        b = new DateRange(5,7);
        assertTrue(a.sameness(b) == 1.0);
        assertTrue(b.sameness(a) == 1.0);

        // Test 11: overlap, large size = 2, small size = 2, overlap = 0.5
        a = new DateRange(5,6);
        b = new DateRange(6,7);
        assertTrue(a.sameness(b) == 0.5);
        assertTrue(b.sameness(a) == 0.5);

        // Test 12: overlap, large size = 3, small size = 2, overlap = 0.5
        a = new DateRange(4,6);
        assertTrue(a.sameness(b) == 0.5);
        assertTrue(b.sameness(a) == 0.5);

        // Test 13: reverse order of ranges in test 12
        a = new DateRange(4,5);
        b = new DateRange(5,7);
        assertTrue(a.sameness(b) == 0.5);
        assertTrue(b.sameness(a) == 0.5);

        // Test 14: overlap, large size = 1095, small size = 365, overlap = 0
        a = new DateRange(1,1095);
        b = new DateRange(1096, 1460);
        assertTrue(a.sameness(b) == 0.0);
        assertTrue(b.sameness(a) == 0.0);

        // Test 15: overlap, large size = 1095, small size = 365, overlap = 1 day
        b = new DateRange(1095,1459);
        assertTrue(a.sameness(b) == (double)1/(double)365);
        assertTrue(b.sameness(a) == (double)1/(double)365);

        // Test 16: overlap, large size = 1095, small size = 365, overlap = 1 week
        b = new DateRange(1089, 1453);
        assertTrue(a.sameness(b) == (double)7/(double)365);
        assertTrue(b.sameness(a) == (double)7/(double)365);

        // Test 17: overlap, large size = 1095, small size = 365, overlap = 364 days
        b = new DateRange(732, 1096);
        assertTrue(a.sameness(b) == (double)364/(double)365);
        assertTrue(b.sameness(a) == (double)364/(double)365);
    }

    @Test
    public void other(){
        DateRange test = new DateRange(5);
        assertTrue(test.getFirstDay() == 5);
        assertTrue(test.getLastDay() == 5);

        assertTrue(test.toString().equals("[ 5 ]"));

        test = new DateRange(5,6);
        assertTrue(test.toString().equals("[ 5 - 6 ]"));
    }
}