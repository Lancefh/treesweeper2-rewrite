package dates;

import org.junit.Test;

import static org.junit.Assert.*;

public class JulianDayConverterTest {

    @Test
    public void julianDay() {
        JulianDayConverter test = new JulianDayConverter();
        int result = test.julianDay(-4713, 11, 24);
        assertTrue(result == 0);
    }

    @Test
    public void gregorianDate() {
        JulianDayConverter test = new JulianDayConverter();
        String result = test.gregorianDate(0);
        assertTrue(result.equals("November 24, -4713"));
    }

    @Test
    public void month() {
        JulianDayConverter test = new JulianDayConverter();
        String result = test.month(1);
        assertTrue(result.equals("January"));
        result = test.month(2);
        assertTrue(result.equals("February"));
        result = test.month(3);
        assertTrue(result.equals("March"));
        result = test.month(4);
        assertTrue(result.equals("April"));
        result = test.month(5);
        assertTrue(result.equals("May"));
        result = test.month(6);
        assertTrue(result.equals("June"));
        result = test.month(7);
        assertTrue(result.equals("July"));
        result = test.month(8);
        assertTrue(result.equals("August"));
        result = test.month(9);
        assertTrue(result.equals("September"));
        result = test.month(10);
        assertTrue(result.equals("October"));
        result = test.month(11);
        assertTrue(result.equals("November"));
        result = test.month(12);
        assertTrue(result.equals("December"));
        assertNull(test.month(13));
    }
}