package dates;

import org.junit.Test;

import static org.junit.Assert.*;

public class FormalDateConverterTest {

    @Test
    public void convert() {
        FormalDateConverter test = new FormalDateConverter();

        // Test 1: No match
        String input = "bob";
        assertNull(test.convert(input));

        // Test 2: Single date, no month or day
        input = "A+1890";
        DateRange result = test.convert(input);
        int firstDay = result.getFirstDay();
        assertTrue(firstDay == 2410639);
        int lastDay = result.getLastDay();
        assertTrue(lastDay == 2412463);

        // Test 3: Single date, no day
        input = "+1890-1";
        result = test.convert(input);
        firstDay = result.getFirstDay();
        assertTrue(firstDay == 2411369);
        lastDay = result.getLastDay();
        assertTrue(lastDay == 2411399);


        // Test 4: Single date, full info
        input = "+1890-2-13";
        result = test.convert(input);
        firstDay = result.getFirstDay();
        assertTrue(firstDay == 2411412);
        lastDay = result.getLastDay();
        assertTrue(lastDay == 2411412);

        // Test 5: Two dates
        input = "+1890-3/+1891-4";
        result = test.convert(input);
        firstDay = result.getFirstDay();
        assertTrue(firstDay == 2411428);
        lastDay = result.getLastDay();
        assertTrue(lastDay == 2411853);
    }

    @Test
    public void numDays(){
        FormalDateConverter test = new FormalDateConverter();
        assertTrue(test.numDays(1) == 31);
        assertTrue(test.numDays(2) == 28);
        assertTrue(test.numDays(3) == 31);
        assertTrue(test.numDays(4) == 30);
        assertTrue(test.numDays(5) == 31);
        assertTrue(test.numDays(6) == 30);
        assertTrue(test.numDays(7) == 31);
        assertTrue(test.numDays(8) == 31);
        assertTrue(test.numDays(9) == 30);
        assertTrue(test.numDays(10) == 31);
        assertTrue(test.numDays(11) == 30);
        assertTrue(test.numDays(12) == 31);
        assertTrue(test.numDays(13) == 28);
    }
}