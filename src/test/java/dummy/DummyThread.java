package dummy;

import checkerImplementations.importer.IImportThread;
import checkerImplementations.importer.organization.Family;
import interfaces.DataImportException;

import static java.lang.Thread.sleep;

/**
 * Testing class for import threads
 */
public class DummyThread implements IImportThread {
    private String name;
    private Thread thread;
    private boolean alive;

    public DummyThread(){
        name = null;
        thread = null;
        alive = true;
    }

    @Override
    public void kill() {
        alive = false;
    }

    @Override
    public Family getFamily() {
        return null;
    }

    @Override
    public String getStatus() {
        return "status";
    }

    @Override
    public void setDetails(String details) {

    }

    @Override
    public int getThrottled() {
        return 0;
    }

    @Override
    public void setThrottled(int throttled) {

    }

    @Override
    public DataImportException getError() {
        return null;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void start() {
        if(name != null && name.equals("natural_stop")){
            thread = new Thread(() ->{
                long endTime = System.currentTimeMillis() + 100;
                while(System.currentTimeMillis() < endTime){
                    // wait for it...
                }
            });
        }else{
            thread = new Thread(() -> {
                try {
                    sleep(2000);
                    long startTime = System.currentTimeMillis();
                    long endTime = startTime + 1000;
                    while(System.currentTimeMillis() < endTime){
                        // wait for it...
                    }
                    TestUpdater.BREAK_FLAG = true;
                    endTime = System.currentTimeMillis() + 1000;
                    while(System.currentTimeMillis() < endTime){
                        // wait until the break!
                        if(!alive) break;
                    }
                }catch(InterruptedException e){
                    Thread.currentThread().interrupt();
                }
            });
        }

        thread.start();
    }

    @Override
    public void join() throws InterruptedException {
        if(thread != null) thread.join();
    }

    @Override
    public Thread.State getState() {
        return thread != null ? thread.getState() : null;
    }
}
