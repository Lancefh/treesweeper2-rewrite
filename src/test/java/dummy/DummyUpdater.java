package dummy;

import checkerImplementations.handlers.results.FlaggedResult;
import server.IUpdater;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * This is a class for testing purposes
 */
public class DummyUpdater implements IUpdater {
    private List<String> messages;
    private List<String> progress;
    private List<String> errors;
    private List<FlaggedResult> results;
    private List<String> consoleMessages;

    public DummyUpdater(){
        messages = new ArrayList<>();
        progress = new ArrayList<>();
        errors = new ArrayList<>();
        results = new ArrayList<>();
        consoleMessages = new ArrayList<>();
    }

    @Override
    public void sendMessageUpdate(String message, String submessage) throws IOException {
        messages.add(message);
    }

    @Override
    public void sendProgressUpdate(String message, String submessage, int target, int progress) throws IOException {
        this.progress.add(message);
    }

    @Override
    public void sendErrorUpdate(String message) throws IOException {
        errors.add(message);
    }

    @Override
    public void sendPersonResults(FlaggedResult results) throws IOException {
        this.results.add(results);
    }

    @Override
    public void sendConsoleMessage(String message) throws IOException {
        consoleMessages.add(message);
    }

    public List<String> getMessages() {
        return messages;
    }

    public List<String> getProgress() {
        return progress;
    }

    public List<String> getErrors() {
        return errors;
    }

    public List<FlaggedResult> getResults() {
        return results;
    }

    public List<String> getConsoleMessages() {
        return consoleMessages;
    }
}
