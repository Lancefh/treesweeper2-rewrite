package dummy;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.WebSocketListener;

import java.io.IOException;

/**
 * This is a class for testing purposes
 */
public class TestSocket implements WebSocketListener {
    private Session session;
    private TestClient client;

    public TestSocket(TestClient client){
        session = null;
        this.client = client;
    }

    public boolean isOpen(){
        return session != null && session.isOpen();
    }

    public void disconnect(){
        if(session != null) session.close();
        session = null;
    }

    public void send(String msg) throws IOException{
        if(session != null) session.getRemote().sendString(msg);
    }

    @Override
    public void onWebSocketBinary(byte[] bytes, int i, int i1) {
        // meh
    }

    @Override
    public void onWebSocketText(String s) {
        if(client != null) client.addMessage(s);
    }

    @Override
    public void onWebSocketClose(int i, String s) {
        session = null;
    }

    @Override
    public void onWebSocketConnect(Session session) {
        this.session = session;
    }

    @Override
    public void onWebSocketError(Throwable throwable) {
        throwable.printStackTrace();
    }
}
