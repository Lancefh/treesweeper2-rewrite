package dummy;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.client.ClientUpgradeRequest;
import org.eclipse.jetty.websocket.client.WebSocketClient;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

/**
 * This is a class for testing purposes
 */
public class TestClient {
    private List<String> messages;
    private TestSocket socket;
    private WebSocketClient client;

    public TestClient(){
        messages = new ArrayList<>();
        socket = null;
        client = null;
    }

    public void connect(String target){
        client = new WebSocketClient();

        socket = new TestSocket(this);
        try{
            client.start();

            URI uri = new URI(target);
            ClientUpgradeRequest request = new ClientUpgradeRequest();
            Future<Session> future = client.connect(socket, uri, request);

            future.get();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void addMessage(String msg){
        messages.add(msg);
    }

    public List<String> getMessages(){ return messages;}

    public void terminate(){
        if(socket != null)socket.disconnect();
        socket = null;
        try {
            client.stop();
        }catch(Exception e){
            e.printStackTrace();
        }
        client = null;
    }

    public void send(String msg) throws IOException {
        if(socket != null) socket.send(msg);
    }

    public boolean isConnected(){
        return socket != null && socket.isOpen();
    }
}
