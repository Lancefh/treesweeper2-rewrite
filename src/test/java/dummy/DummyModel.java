package dummy;

import compileTester.ModelFactory;
import modelVisitor.ModelBuildException;
import osax.model.OSAXModel;
import osax.object.ModelObject;
import osax.object.lexical.STRING;
import osax.object.nonlexical.NonLexicalObject;
import osax.relationSet.Tuple;
import osax.relationSet.TupleMismatchException;

/**
 * This is a class for testing purposes.
 */
public class DummyModel {
    private static final String MODEL_FILE = "model_and_configuration/FamilySearchModel.txt";

    private OSAXModel model;
    private NonLexicalObject person1;

    public DummyModel(){
        model = null;
        person1 = null;
    }

    public ModelObject getPerson1(){ return person1; }

    public OSAXModel getModel(){ return model; }

    public void initialize(){
        this.clear();
        ModelFactory factory = new ModelFactory();
        try{
            OSAXModel model = factory.generateModel(MODEL_FILE);
            populate(model);
            this.model = model;
        }catch(ModelBuildException | TupleMismatchException e){
            e.printStackTrace();
            clear();
        }
    }

    private void populate(OSAXModel model) throws TupleMismatchException {
        // Create person1
        NonLexicalObject person1 = new NonLexicalObject();
        model.getNonLexicalObjectSet("Person").addObject(person1);
        this.person1 = person1;

        // Assign person1 some data
        ModelObject person1gender = new STRING("Female");
        model.getLexicalObjectSet("Gender").addObject(person1gender);
        model.getRelationshipSet("Person has Gender").insert(new Tuple(person1, person1gender));

        // Assign some unaffiliated data
        NonLexicalObject person2 = new NonLexicalObject();
        model.getNonLexicalObjectSet("Person").addObject(person2);
        ModelObject person2Name = new STRING("Tim");
        model.getLexicalObjectSet("Name").addObject(person2Name);
        model.getRelationshipSet("Person has Name").insert(new Tuple(person2, person2Name));
    }

    private void clear(){
        model = null;
        person1 = null;
    }
}
