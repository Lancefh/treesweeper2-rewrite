package dummy;

import checkerImplementations.handlers.results.FlaggedResult;
import server.IUpdater;

import java.io.IOException;

/**
 * This is a class for testing purposes.
 */
public class BrokenUpdater implements IUpdater {
    @Override
    public void sendMessageUpdate(String message, String submessage) throws IOException {
        throw new IOException("i am broken");
    }

    @Override
    public void sendProgressUpdate(String message, String submessage, int target, int progress) throws IOException {
        throw new IOException("i am broken");
    }

    @Override
    public void sendErrorUpdate(String message) throws IOException {
        throw new IOException("i am broken");
    }

    @Override
    public void sendPersonResults(FlaggedResult results) throws IOException {
        throw new IOException("i am broken");
    }

    @Override
    public void sendConsoleMessage(String message) throws IOException {
        throw new IOException("i am broken");
    }
}
