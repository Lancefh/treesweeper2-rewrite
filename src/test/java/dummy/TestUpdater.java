package dummy;

import checkerImplementations.handlers.results.FlaggedResult;
import server.IUpdater;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * This is a testing class, used when testing ImportThreadManager
 */
public class TestUpdater implements IUpdater {
    public static boolean BREAK_FLAG = false;

    private List<String> userUpdates;
    private List<String> consoleUpdates;
    private boolean autofail;

    public TestUpdater(boolean autofail){
        userUpdates = new ArrayList<>();
        consoleUpdates = new ArrayList<>();
        this.autofail = autofail;
    }

    @Override
    public void sendMessageUpdate(String message, String submessage) throws IOException {
        if(!BREAK_FLAG) userUpdates.add(message);
        else throw new IOException("haha");
        if(message.equals("All data gathered, organizing it now...") && autofail) BREAK_FLAG = true;
    }

    @Override
    public void sendProgressUpdate(String message, String submessage, int target, int progress) throws IOException {
        if(!BREAK_FLAG) userUpdates.add(message);
        else throw new IOException("haha");
    }

    @Override
    public void sendErrorUpdate(String message) throws IOException {
        if(!BREAK_FLAG) userUpdates.add(message);
        else throw new IOException("haha");
    }

    @Override
    public void sendPersonResults(FlaggedResult results) throws IOException {

    }

    @Override
    public void sendConsoleMessage(String message) throws IOException {
        if(!BREAK_FLAG) consoleUpdates.add(message);
        else throw new IOException("haha");
    }
}
