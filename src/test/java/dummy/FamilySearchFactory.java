package dummy;

import checkerImplementations.importer.familySearchModel.extensibleData.hypermediaEnabledData.conclusion.Gender;
import checkerImplementations.importer.familySearchModel.extensibleData.hypermediaEnabledData.conclusion.subject.Person;
import checkerImplementations.importer.familySearchModel.extensibleData.hypermediaEnabledData.gedcomx.FamilySearchPlatform;

/**
 * This is a class for testing purposes
 */
public class FamilySearchFactory {
    public FamilySearchFactory(){}

    public FamilySearchPlatform generateDetails(String pid, boolean isMale){
        FamilySearchPlatform result = new FamilySearchPlatform();
        Person person = new Person();
        person.setId(pid);
        String genderType = isMale ? "http://gedcomx.org/Male" : "http://gedcomx.org/Female";
        Gender gender = new Gender();
        gender.setType(genderType);
        person.setGender(gender);
        result.setPersons(new Person[]{person});
        return result;
    }
}
