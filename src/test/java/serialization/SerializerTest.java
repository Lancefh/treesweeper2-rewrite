package serialization;

import checkerImplementations.importer.familySearchModel.extensibleData.hypermediaEnabledData.gedcomx.FamilySearchPlatform;
import com.google.gson.stream.MalformedJsonException;
import org.junit.Test;
import server.ImportOptions;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import static org.junit.Assert.*;

public class SerializerTest {
    private static final String TEST_FILE_ROOT = "src/test/test_files/serialization_tests/";
    private static final String BAD_FILE_ROOT = "src/test/test_files/bad_files/";

    /**
     * Ensures that deserializing is working. Deserializes multiple files from JSON;
     * each of them should deserialize.
     */
    @Test
    public void deserialize() throws Exception{
        // Deserialize every file in the test files
        File testDirectory = new File(TEST_FILE_ROOT);
        assertTrue(testDirectory.exists() && testDirectory.isDirectory());
        File[] tests = testDirectory.listFiles();
        assertNotNull(tests);
        for(File test : tests){
            // Convert file to string
            Scanner scanner = new Scanner(test);
            String content = scanner.useDelimiter("\\A").next();
            scanner.close();

            // Deserialize the content
            FamilySearchPlatform result =
                    (FamilySearchPlatform)Serializer.get()
                            .deserialize(content, FamilySearchPlatform.class);
            assertNotNull(result);
        }
    }

    /**
     * Ensures that files that shouldn't deserialize don't. Each file
     * tested should fail to deserialize from JSON.
     */
    @Test
    public void badTests() throws Exception{
        // Deserialize every file in the test files
        File testDirectory = new File(BAD_FILE_ROOT);
        assertTrue(testDirectory.exists() && testDirectory.isDirectory());
        File[] tests = testDirectory.listFiles();
        assertNotNull(tests);
        for(File test : tests){
            // Convert file to string
            Scanner scanner = new Scanner(test);
            String content = scanner.useDelimiter("\\A").next();
            scanner.close();

            // Deserialize the content; this should fail
            try {
                Serializer.get().deserialize(content, FamilySearchPlatform.class);
                fail();
            }catch(SerializeException e){}
        }
    }

    /**
     * Ensures that data can be de-serialized, re-serialized, and then de-serialized again.
     * @throws Exception
     */
    @Test
    public void serializeTest() throws Exception{
        // Deserialize, then re-serialize
        File target = new File(TEST_FILE_ROOT + "/hypermedia_links_1.txt");
        Scanner scanner = new Scanner(target);
        String content = scanner.useDelimiter("\\A").next();
        scanner.close();
        FamilySearchPlatform result = (FamilySearchPlatform)Serializer.get().deserialize(content, FamilySearchPlatform.class);

        Serializer.get().serialize(result); // Just making sure this doesn't break

        target = new File(TEST_FILE_ROOT + "/sourceDescriptions_1.txt");
        scanner = new Scanner(target);
        content = scanner.useDelimiter("\\A").next();
        scanner.close();
        result = (FamilySearchPlatform)Serializer.get().deserialize(content, FamilySearchPlatform.class);

        Serializer.get().serialize(result); // Just making sure this doesn't break
    }

    /**
     * This just tests that a constructor for a SerializeException isn't broken.
     */
    @Test
    public void serializeException(){
        try{
            throw new SerializeException();
        }catch(SerializeException e){}
    }

    /**
     * This ensures that an empty string will not be parsed as JSON successfully
     */
    @Test
    public void pingTest(){
        String message = "";
        try{
            Serializer.get().deserialize(message, ImportOptions.class);
            fail();
        }catch(SerializeException e){}
    }
}