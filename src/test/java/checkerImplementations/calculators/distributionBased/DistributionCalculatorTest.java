package checkerImplementations.calculators.distributionBased;

import distribution.Distribution;
import org.junit.Test;
import osax.object.ModelObject;
import osax.object.lexical.INTEGER;
import osax.object.lexical.STRING;
import osax.relationSet.Tuple;

import java.io.FileInputStream;

import static org.junit.Assert.*;

public class DistributionCalculatorTest {

    /**
     * Ensures that the base distribution probability calculator is calculating
     * probability correctly.
     */
    @Test
    public void calculateProbability() throws Exception{
        DistributionCalculator test = new DistributionCalculator() {
            @Override
            protected ModelObject getValue(Tuple tuple) {
                return new INTEGER("10");
            }

            @Override
            protected double getProbability(Distribution distribution, ModelObject value) {
                return 0.5;
            }
        };
        Distribution d = new Distribution(new FileInputStream("distributions/HusbandLifespan.csv"));
        test.useDistribution(d);
        Tuple t = new Tuple(new STRING("hi"), new STRING("world"));
        double probability = test.calculateProbability(t);
        assertTrue(probability == 0.5);
    }

    /**
     * Ensures that the base distribution probability calculator can
     * set the lower bound without breaking.
     */
    @Test
    public void useLowerBound() throws Exception{
        DistributionCalculator test = new DistributionCalculator() {
            @Override
            protected ModelObject getValue(Tuple tuple) {
                return new INTEGER("10");
            }

            @Override
            protected double getProbability(Distribution distribution, ModelObject value) {
                return 0.5;
            }
        };
        Distribution d = new Distribution(new FileInputStream("distributions/HusbandLifespan.csv"));
        test.useDistribution(d);
        test.useLowerBound(20);
    }

    /**
     * Ensures that the base distribution probability calculator can
     * set the upper bound without breaking.
     */
    @Test
    public void useUpperBound() throws Exception{
        DistributionCalculator test = new DistributionCalculator() {
            @Override
            protected ModelObject getValue(Tuple tuple) {
                return new INTEGER("10");
            }

            @Override
            protected double getProbability(Distribution distribution, ModelObject value) {
                return 0.5;
            }
        };
        Distribution d = new Distribution(new FileInputStream("distributions/HusbandLifespan.csv"));
        test.useDistribution(d);
        test.useUpperBound(20);
    }
}