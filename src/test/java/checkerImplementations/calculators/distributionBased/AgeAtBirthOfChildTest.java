package checkerImplementations.calculators.distributionBased;

import distribution.Distribution;
import org.junit.Test;
import osax.object.ModelObject;
import osax.object.lexical.INTEGER;
import osax.relationSet.Tuple;

import java.io.FileInputStream;

import static org.junit.Assert.*;

public class AgeAtBirthOfChildTest {

    /**
     * Ensures that AgeAtBirthOfChild calculator is picking the right value.
     */
    @Test
    public void getValue() {
        AgeAtBirthOfChild test = new AgeAtBirthOfChild();
        Tuple t = new Tuple(new INTEGER("10"), new INTEGER("20"), new INTEGER("30"));
        ModelObject m = test.getValue(t);
        assertTrue(m.equals(new INTEGER("30")));
    }

    /**
     * Ensures that the probability calculator calculates probability correctly.
     */
    @Test
    public void getProbability() throws Exception {
        Distribution d = new Distribution(new FileInputStream("distributions/FatherAgeAtFirstChild.csv"));
        AgeAtBirthOfChild test = new AgeAtBirthOfChild();
        double probability = test.getProbability(d, new INTEGER("20"));
        assertTrue(probability == 0.0348140224814415);
    }
}