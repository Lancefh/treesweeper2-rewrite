package checkerImplementations.calculators.distributionBased;

import distribution.Distribution;
import org.junit.Test;
import osax.object.ModelObject;
import osax.object.lexical.INTEGER;
import osax.relationSet.Tuple;

import java.io.FileInputStream;

import static org.junit.Assert.*;

public class MarriageToChildTest {

    /**
     * Ensures that the calculator is getting the right value.
     */
    @Test
    public void getValue() {
        MarriageToChild test = new MarriageToChild();
        Tuple t = new Tuple(new INTEGER("10"), new INTEGER("20"), new INTEGER("30"), new INTEGER("40"));
        ModelObject m = test.getValue(t);
        assertTrue(m.equals(new INTEGER("40")));
    }

    /**
     * Ensures that the calculator is calculating probability correctly.
     */
    @Test
    public void getProbability() throws Exception{
        Distribution d = new Distribution(new FileInputStream("distributions/FatherAgeAtFirstChild.csv"));
        MarriageToChild test = new MarriageToChild();
        double probability = test.getProbability(d, new INTEGER("20"));
        assertTrue(probability == 0.0348140224814415);
    }
}