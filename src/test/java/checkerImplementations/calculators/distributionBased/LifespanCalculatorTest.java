package checkerImplementations.calculators.distributionBased;

import distribution.Distribution;
import org.junit.Test;
import osax.object.ModelObject;
import osax.object.lexical.INTEGER;
import osax.relationSet.Tuple;

import java.io.FileInputStream;

import static org.junit.Assert.*;

public class LifespanCalculatorTest {

    /**
     * Ensures that the calculator is getting the right value.
     */
    @Test
    public void getValue() {
        LifespanCalculator test = new LifespanCalculator();
        Tuple t = new Tuple(new INTEGER("10"), new INTEGER("20"), new INTEGER("30"));
        ModelObject m = test.getValue(t);
        assertTrue(m.equals(new INTEGER("20")));
    }

    /**
     * Ensures that the calculator is calculating probability correctly.
     */
    @Test
    public void getProbability() throws Exception{
        Distribution d = new Distribution(new FileInputStream("distributions/FatherAgeAtFirstChild.csv"));
        LifespanCalculator test = new LifespanCalculator();
        double probability = test.getProbability(d, new INTEGER("20"));
        assertTrue(probability == 0.0348140224814415);
    }
}