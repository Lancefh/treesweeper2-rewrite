package checkerImplementations.calculators.notDistributionBased;

import org.junit.Test;
import osax.object.lexical.DOUBLE;
import osax.object.lexical.NULL;
import osax.object.nonlexical.NonLexicalObject;
import osax.relationSet.Tuple;

import static org.junit.Assert.*;

public class SpouseAgeGapCalculatorTest {

    @Test
    public void calculateProbability() {
        SpouseAgeGapCalculator test = new SpouseAgeGapCalculator();
        Tuple tuple = new Tuple(new NonLexicalObject(), new NonLexicalObject(), new NULL());
        assertTrue(test.calculateProbability(tuple) == 1.0);

        tuple = new Tuple(new NonLexicalObject(), new NonLexicalObject(), new DOUBLE("99"));
        assertTrue(test.calculateProbability(tuple) == 1.0);
        tuple = new Tuple(new NonLexicalObject(), new NonLexicalObject(), new DOUBLE("-99"));
        assertTrue(test.calculateProbability(tuple) == 1.0);

        tuple = new Tuple(new NonLexicalObject(), new NonLexicalObject(), new DOUBLE("100"));
        assertTrue(test.calculateProbability(tuple) == 0.0);
        tuple = new Tuple(new NonLexicalObject(), new NonLexicalObject(), new DOUBLE("-100"));
        assertTrue(test.calculateProbability(tuple) == 0.0);
    }

    @Test
    public void otherTests(){
        SpouseAgeGapCalculator test = new SpouseAgeGapCalculator();
        test.useDistribution(null);
        test.useLowerBound(0.0);
        test.useUpperBound(0.0);
    }
}
