package checkerImplementations.calculators.notDistributionBased;

import org.junit.Test;
import osax.object.lexical.INTEGER;
import osax.relationSet.Tuple;

import static org.junit.Assert.*;

public class DeathProbabilityCalculatorTest {

    /**
     * Ensures that the calculator is calculating probability correctly.
     */
    @Test
    public void calculateProbability() {
        DeathProbabilityCalculator test = new DeathProbabilityCalculator();
        Tuple t = new Tuple(new INTEGER("10"), new INTEGER("123"));
        double probability = test.calculateProbability(t);
        assertTrue(probability == 0);
    }

    /**
     * Ensures that the calculator can call useDistribution() without breaking.
     */
    @Test
    public void useDistribution() {
        DeathProbabilityCalculator test = new DeathProbabilityCalculator();
        test.useDistribution(null);
    }

    /**
     * Ensures that the calculator can call useLowerBound() without breaking.
     */
    @Test
    public void useLowerBound() {
        DeathProbabilityCalculator test = new DeathProbabilityCalculator();
        test.useLowerBound(4);
    }

    /**
     * Ensures that the calculator can call useUpperBound() without breaking.
     */
    @Test
    public void useUpperBound() {
        DeathProbabilityCalculator test = new DeathProbabilityCalculator();
        test.useUpperBound(10);
    }
}