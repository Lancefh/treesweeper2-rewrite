package checkerImplementations.importer.proxy;

import org.junit.Test;

import static org.junit.Assert.*;

public class ResponseTest {

    /**
     * Ensures that getBody() works
     */
    @Test
    public void getBody() {
        Response test = new Response(null);
        test.setBody(null);
        assertTrue(test.getBody() == null);
    }

    /**
     * Ensures that getRetry() works
     */
    @Test
    public void getRetry() {
        Response test = new Response(null, 10);
        test.setRetry(10);
        assertTrue(test.getRetry() == 10);
    }
}