package checkerImplementations.importer.proxy;

import injector.DependencyInjector;
import interfaces.DataImportException;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class HttpClientTest {

    /**
     * Ensures that multiple tests are working correctly. Descriptions for
     * each sub-test are given below.
     */
    @Test
    public void get() throws Exception{
        // Set to testing mode
        DependencyInjector.get().setProductionMode(false);

        // Test 1: Make sure a simple request works
        HttpClient testClient = new HttpClient();
        Response result = testClient.get("test1", "token");
        assertTrue(result.getBody().equals("This is a message."));

        // Test 2: Make sure a no-content result returns null
        result = testClient.get("test2", "token");
        assertNull(result);

        // Test 3: Make sure when an error is thrown that it is passed on
        try {
            testClient.get("test3", "token");
            fail();
        }catch(DataImportException e){}

        // Test 4: Make sure that an error and retry result can be retrieved
        result = testClient.get("test4", "token");
        assertTrue(result.getBody().equals("This is an error message."));
        assertTrue(result.getRetry() == 10);

        // Test 5: Make sure that IOExceptions are handled properly internally
        try{
            testClient.get("test5", "token");
            fail();
        }catch(DataImportException e){}

        // Set back to production mode
        DependencyInjector.get().setProductionMode(true);
        assertTrue(DependencyInjector.get().isProductionMode());
    }
}