package checkerImplementations.importer.proxy.test;

import injector.DependencyInjector;
import org.junit.Test;

import static org.junit.Assert.*;

public class TestThreadTest {

    /**
     * Ensures that some TestThread methods work.
     */
    @Test
    public void completeness(){
        DependencyInjector.get().setProductionMode(false);

        TestThread test = new TestThread("test_thread", 0);
        test.kill();    // This does nothing.
        assertNull(test.getFamily());

        test = new TestThread("exception", 0);
        assertNull(test.getFamily());

        assertNull(test.getStatus());
        test.setDetails(null);
        assertTrue(test.getThrottled() == 0);
        test.setThrottled(0);

        DependencyInjector.get().setProductionMode(true);
        assertTrue(DependencyInjector.get().isProductionMode());
    }
}