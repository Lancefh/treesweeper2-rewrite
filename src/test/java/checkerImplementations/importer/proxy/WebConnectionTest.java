package checkerImplementations.importer.proxy;

import org.junit.Test;

import java.net.HttpURLConnection;

import static org.junit.Assert.*;

public class WebConnectionTest {

    /**
     * Ensures that WebConnection can connect to web sites. This
     * test needs internet access to work.
     */
    @Test
    public void connect() throws Exception{
        // This test uses an external api for testing web connections; if that api changes, this test will break.
        // If this test has broken, check and make sure that the api has not changed.
        WebFactory factory = new WebFactory();
        WebConnection test = (WebConnection)factory.openConnection("http://ron-swanson-quotes.herokuapp.com/v2/quotes");
        test.setRequestMethod("GET");
        test.setRequestProperty("hi", "hello");
        test.connect();
        assertTrue(test.getResponseCode() == HttpURLConnection.HTTP_OK);
        assertTrue(test.getInputStream() != null);
        assertTrue(test.getErrorStream() == null);
        assertTrue(test.getHeaderField("Retry") == null);

        test.setConnectTimeout(10);
        test.setReadTimeout(10);
    }
}