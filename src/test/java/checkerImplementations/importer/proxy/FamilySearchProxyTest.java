package checkerImplementations.importer.proxy;

import checkerImplementations.importer.familySearchModel.extensibleData.hypermediaEnabledData.gedcomx.FamilySearchPlatform;
import injector.DependencyInjector;
import interfaces.DataImportException;
import org.junit.Test;

import static org.junit.Assert.*;

public class FamilySearchProxyTest {

    /**
     * Ensures that getAncestry() works, and that throttling works correctly.
     */
    @Test
    public void getAncestry() throws Exception{
        // This test also tests throttling
        enableTesting();

        FamilySearchProxy test = new FamilySearchProxy();
        test.setThread(null);
        FamilySearchPlatform result = test.getAncestry("test0", "token", 7);
        assertNull(result);

        result = test.getAncestry("test1", "token", 7);
        assertNull(result.getUsers());

        disableTesting();
    }

    /**
     * Ensures that getSpouses() works, and that timeouts are handled correctly.
    */
    @Test
    public void getSpouses() throws Exception {
        // This test also tests timeouts
        enableTesting();

        FamilySearchProxy test = new FamilySearchProxy();
        FamilySearchPlatform result = test.getSpouses("test2", "token");
        assertNull(result);

        disableTesting();
    }

    /**
     * Ensures that getChildren() works
     */
    @Test
    public void getChildren() throws Exception{
        enableTesting();

        FamilySearchProxy test = new FamilySearchProxy();
        FamilySearchPlatform result = test.getChildren("test3", "token");
        assertNull(result.getUsers());

        disableTesting();
    }

    /**
     * Ensures that getDetails() works
     */
    @Test
    public void getDetails() throws Exception{
        enableTesting();

        FamilySearchProxy test = new FamilySearchProxy();
        FamilySearchPlatform result = test.getDetails("test4", "token");
        assertNull(result.getUsers());

        disableTesting();
    }

    /**
     * Ensures that getCurrentUser() works. It should return
     * a FamilySearchPlatform with a null 'users' field.
     */
    @Test
    public void getCurrentUser() throws Exception{
        enableTesting();

        FamilySearchProxy test = new FamilySearchProxy();
        FamilySearchPlatform result = test.getCurrentUser("token");
        assertNull(result.getUsers());

        disableTesting();
    }

    /**
     * Ensures that getParents() works
     */
    @Test
    public void getParents() throws Exception{
        enableTesting();

        FamilySearchProxy test = new FamilySearchProxy();
        FamilySearchPlatform result = test.getParents("test3", "token");
        assertNull(result.getUsers());

        disableTesting();
    }

    /**
     * Ensures that deserialization errors are handled correctly
     * and return a null.
     */
    @Test
    public void badSerialization() throws Exception{
        enableTesting();

        FamilySearchProxy test = new FamilySearchProxy();
        FamilySearchPlatform result = test.getCurrentUser("badserialize");
        assertNull(result);

        disableTesting();
    }

    /**
     * Ensures that when an exception is thrown during the download
     * process that it is handled correctly and a DataImportException is thrown.
     */
    @Test
    public void interrupting() throws Exception{
        enableTesting();

        FamilySearchProxy test = new FamilySearchProxy();
        try {
            test.getAncestry("test5", "token", 7);
            fail();
        }catch(DataImportException e){
            assertTrue(e.getMessage().equals("tada"));
        }

        disableTesting();
    }

    /**
     * Enables testing mode for the dependency injector
     */
    private void enableTesting(){
        DependencyInjector.get().setProductionMode(false);
    }

    /**
     * Disables testing mode for the dependency injector
     */
    private void disableTesting(){
        DependencyInjector.get().setProductionMode(true);
        assertTrue(DependencyInjector.get().isProductionMode());
    }
}