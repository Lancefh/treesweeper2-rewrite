package checkerImplementations.importer.familySearchModel;

import checkerImplementations.importer.familySearchModel.extensibleData.*;
import checkerImplementations.importer.familySearchModel.extensibleData.hypermediaEnabledData.*;
import checkerImplementations.importer.familySearchModel.extensibleData.hypermediaEnabledData.abstractMessageThread.AbstractMessageThread;
import checkerImplementations.importer.familySearchModel.extensibleData.hypermediaEnabledData.abstractMessageThread.MessageThread;
import checkerImplementations.importer.familySearchModel.extensibleData.hypermediaEnabledData.abstractMessageThread.UserMessageThreadSummary;
import checkerImplementations.importer.familySearchModel.extensibleData.hypermediaEnabledData.conclusion.Conclusion;
import checkerImplementations.importer.familySearchModel.extensibleData.hypermediaEnabledData.conclusion.Fact;
import checkerImplementations.importer.familySearchModel.extensibleData.hypermediaEnabledData.conclusion.Gender;
import checkerImplementations.importer.familySearchModel.extensibleData.hypermediaEnabledData.conclusion.Name;
import checkerImplementations.importer.familySearchModel.extensibleData.hypermediaEnabledData.conclusion.reservation.Ordinance;
import checkerImplementations.importer.familySearchModel.extensibleData.hypermediaEnabledData.conclusion.reservation.Reservation;
import checkerImplementations.importer.familySearchModel.extensibleData.hypermediaEnabledData.conclusion.subject.*;
import checkerImplementations.importer.familySearchModel.extensibleData.hypermediaEnabledData.gedcomx.FamilySearchPlatform;
import checkerImplementations.importer.familySearchModel.extensibleData.hypermediaEnabledData.gedcomx.Gedcomx;
import checkerImplementations.importer.familySearchModel.textValue.AgentName;
import checkerImplementations.importer.familySearchModel.textValue.TextValue;
import org.junit.Test;
import serialization.HypermediaLinksAdapter;

import static org.junit.Assert.*;

/**
 * This class just tests methods for various
 * FamilySearch model classes. Each class has a single test that
 * tests all its methods.
 */
public class FamilySearchModelTest {
    @Test
    public void tag(){
        Tag test = new Tag();
        test.setResource(null);
        assertTrue(test.getResource() == null);
    }

    @Test
    public void searchInfo(){
        SearchInfo test = new SearchInfo();
        test.setCloseHits(0);
        assertTrue(test.getCloseHits() == 0);
        test.setTotalHits(0);
        assertTrue(test.getTotalHits() == 0);
    }

    @Test
    public void resourceReference(){
        ResourceReference test = new ResourceReference();
        test.setResource(null);
        assertTrue(test.getResource() == null);
        test.setResourceId(null);
        assertTrue(test.getResourceId() == null);
        String s = test.toString();
        assertTrue(s.equals("{resource: null, resourceId: null}"));
    }

    @Test
    public void qualifier(){
        Qualifier test = new Qualifier();
        test.setName(null);
        assertTrue(test.getName() == null);
        test.setValue(null);
        assertTrue(test.getValue() == null);
    }

    @Test
    public void placeDescriptionInfo(){
        PlaceDescriptionInfo test = new PlaceDescriptionInfo();
        test.setRelatedSubType(null);
        assertTrue(test.getRelatedSubType() == null);
        test.setRelatedType(null);
        assertTrue(test.getRelatedType() == null);
        test.setZoomLevel(0);
        assertTrue(test.getZoomLevel() == 0);
    }

    @Test
    public void personInfo(){
        PersonInfo test = new PersonInfo();
        test.setPrivateSpaceRestricted(true);
        assertTrue(test.isPrivateSpaceRestricted());
        test.setReadOnly(true);
        assertTrue(test.isReadOnly());
    }

    @Test
    public void oEmbed(){
        OEmbed test = new OEmbed();
        test.setAuthor_name(null);
        assertTrue(test.getAuthor_name() == null);
        test.setAuthor_url(null);
        assertTrue(test.getAuthor_url() == null);
        test.setCache_age(null);
        assertTrue(test.getCache_age() == null);
        test.setCitation(null);
        assertTrue(test.getCitation() == null);
        test.setCollection(null);
        assertTrue(test.getCollection() == null);
        test.setCollection_uri(null);
        assertTrue(test.getCollection_uri() == null);
        test.setHeight(null);
        assertTrue(test.getHeight() == null);
        test.setHtml(null);
        assertTrue(test.getHtml() == null);
        test.setLong_lived_url(null);
        assertTrue(test.getLong_lived_url() == null);
        test.setProvider_name(null);
        assertTrue(test.getProvider_name() == null);
        test.setProvider_url(null);
        assertTrue(test.getProvider_url() == null);
        test.setRecord_contains(null);
        assertTrue(test.getRecord_contains() == null);
        test.setThumbnail_height(null);
        assertTrue(test.getThumbnail_height() == null);
        test.setThumbnail_url(null);
        assertTrue(test.getThumbnail_url() == null);
        test.setThumbnail_width(null);
        assertTrue(test.getThumbnail_width() == null);
        test.setTitle(null);
        assertTrue(test.getTitle() == null);
        test.setType(null);
        assertTrue(test.getType() == null);
        test.setUrl(null);
        assertTrue(test.getUrl() == null);
        test.setVersion(null);
        assertTrue(test.getVersion() == null);
        test.setWidth(null);
        assertTrue(test.getWidth() == null);
    }

    @Test
    public void nameFormInfo(){
        NameFormInfo test = new NameFormInfo();
        test.setOrder(null);
        assertTrue(test.getOrder() == null);
        assertTrue(test.toString().equals("{ order: null}\n"));
    }

    @Test
    public void mergeConflict(){
        MergeConflict test = new MergeConflict();
        test.setDuplicateResource(null);
        assertTrue(test.getDuplicateResource() == null);
        test.setSurvivorResource(null);
        assertTrue(test.getSurvivorResource() == null);
    }

    @Test
    public void mergeAnalysis(){
        MergeAnalysis test = new MergeAnalysis();
        test.setConflictingResources(null);
        assertTrue(test.getConflictingResources() == null);
        test.setDuplicate(null);
        assertTrue(test.getDuplicate() == null);
        test.setDuplicateResources(null);
        assertTrue(test.getDuplicateResources() == null);
        test.setSurvivor(null);
        assertTrue(test.getSurvivor() == null);
        test.setSurvivorResources(null);
        assertTrue(test.getSurvivorResources() == null);
    }

    @Test
    public void merge(){
        Merge test = new Merge();
        test.setResourcesToCopy(null);
        assertTrue(test.getResourcesToCopy() == null);
        test.setResourcesToDelete(null);
        assertTrue(test.getResourcesToDelete() == null);
    }

    @Test
    public void matchInfo(){
        MatchInfo test = new MatchInfo();
        test.setAddsDateOrPlace(true);
        assertTrue(test.isAddsDateOrPlace());
        test.setAddsFact(true);
        assertTrue(test.isAddsFact());
        test.setAddsPerson(true);
        assertTrue(test.isAddsPerson());
        test.setCollection(null);
        assertTrue(test.getCollection() == null);
        test.setStatus(null);
        assertTrue(test.getStatus() == null);
    }

    @Test
    public void link(){
        Link test = new Link();
        test.setAccept(null);
        assertTrue(test.getAccept() == null);
        test.setAllow(null);
        assertTrue(test.getAllow() == null);
        test.setHref(null);
        assertTrue(test.getHref() == null);
        test.setHreflang(null);
        assertNull(test.getHreflang());
        test.setOffset(0);
        assertTrue(test.getOffset() == 0);
        test.setTemplate(null);
        assertNull(test.getTemplate());
        test.setTitle(null);
        assertNull(test.getTitle());
        test.setType(null);
        assertNull(test.getType());
    }

    @Test
    public void healthConfig(){
        HealthConfig test = new HealthConfig();
        test.setBaseUri(null);
        assertNull(test.getBaseUri());
        test.setBuildDate(null);
        assertNull(test.getBuildDate());
        test.setBuildVersion(null);
        assertNull(test.getBuildVersion());
        test.setCatalinaBase(null);
        assertNull(test.getCatalinaBase());
        test.setHostname(null);
        assertNull(test.getHostname());
        test.setJavaRuntimeVersion(null);
        assertNull(test.getJavaRuntimeVersion());
        test.setJavaSpecificationVendor(null);
        assertNull(test.getJavaSpecificationVendor());
        test.setJavaVersion(null);
        assertNull(test.getJavaVersion());
        test.setOriginalPath(null);
        assertNull(test.getOriginalPath());
        test.setOsArch(null);
        assertNull(test.getOsArch());
        test.setOsName(null);
        assertNull(test.getOsName());
        test.setOsVersion(null);
        assertNull(test.getOsVersion());
        test.setPlatformVersion(null);
        assertNull(test.getPlatformVersion());
        test.setServiceConfigurationName(null);
        assertNull(test.getServiceConfigurationName());
        test.setSystemName(null);
        assertNull(test.getSystemName());
        test.setUsername(null);
        assertNull(test.getUsername());
        test.setxPart1(null);
        assertNull(test.getxPart1());
        test.setxPart2(null);
        assertNull(test.getxPart2());
    }

    @Test
    public void feedbackinfo(){
        FeedbackInfo test = new FeedbackInfo();
        test.setDetails(null);
        assertNull(test.getDetails());
        test.setPlace(null);
        assertNull(test.getPlace());
        test.setResolution(null);
        assertNull(test.getResolution());
        test.setStatus(null);
        assertNull(test.getStatus());
    }

    @Test
    public void feature(){
        Feature test = new Feature();
        test.setActivationDate(0);
        assertTrue(test.getActivationDate() == 0);
        test.setDescription(null);
        assertNull(test.getDescription());
        test.setEnabled(true);
        assertTrue(test.isEnabled());
        test.setName(null);
        assertNull(test.getName());
    }

    @Test
    public void error(){
        Error error = new Error();
        error.setCode(0);
        assertTrue(error.getCode() == 0);
        error.setLabel(null);
        assertNull(error.getLabel());
        error.setMessage(null);
        assertNull(error.getMessage());
        error.setStacktrace(null);
        assertNull(error.getStacktrace());
    }

    @Test
    public void conceptAttribute(){
        ConceptAttribute test = new ConceptAttribute();
        test.setId(null);
        assertNull(test.getId());
        test.setName(null);
        assertNull(test.getName());
        test.setValue(null);
        assertNull(test.getValue());
    }

    @Test
    public void changeInfo(){
        ChangeInfo test = new ChangeInfo();
        test.setObjectModifier(null);
        assertNull(test.getObjectModifier());
        test.setObjectType(null);
        assertNull(test.getObjectType());
        test.setOperation(null);
        assertNull(test.getOperation());
        test.setOriginal(null);
        assertNull(test.getOriginal());
        test.setParent(null);
        assertNull(test.getParent());
        test.setReason(null);
        assertNull(test.getReason());
        test.setRemoved(null);
        assertNull(test.getRemoved());
        test.setResulting(null);
        assertNull(test.getResulting());
    }

    @Test
    public void artifactMetadata(){
        ArtifactMetadata test = new ArtifactMetadata();
        test.setEditable(true);
        assertTrue(test.isEditable());
        test.setFilename(null);
        assertNull(test.getFilename());
        test.setHeight(0);
        assertTrue(test.getHeight() == 0);
        test.setQualifiers(null);
        assertNull(test.getQualifiers());
        test.setScreeningState(null);
        assertNull(test.getScreeningState());
        test.setSize(0);
        assertTrue(test.getSize() == 0);
        test.setWidth(0);
        assertTrue(test.getWidth() == 0);
    }

    @Test
    public void textValue(){
        TextValue test = new TextValue();
        test.setLang(null);
        assertNull(test.getLang());
        test.setValue(null);
        assertNull(test.getValue());
        String s = test.toString();
        assertTrue(s.equals("{\n" +
                "     lang: null\n" +
                "     value: null\n" +
                "}\n"));
    }

    @Test
    public void agentName(){
        AgentName test = new AgentName();
        test.setType(null);
        assertNull(test.getType());
    }

    @Test
    public void placeReference(){
        PlaceReference test = new PlaceReference();
        test.setDescription(null);
        assertNull(test.getDescription());
        test.setNormalized(null);
        assertNull(test.getNormalized());
        test.setOriginal(null);
        assertNull(test.getOriginal());
    }

    @Test
    public void placeDisplayProperties(){
        PlaceDisplayProperties test = new PlaceDisplayProperties();
        test.setFullName(null);
        assertNull(test.getFullName());
        test.setName(null);
        assertNull(test.getName());
        test.setType(null);
        assertNull(test.getType());
    }

    @Test
    public void onlineAccount(){
        OnlineAccount test = new OnlineAccount();
        test.setAccountName(null);
        assertNull(test.getAccountName());
        test.setServiceHomepage(null);
        assertNull(test.getServiceHomepage());
    }

    @Test
    public void namePart(){
        NamePart test = new NamePart();
        test.setQualifiers(null);
        assertNull(test.getQualifiers());
        test.setType(null);
        assertNull(test.getType());
        test.setValue(null);
        assertNull(test.getValue());
    }

    @Test
    public void nameForm(){
        NameForm test = new NameForm();
        test.setFullText(null);
        assertNull(test.getFullText());
        test.setLang(null);
        assertNull(test.getLang());
        test.setNameFormInfo(null);
        assertNull(test.getNameFormInfo());
        test.setParts(null);
        assertNull(test.getParts());
    }

    @Test
    public void extensibleData(){
        ExtensibleData test = new ExtensibleData();
        test.setId(null);
        assertNull(test.getId());
    }

    @Test
    public void displayProperties(){
        DisplayProperties test = new DisplayProperties();
        test.setAscendancyNumber(null);
        assertNull(test.getAscendancyNumber());
        test.setBirthDate(null);
        assertNull(test.getBirthDate());
        test.setBirthPlace(null);
        assertNull(test.getBirthPlace());
        test.setDeathDate(null);
        assertNull(test.getDeathDate());
        test.setDeathPlace(null);
        assertNull(test.getDeathPlace());
        test.setDescendancyNumber(null);
        assertNull(test.getDescendancyNumber());
        test.setFamiliesAsChild(null);
        assertNull(test.getFamiliesAsChild());
        test.setFamiliesAsParent(null);
        assertNull(test.getFamiliesAsParent());
        test.setGender(null);
        assertNull(test.getGender());
        test.setLifespan(null);
        assertNull(test.getLifespan());
        test.setMarriageDate(null);
        assertNull(test.getMarriageDate());
        test.setMarriagePlace(null);
        assertNull(test.getMarriagePlace());
        test.setName(null);
        assertNull(test.getName());

        String s = test.toString();
        assertTrue(s.equals("{\n" +
                "     name: null\n" +
                "     gender: null\n" +
                "     lifespan: null\n" +
                "     birthDate: null\n" +
                "     birthPlace: null\n" +
                "     deathDate: null\n" +
                "     deathPlace: null\n" +
                "     marriageDate: null\n" +
                "     marriagePlace: null\n" +
                "     ascendancyNumber: null\n" +
                "     descendancyNumber: null\n" +
                "}\n"));
    }

    @Test
    public void date(){
        Date test = new Date();
        test.setFormal(null);
        assertNull(test.getFormal());
        test.setNormalized(null);
        assertNull(test.getNormalized());
        test.setOriginal(null);
        assertNull(test.getOriginal());
    }

    @Test
    public void attribution(){
        Attribution test = new Attribution();
        test.setChangeMessage(null);
        assertNull(test.getChangeMessage());
        test.setContributor(null);
        assertNull(test.getContributor());
        test.setCreated(0);
        assertTrue(test.getCreated() == 0);
        test.setCreator(null);
        assertNull(test.getCreator());
        test.setModified(0);
        assertTrue(test.getModified() == 0);
    }

    @Test
    public void address(){
        Address test = new Address();
        test.setCity(null);
        assertNull(test.getCity());
        test.setCountry(null);
        assertNull(test.getCountry());
        test.setPostalCode(null);
        assertNull(test.getPostalCode());
        test.setStateOrProvince(null);
        assertNull(test.getStateOrProvince());
        test.setStreet(null);
        assertNull(test.getStreet());
        test.setStreet2(null);
        assertNull(test.getStreet2());
        test.setStreet3(null);
        assertNull(test.getStreet3());
        test.setStreet4(null);
        assertNull(test.getStreet4());
        test.setStreet5(null);
        assertNull(test.getStreet5());
        test.setStreet6(null);
        assertNull(test.getStreet6());
        test.setValue(null);
        assertNull(test.getValue());
    }

    @Test
    public void userMessageThreadsSummary(){
        UserMessageThreadsSummary test = new UserMessageThreadsSummary();
        test.setMessageCount(0);
        assertTrue(test.getMessageCount() == 0);
        test.setThreadCount(0);
        assertTrue(test.getThreadCount() == 0);
        test.setUnreadMessageCount(0);
        assertTrue(test.getUnreadMessageCount() == 0);
        test.setUnreadThreadCount(0);
        assertTrue(test.getUnreadThreadCount() == 0);
        test.setUserId(null);
        assertNull(test.getUserId());
        test.setUserMessageThreadSummaries(null);
        assertNull(test.getUserMessageThreadSummaries());
    }

    @Test
    public void user(){
        User test = new User();
        test.setAlternateEmail(null);
        assertNull(test.getAlternateEmail());
        test.setBirthDate(null);
        assertNull(test.getBirthDate());
        test.setContactName(null);
        assertNull(test.getContactName());
        test.setCountry(null);
        assertNull(test.getCountry());
        test.setDisplayName(null);
        assertNull(test.getDisplayName());
        test.setEmail(null);
        assertNull(test.getEmail());
        test.setFamilyName(null);
        assertNull(test.getFamilyName());
        test.setFullName(null);
        assertNull(test.getFullName());
        test.setGender(null);
        assertNull(test.getGender());
        test.setGivenName(null);
        assertNull(test.getGivenName());
        test.setHelperAccessPin(null);
        assertNull(test.getHelperAccessPin());
        test.setMailingAddress(null);
        assertNull(test.getMailingAddress());
        test.setMobilePhoneNumber(null);
        assertNull(test.getMobilePhoneNumber());
        test.setPersonId(null);
        assertNull(test.getPersonId());
        test.setPhoneNumber(null);
        assertNull(test.getPhoneNumber());
        test.setPreferredLanguage(null);
        assertNull(test.getPreferredLanguage());
        test.setTreeUserId(null);
        assertNull(test.getTreeUserId());
    }

    @Test
    public void term(){
        Term test = new Term();
        test.setConcept(null);
        assertNull(test.getConcept());
        test.setSublist(null);
        assertNull(test.getSublist());
        test.setSublistPosition(0);
        assertTrue(test.getSublistPosition() == 0);
        test.setType(null);
        assertNull(test.getType());
        test.setValues(null);
        assertNull(test.getValues());
    }

    @Test
    public void sourceReference(){
        SourceReference test = new SourceReference();
        test.setAttribution(null);
        assertNull(test.getAttribution());
        test.setDescription(null);
        assertNull(test.getDescription());
        test.setDescriptionId(null);
        assertNull(test.getDescriptionId());
        test.setQualifiers(null);
        assertNull(test.getQualifiers());
        test.setTags(null);
        assertNull(test.getTags());
    }

    @Test
    public void sourceDescription(){
        SourceDescription test = new SourceDescription();
        test.setAbout(null);
        assertNull(test.getAbout());
        test.setAnalysis(null);
        assertNull(test.getAnalysis());
        test.setAttribution(null);
        assertNull(test.getAttribution());
        test.setCitations(null);
        assertNull(test.getCitations());
        test.setComponentOf(null);
        assertNull(test.getComponentOf());
        test.setIdentifiers(null);
        assertNull(test.getIdentifiers());
        test.setMediator(null);
        assertNull(test.getMediator());
        test.setNotes(null);
        assertNull(test.getNotes());
        test.setPublisher(null);
        assertNull(test.getPublisher());
        test.setReplacedBy(null);
        assertNull(test.getReplacedBy());
        test.setReplaces(null);
        assertNull(test.getReplaces());
        test.setRights(null);
        assertNull(test.getRights());
        test.setSources(null);
        assertNull(test.getSources());
        test.setStatuses(null);
        assertNull(test.getStatuses());
        test.setTitles(null);
        assertNull(test.getTitles());
    }

    @Test
    public void sourceCitation(){
        SourceCitation test = new SourceCitation();
        test.setCitationTemplate(null);
        assertNull(test.getCitationTemplate());
        test.setLang(null);
        assertNull(test.getLang());
        test.setValue(null);
        assertNull(test.getValue());
        test.setFields(null);
        assertNull(test.getFields());
    }

    @Test
    public void note(){
        Note test = new Note();
        test.setAttribution(null);
        assertNull(test.getAttribution());
        test.setLang(null);
        assertNull(test.getLang());
        test.setSubject(null);
        assertNull(test.getSubject());
        test.setText(null);
        assertNull(test.getText());
    }

    @Test
    public void message(){
        Message test = new Message();
        test.setAuthor(null);
        assertNull(test.getAuthor());
        test.setBody(null);
        assertNull(test.getBody());
        test.setCreated(0);
        assertTrue(test.getCreated() == 0);
    }

    @Test
    public void hypermediaEnabledData(){
        HypermediaEnabledData test = new HypermediaEnabledData();
        test.setLinks(null);
        assertNull(test.getLinks());
    }

    @Test
    public void familyView(){
        FamilyView test = new FamilyView();
        test.setChildren(null);
        assertNull(test.getChildren());
        test.setParent1(null);
        assertNull(test.getParent1());
        test.setParent2(null);
        assertNull(test.getParent2());
    }

    @Test
    public void evidenceReference(){
        EvidenceReference test = new EvidenceReference();
        test.setAttribution(null);
        assertNull(test.getAttribution());
        test.setResource(null);
        assertNull(test.getResource());
        test.setResourceId(null);
        assertNull(test.getResourceId());
    }

    @Test
    public void discussionReference(){
        DiscussionReference test = new DiscussionReference();
        test.setAttribution(null);
        assertNull(test.getAttribution());
        test.setResource(null);
        assertNull(test.getResource());
        test.setResourceId(null);
        assertNull(test.getResourceId());
    }

    @Test
    public void discussion(){
        Discussion test = new Discussion();
        test.setComments(null);
        assertNull(test.getComments());
        test.setContributor(null);
        assertNull(test.getContributor());
        test.setCreated(0);
        assertTrue(test.getCreated() == 0);
        test.setDetails(null);
        assertNull(test.getDetails());
        test.setModified(0);
        assertTrue(test.getModified() == 0);
        test.setNumberOfComments(0);
        assertTrue(test.getNumberOfComments() == 0);
        test.setTitle(null);
        assertNull(test.getTitle());
    }

    @Test
    public void coverage(){
        Coverage test = new Coverage();
        test.setSpatial(null);
        assertNull(test.getSpatial());
        test.setTemporal(null);
        assertNull(test.getTemporal());
    }

    @Test
    public void concept(){
        Concept test = new Concept();
        test.setAttributes(null);
        assertNull(test.getAttributes());
        test.setDescription(null);
        assertNull(test.getDescription());
        test.setGedcomxUri(null);
        assertNull(test.getGedcomxUri());
        test.setNote(null);
        assertNull(test.getNote());
        test.setTerms(null);
        assertNull(test.getTerms());
    }

    @Test
    public void comment(){
        Comment test = new Comment();
        test.setContributor(null);
        assertNull(test.getContributor());
        test.setCreated(0);
        assertTrue(test.getCreated() == 0);
        test.setText(null);
        assertNull(test.getText());
    }

    @Test
    public void agent(){
        Agent test = new Agent();
        test.setAccounts(null);
        assertNull(test.getAccounts());
        test.setAddresses(null);
        assertNull(test.getAddresses());
        test.setEmails(null);
        assertNull(test.getEmails());
        test.setHomepage(null);
        assertNull(test.getHomepage());
        test.setIdentifiers(null);
        assertNull(test.getIdentifiers());
        test.setNames(null);
        assertNull(test.getNames());
        test.setOpenid(null);
        assertNull(test.getOpenid());
        test.setPhones(null);
        assertNull(test.getPhones());
    }

    @Test
    public void gedcomx(){
        Gedcomx test = new Gedcomx();
        test.setAgents(null);
        assertNull(test.getAgents());
        test.setAttribution(null);
        assertNull(test.getAttribution());
        test.setPersons(null);
        assertNull(test.getPersons());
        test.setPlaces(null);
        assertNull(test.getPlaces());
        test.setRelationships(null);
        assertNull(test.getRelationships());
        test.setSourceDescriptions(null);
        assertNull(test.getSourceDescriptions());
        test.setDocuments(null);
        assertNull(test.getDocuments());
        test.setEvents(null);
        assertNull(test.getEvents());
    }

    @Test
    public void familySearchPlatform(){
        FamilySearchPlatform test = new FamilySearchPlatform();
        test.setChildAndParentsRelationships(null);
        assertNull(test.getChildAndParentsRelationships());
        test.setDescription(null);
        assertNull(test.getDescription());
        test.setDiscussions(null);
        assertNull(test.getDiscussions());
        test.setFeatures(null);
        assertNull(test.getFeatures());
        test.setMergeAnalyses(null);
        assertNull(test.getMergeAnalyses());
        test.setMerges(null);
        assertNull(test.getMerges());
        test.setUserMessageThreadSummaries(null);
        assertNull(test.getUserMessageThreadSummaries());
        test.setUsers(null);
        assertNull(test.getUsers());

        assertTrue(test.hashCode() == 0);
        String s = test.toString();
        assertTrue(s.equals("{\n" +
                "     id: null\n" +
                "     persons: [\n" +
                "     ]\n" +
                "    childAndParentsRelationships: [\n" +
                "     ]\n" +
                "}\n"));
    }

    @Test
    public void name(){
        Name test = new Name();
        test.setDate(null);
        assertNull(test.getDate());
        test.setNameForms(null);
        assertNull(test.getNameForms());
        test.setPreferred(true);
        assertTrue(test.isPreferred());
        test.setType(null);
        assertNull(test.getType());

        String s = test.toString();
        assertTrue(s.equals("{\n" +
                "     type: null\n" +
                "     preferred: true\n" +
                "     date: null\n" +
                "     nameForms: [\n" +
                "     ]\n" +
                "}\n"));
    }

    @Test
    public void gender(){
        Gender test = new Gender();
        test.setType(null);
        assertNull(test.getType());
        String s = test.toString();
        assertTrue(s.equals("{     type: null }\n"));
    }

    @Test
    public void fact(){
        Fact test = new Fact();
        test.setDate(null);
        assertNull(test.getDate());
        test.setPlace(null);
        assertNull(test.getPlace());
        test.setQualifiers(null);
        assertNull(test.getQualifiers());
        test.setType(null);
        assertNull(test.getType());
        test.setValue(null);
        assertNull(test.getValue());

        String s = test.toString();
        assertTrue(s.equals("{\n" +
                "     type: null\n" +
                "     date: null\n" +
                "     place: null\n" +
                "     value: null\n" +
                "}\n"));
    }

    @Test
    public void conclusion(){
        Conclusion test = new Conclusion();
        test.setAnalysis(null);
        assertNull(test.getAnalysis());
        test.setAttribution(null);
        assertNull(test.getAttribution());
        test.setNotes(null);
        assertNull(test.getNotes());
        test.setSortKey(null);
        assertNull(test.getSortKey());
        test.setSources(null);
        assertNull(test.getSources());
    }

    @Test
    public void subject(){
        Subject test = new Subject();
        test.setEvidence(null);
        assertNull(test.getEvidence());
        test.setExtracted(true);
        assertTrue(test.isExtracted());
        test.setIdentifiers(null);
        assertNull(test.getIdentifiers());
        test.setMedia(null);
        assertNull(test.getMedia());
    }

    @Test
    public void relationship(){
        Relationship test = new Relationship();
        test.setFacts(null);
        assertNull(test.getFacts());
        test.setPerson1(null);
        assertNull(test.getPerson1());
        test.setPerson2(null);
        assertNull(test.getPerson2());
        test.setType(null);
        assertNull(test.getType());
    }

    @Test
    public void placeDescription(){
        PlaceDescription test = new PlaceDescription();
        test.setDisplay(null);
        assertNull(test.getDisplay());
        test.setJurisdiction(null);
        assertNull(test.getJurisdiction());
        test.setLatitude(0);
        assertTrue(test.getLatitude() == 0);
        test.setLongitude(0);
        assertTrue(test.getLongitude() == 0);
        test.setNames(null);
        assertNull(test.getNames());
        test.setPlace(null);
        assertNull(test.getPlace());
        test.setSpatialDescription(null);
        assertNull(test.getSpatialDescription());
        test.setTemporalDescription(null);
        assertNull(test.getTemporalDescription());
    }

    @Test
    public void person(){
        Person test = new Person();
        test.setGender(null);
        assertNull(test.getGender());
        test.setDiscussionReferences(null);
        assertNull(test.getDiscussionReferences());
        test.setDisplay(null);
        assertNull(test.getDisplay());
        test.setFacts(null);
        assertNull(test.getFacts());
        test.setLiving(true);
        assertTrue(test.isLiving());
        test.setNames(null);
        assertNull(test.getNames());
        test.setPrivate(true);
        assertTrue(test.isPrivate());

        assertTrue(test.equals(new Person()));
        String t = "hi";
        test.setId(t);
        assertTrue(test.hashCode() == t.hashCode());

        String s = test.toString();
        assertTrue(s.equals("{\n" +
                "     id: hi\n" +
                "     sortKey: null\n" +
                "     extracted: false\n" +
                "     living: true\n" +
                "     private: true\n" +
                "     gender: null\n" +
                "     name: [\n" +
                "null\n" +
                "     ]\n" +
                "     facts:[\n" +
                "null\n" +
                "     ]\n" +
                "     display: null\n" +
                "}\n"));
    }

    @Test
    public void childAndParentsRelationship(){
        ChildAndParentsRelationship test = new ChildAndParentsRelationship();
        test.setChild(null);
        assertNull(test.getChild());
        test.setFather(null);
        assertNull(test.getFather());
        test.setFatherFacts(null);
        assertNull(test.getFatherFacts());
        test.setMother(null);
        assertNull(test.getMother());
        test.setMotherFacts(null);
        assertNull(test.getMotherFacts());

        String s = test.toString();
        assertTrue(s.equals("[]"));
    }

    @Test
    public void reservation(){
        Reservation test = new Reservation();
        test.setAsignee(null);
        assertNull(test.getAsignee());
        test.setFather(null);
        assertNull(test.getFather());
        test.setMother(null);
        assertNull(test.getMother());
        test.setOrdinanceType(null);
        assertNull(test.getOrdinanceType());
        test.setSpouse(null);
        assertNull(test.getSpouse());
        test.setStatus(null);
        assertNull(test.getStatus());
        test.setType(null);
        assertNull(test.getType());
    }

    @Test
    public void userMessageThreadSummary(){
        UserMessageThreadSummary test = new UserMessageThreadSummary();
        test.setMessageCount(0);
        assertTrue(test.getMessageCount() == 0);
        test.setUnreadMessageCount(0);
        assertTrue(test.getUnreadMessageCount() == 0);
        test.setUserId(null);
        assertNull(test.getUserId());
    }

    @Test
    public void messageThread(){
        MessageThread test = new MessageThread();
        test.setMessages(null);
        assertNull(test.getMessages());
    }

    @Test
    public void abstractMessageThread(){
        AbstractMessageThread test = new AbstractMessageThread();
        test.setAbout(null);
        assertNull(test.getAbout());
        test.setAboutUri(null);
        assertNull(test.getAboutUri());
        test.setAuthor(null);
        assertNull(test.getAuthor());
        test.setLastModified(0);
        assertTrue(test.getLastModified() == 0);
        test.setParticipants(null);
        assertNull(test.getParticipants());
        test.setSubject(null);
        assertNull(test.getSubject());
    }

    @Test
    public void ordinance(){
        Ordinance test = new Ordinance();
        test.setDate(null);
        assertNull(test.getDate());
        test.setLiving(true);
        assertTrue(test.isLiving());
        test.setTempleCode(null);
        assertNull(test.getTempleCode());
    }
}