package checkerImplementations.importer.organization;

import checkerImplementations.importer.familySearchModel.extensibleData.hypermediaEnabledData.gedcomx.FamilySearchPlatform;
import dummy.FamilySearchFactory;
import org.junit.Test;

import static org.junit.Assert.*;

public class FamilyTreeNodeTest {

    /**
     * Generates a FamilySearchPlatform for testing.
     */
    private FamilySearchPlatform getDetails(String pid, boolean isMale){
        return new FamilySearchFactory().generateDetails(pid, isMale);
    }

    /**
     * Ensures that cloning a FamilyTreeNode works and information is maintained.
     */
    @Test
    public void cloneTest(){
        FamilySearchPlatform details = getDetails("test", true);
        FamilyTreeNode original = new FamilyTreeNode(details,2);
        FamilyTreeNode clone = new FamilyTreeNode(original);
        assertTrue(clone.getPid().equals("test"));
        assertTrue(clone.getGeneration() == 2);
    }

    /**
     * Ensures that merging FamilyTreeNodes correctly merges
     * their data.
     */
    @Test
    public void merge() {
        FamilySearchPlatform details = getDetails("woman", false);
        FamilyTreeNode original = new FamilyTreeNode(details, 1);
        FamilyTreeNode second = new FamilyTreeNode(details, 1);
        second.addChildPid("child","http://gedcomx.org/BiologicalParent");
        second.addSpousePid("spouse");
        second.setAncestor(true);
        second.addFatherPid("father", "Biological");
        second.addMotherPid("mother", "Biological");
        original.merge(second);
        assertTrue(original.getChildrenPids().size() == 1);
        assertTrue(original.getSpousePids().size() == 1);

        assertTrue(original.isParentOf("child"));
        assertTrue(!original.isParentOf("spoon"));
        assertTrue(original.isSpouseOf("spouse"));
        assertTrue(!original.isSpouseOf("spoon"));
        assertTrue(original.isAncestor());
        assertTrue(original.getMotherPids().contains("mother"));
        assertTrue(original.getFatherPids().contains("father"));
    }

    /**
     * Ensures that toString() works
     */
    @Test
    public void toStringTest() {
        FamilySearchPlatform details = getDetails("person", false);
        FamilyTreeNode t = new FamilyTreeNode(details, 1);
        String s = t.toString();
        assertTrue(s.equals("[Pid: person Fathers: [] Mothers: [] Children: []]"));
    }

    /**
     * Ensures that hashCode() works
     */
    @Test
    public void hashCodeTest() {
        String id = "person";
        FamilySearchPlatform details = getDetails(id, false);
        FamilyTreeNode t = new FamilyTreeNode(details, 1);
        assertTrue(id.hashCode() == t.hashCode());
    }

    /**
     * Ensures that equals() returns true when nodes have the same details and generation.
     */
    @Test
    public void equals() {
        FamilySearchPlatform details = getDetails("person", false);
        FamilyTreeNode t = new FamilyTreeNode(details, 1);
        FamilyTreeNode o = new FamilyTreeNode(details, 1);
        assertTrue(t.equals(o));
    }

    /**
     * Ensures that getChildRelationshipType() works properly
     */
    @Test
    public void getChildRelationshipType() {
        FamilySearchPlatform details = getDetails("person", false);
        FamilyTreeNode test = new FamilyTreeNode(details, 1);
        assertNull(test.getChildRelationshipType("tim"));
    }
}