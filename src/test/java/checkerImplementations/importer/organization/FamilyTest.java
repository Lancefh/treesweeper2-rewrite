package checkerImplementations.importer.organization;

import checkerImplementations.importer.familySearchModel.extensibleData.hypermediaEnabledData.gedcomx.FamilySearchPlatform;
import dummy.FamilySearchFactory;
import org.junit.Test;

import static org.junit.Assert.*;

public class FamilyTest {

    /**
     * Generates a family search platform for testing.
     */
    private FamilySearchPlatform generateDetails(String pid, boolean isMale){
        FamilySearchFactory factory = new FamilySearchFactory();
        return factory.generateDetails(pid, isMale);
    }

    /**
     * Ensures that addSpouse() works
     */
    @Test
    public void addSpouse() {
        FamilyTreeNode ancestor = new FamilyTreeNode(generateDetails("ancestor", true),0);
        Family test = new Family(ancestor);
        FamilyTreeNode spouse = new FamilyTreeNode(generateDetails("spouse", false), 0);
        test.addSpouse(spouse);
        assertTrue(test.getSpouses().size() == 1);
    }

    /**
     * Ensures that addChild() works
     */
    @Test
    public void addChild() {
        FamilyTreeNode ancestor = new FamilyTreeNode(generateDetails("ancestor", false), 0);
        Family test = new Family(ancestor);
        FamilyTreeNode child = new FamilyTreeNode(generateDetails("child", true), -1);
        test.addChild(child,new ParentInfo("http://gedcomx.org/BiologicalParent","Father"));
        assertTrue(test.getChildren().size() == 1);
    }

    /**
     * Ensures that getAncestor() works
     */
    @Test
    public void getAncestor() {
        FamilyTreeNode ancestor = new FamilyTreeNode(generateDetails("ancestor", false), 0);
        Family test = new Family(ancestor);
        assertTrue(test.getAncestor().getPid().equals("ancestor"));
    }

    /**
     * Ensures that addParent() works
     */
    @Test
    public void addParent() {
        FamilyTreeNode ancestor = new FamilyTreeNode(generateDetails("ancestor", false), 0);
        Family test = new Family(ancestor);
        FamilyTreeNode parent = new FamilyTreeNode(generateDetails("parent", true), 1);
        test.addParent(parent, new ParentInfo("http://gedcomx.org/BiologicalParent","Mother"));
        assertTrue(test.getParents().size() == 1);
    }
}