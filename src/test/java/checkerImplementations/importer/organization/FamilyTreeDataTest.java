package checkerImplementations.importer.organization;

import checkerImplementations.importer.familySearchModel.extensibleData.hypermediaEnabledData.gedcomx.FamilySearchPlatform;
import dummy.FamilySearchFactory;
import org.junit.Test;

import static org.junit.Assert.*;

public class FamilyTreeDataTest {

    /**
     * Generates a FamilySearchPlatform for testing purposes.
     */
    private FamilySearchPlatform getDetails(String pid, boolean isMale){
        FamilySearchFactory factory = new FamilySearchFactory();
        return factory.generateDetails(pid, isMale);
    }

    /**
     * Generates a FamilyTreeData for testing
     */
    private FamilyTreeData generateTestSubject(){
        FamilySearchPlatform personDetails = getDetails("person", false);
        FamilyTreeNode person = new FamilyTreeNode(personDetails, 1);
        FamilyTreeData tree = new FamilyTreeData(person);

        FamilySearchPlatform spouseDetails = getDetails("spouse", true);
        FamilyTreeNode spouse = new FamilyTreeNode(spouseDetails, 1);
        person.addSpousePid("spouse");
        spouse.addSpousePid("person");
        tree.addPerson(spouse);

        FamilySearchPlatform childDetails = getDetails("child", false);
        FamilyTreeNode child = new FamilyTreeNode(childDetails, 0);
        person.addChildPid("child", "http://gedcomx.org/BiologicalParent");
        spouse.addChildPid("child", "http://gedcomx.org/BiologicalParent");
        child.addMotherPid("person", "Biological");
        child.addFatherPid("spouse", "Biological");
        tree.addPerson(child);

        return tree;
    }

    /**
     * Ensures that getFatherOfPerson() works
     */
    @Test
    public void getFatherOfPerson() {
        FamilyTreeData tree = generateTestSubject();
        assertTrue(tree.getFathersOfPerson("child").size() == 1);
    }

    /**
     * Ensures that getMotherOfPerson() works
     */
    @Test
    public void getMotherOfPerson() {
        FamilyTreeData tree = generateTestSubject();
        assertTrue(tree.getMothersOfPerson("child").size() == 1);
    }

    /**
     * Ensures that getChildrenOfPerson() works
     */
    @Test
    public void getChildrenOfPerson() {
        FamilyTreeData tree = generateTestSubject();
        assertTrue(tree.getChildrenOfPerson("person").size() == 1);
    }

    /**
     * Ensures that getSpousesOfPerson() works
     */
    @Test
    public void getSpousesOfPerson() {
        FamilyTreeData tree = generateTestSubject();
        assertTrue(tree.getSpousesOfPerson("person").size() == 1);
    }

    /**
     * Ensures that getRootPerson() works
     */
    @Test
    public void getRootPerson() {
        FamilyTreeData tree = generateTestSubject();
        assertTrue(tree.getRootPerson().getPid().equals("person"));
    }

    /**
     * Ensures that getPerson() works
     */
    @Test
    public void getPerson() {
        FamilyTreeData tree = generateTestSubject();
        assertTrue(tree.getPerson("child") != null);
    }

    /**
     * Ensures that getRootPersonPid() works
     */
    @Test
    public void getRootPersonPid() {
        FamilyTreeData tree = generateTestSubject();
        assertTrue(tree.getRootPersonPid().equals("person"));

        tree.setRootPersonPid("steve");
        assertTrue(tree.getRootPersonPid().equals("steve"));
    }

    /**
     * Ensures that getAllNodes() works
     */
    @Test
    public void getAllNodes() {
        FamilyTreeData tree = generateTestSubject();
        assertTrue(tree.getAllNodes().size() == 3);
        assertTrue(tree.size() == 3);
    }

    /**
     * Ensures that an empty FamilyTreeData getRootPersonPid() returns null
     */
    @Test
    public void lame(){
        FamilyTreeData test = new FamilyTreeData();
        assertTrue(test.getRootPersonPid() == null);
    }
}