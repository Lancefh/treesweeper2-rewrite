package checkerImplementations.importer;

import dummy.DummyThread;
import dummy.TestUpdater;
import org.junit.Test;

import java.util.Iterator;

import static org.junit.Assert.*;

public class ImportThreadManagerTest {

    /**
     * Ensures that printStatus() works.
     */
    @Test
    public void printStatus() throws Exception{
        ImportThreadManager test = new ImportThreadManager(10,false,false);
        TestUpdater updater = new TestUpdater(false);
        TestUpdater.BREAK_FLAG = false;
        test.setUpdater(updater);
        DummyThread thread = new DummyThread();
        thread.start();
        test.addThread(thread,false,true);
        thread.join();
        test.terminateUpdates();
    }

    /**
     * Ensures that naturally stopping printing updates works.
     */
    @Test
    public void naturalStop() throws Exception{
        ImportThreadManager test = new ImportThreadManager(10, false, true);
        TestUpdater updater = new TestUpdater(false);
        TestUpdater.BREAK_FLAG = false;
        test.setUpdater(updater);
        DummyThread thread = new DummyThread();
        thread.setName("natural_stop");
        thread.start();
        test.addThread(thread, false, true);
        thread.join();
        Iterator<IImportThread> iterator = test.iterator();
        assertTrue(iterator.hasNext());
    }
}