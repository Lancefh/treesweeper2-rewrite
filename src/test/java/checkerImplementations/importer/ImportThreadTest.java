package checkerImplementations.importer;

import checkerImplementations.importer.organization.Family;
import injector.DependencyInjector;
import org.junit.Test;

import static org.junit.Assert.*;

public class ImportThreadTest {

    /**
     * Ensures that various ImportThread methods work correctly.
     */
    @Test
    public void start() throws Exception{
        DependencyInjector.get().setProductionMode(false);

        ImportThread test = new ImportThread("ancestor", "token", 7);
        test.setDetails("test");
        String status = test.getStatus();
        assertTrue(status.equals("ancestor:Created:DETAILS=test"));
        test.setName("name");
        assertTrue(test.getName().equals("name"));
        test.setThrottled(10);
        assertTrue(test.getThrottled() == 10);
        assertNull(test.getState());
        assertNull(test.getError());

        test.start();
        test.join();
        Family result = test.getFamily();
        assertNotNull(result);
        assertTrue(result.getAncestor().getPid().equals("ancestor"));
        assertTrue(result.getChildren().size() == 1);
        assertTrue(result.getSpouses().size() == 1);

        DependencyInjector.get().setProductionMode(true);
        assertTrue(DependencyInjector.get().isProductionMode());
    }

    /**
     * Ensures that when an error is thrown that it is saved correctly.
     */
    @Test
    public void missingRootDetails() throws Exception{
        DependencyInjector.get().setProductionMode(false);

        ImportThread test = new ImportThread("missing", "token", 7);
        test.start();
        test.join();
        assertNotNull(test.getError());
        assertTrue(test.getError().getMessage().equals("Pid: missing, root details was null."));

        DependencyInjector.get().setProductionMode(true);
        assertTrue(DependencyInjector.get().isProductionMode());
    }

    /**
     * Ensures that when the proxy throws a DataImportException that it is stored correctly.
     */
    @Test
    public void dataException() throws Exception{
        DependencyInjector.get().setProductionMode(false);

        ImportThread test = new ImportThread("broken", "token", 7);
        test.start();
        test.join();
        assertNotNull(test.getError());
        assertTrue(test.getError().getMessage().equals("haha"));
        test.kill();

        DependencyInjector.get().setProductionMode(true);
        assertTrue(DependencyInjector.get().isProductionMode());
    }

    /**
     * Ensures that the thread handles it correctly when the type of father is unknown
     */
    @Test
    public void unknownFatherType() throws Exception{
        DependencyInjector.get().setProductionMode(false);

        ImportThread test = new ImportThread("unknown_father", "token", 7);
        test.start();
        test.join();
        assertNull(test.getError());
        assertNotNull(test.getFamily());

        DependencyInjector.get().setProductionMode(true);
        assertTrue(DependencyInjector.get().isProductionMode());
    }

    /**
     * Ensures that the thread handles it correctly when the type of mother is known
     */
    @Test
    public void knownMotherType() throws Exception{
        DependencyInjector.get().setProductionMode(false);

        ImportThread test = new ImportThread("known_mother", "token", 7);
        test.start();
        test.join();
        assertNull(test.getError());
        assertNotNull(test.getFamily());

        DependencyInjector.get().setProductionMode(true);
        assertTrue(DependencyInjector.get().isProductionMode());

    }

    /**
     * Ensures that the thread handles it correctly when the type of mother is unknown
\     */
    @Test
    public void unknownMotherType() throws Exception{
        DependencyInjector.get().setProductionMode(false);

        ImportThread test = new ImportThread("unknown_mother", "token", 7);
        test.start();
        test.join();
        assertNull(test.getError());
        assertNotNull(test.getFamily());

        DependencyInjector.get().setProductionMode(true);
        assertTrue(DependencyInjector.get().isProductionMode());
    }

    /**
     * Ensures that the thread handles parent downloading correctly
     */
    @Test
    public void parents() throws Exception{
        DependencyInjector.get().setProductionMode(false);

        ImportThread test = new ImportThread("parents_test", "token", 7);
        test.start();
        test.join();
        assertNull(test.getError());
        assertNotNull(test.getFamily());

        DependencyInjector.get().setProductionMode(true);
        assertTrue(DependencyInjector.get().isProductionMode());
    }

}