package checkerImplementations.importer;

import checkerImplementations.importer.familySearchModel.extensibleData.Date;
import compileTester.ModelFactory;
import dummy.TestUpdater;
import injector.DependencyInjector;
import interfaces.DataImportException;
import org.junit.Test;
import osax.model.OSAXModel;
import server.ImportOptions;

import static org.junit.Assert.*;

public class FamilySearchImporterTest {
    private static final String MODEL_FILE = "model_and_configuration/FamilySearchModel.txt";

    /**
     * Ensures that getOptions() works
     */
    @Test
    public void getOptions() {
        FamilySearchImporter test = new FamilySearchImporter(generateOptions("standard"), null);
        assertTrue(test.getOptions().getPid().equals("pid"));
    }

    /**
     * Ensures that populating can go smoothly.
     */
    @Test
    public void successfulPopulate() throws Exception{
        // First, get the model
        ModelFactory factory = new ModelFactory();
        OSAXModel model = factory.generateModel(MODEL_FILE);
        assertNotNull(model);

        // Second, run the test
        DependencyInjector.get().setProductionMode(false);
        TestUpdater updater = new TestUpdater(false);
        TestUpdater.BREAK_FLAG = false;
        FamilySearchImporter test = new FamilySearchImporter(generateOptions("standard"), updater);
        test.populate(model);

        DependencyInjector.get().setProductionMode(true);
        assertTrue(DependencyInjector.get().isProductionMode());
    }

    /**
     * Ensures that a DataImportException is thrown if an updater throws an IOException
     */
    @Test
    public void populateBrokenSocket() throws Exception{
        // First, get the model
        ModelFactory factory = new ModelFactory();
        OSAXModel model = factory.generateModel(MODEL_FILE);
        assertNotNull(model);

        // Second, run the test
        DependencyInjector.get().setProductionMode(false);
        TestUpdater updater = new TestUpdater(true);
        TestUpdater.BREAK_FLAG = false;
        FamilySearchImporter test = new FamilySearchImporter(generateOptions("standard"), updater);
        try {
            test.populate(model);
            fail();
        }catch(DataImportException e){}

        DependencyInjector.get().setProductionMode(true);
        assertTrue(DependencyInjector.get().isProductionMode());
    }

    /**
     * Ensures that the importer handles it properly when a user is not logged in.
     */
    @Test
    public void notLoggedIn() throws Exception{
        // First, get the model
        ModelFactory factory = new ModelFactory();
        OSAXModel model = factory.generateModel(MODEL_FILE);
        assertNotNull(model);

        // Second, run the test
        DependencyInjector.get().setProductionMode(false);
        TestUpdater updater = new TestUpdater(false);
        TestUpdater.BREAK_FLAG = false;
        FamilySearchImporter test = new FamilySearchImporter(generateOptions("not_logged_in"), updater);
        test.populate(model);

        DependencyInjector.get().setProductionMode(true);
        assertTrue(DependencyInjector.get().isProductionMode());
    }

    /**
     * Ensures that when the user is not logged in and the updater throws an IOException that
     * a DataImportException is thrown.
     */
    @Test
    public void notLoggedInBroken() throws Exception{
        // First, get the model
        ModelFactory factory = new ModelFactory();
        OSAXModel model = factory.generateModel(MODEL_FILE);
        assertNotNull(model);

        // Second, run the test
        DependencyInjector.get().setProductionMode(false);
        TestUpdater updater = new TestUpdater(false);
        TestUpdater.BREAK_FLAG = true;
        FamilySearchImporter test = new FamilySearchImporter(generateOptions("not_logged_in"), updater);
        try {
            test.populate(model);
            fail();
        }catch(DataImportException e){}

        DependencyInjector.get().setProductionMode(true);
        assertTrue(DependencyInjector.get().isProductionMode());
    }

    /**
     * Ensures that when ancestry comes back null that a DataImportException is thrown.
     */
    @Test
    public void nullAncestry() throws Exception{
        // First, get the model
        ModelFactory factory = new ModelFactory();
        OSAXModel model = factory.generateModel(MODEL_FILE);
        assertNotNull(model);

        // Second, run the test
        DependencyInjector.get().setProductionMode(false);
        TestUpdater updater = new TestUpdater(false);
        TestUpdater.BREAK_FLAG = false;
        FamilySearchImporter test = new FamilySearchImporter(generateOptions("null_ancestry"), updater);
        try {
            test.populate(model);
            fail();
        }catch(DataImportException e){}

        DependencyInjector.get().setProductionMode(true);
        assertTrue(DependencyInjector.get().isProductionMode());
    }

    /**
     * Ensures that when family data comes back null a DataImportException is thrown.
     */
    @Test
    public void nullFamily() throws Exception{
        // First, get the model
        ModelFactory factory = new ModelFactory();
        OSAXModel model = factory.generateModel(MODEL_FILE);
        assertNotNull(model);

        // Second, run the test
        DependencyInjector.get().setProductionMode(false);
        TestUpdater updater = new TestUpdater(false);
        TestUpdater.BREAK_FLAG = false;
        FamilySearchImporter test = new FamilySearchImporter(generateOptions("null_family"), updater);
        try {
            test.populate(model);
            fail();
        }catch(DataImportException e){}

        DependencyInjector.get().setProductionMode(true);
        assertTrue(DependencyInjector.get().isProductionMode());
    }

    /**
     * Ensures that when an unrecognized gender value is found a DataImportException is thrown.
     */
    @Test
    public void badGender() throws Exception{
        // First, get the model
        ModelFactory factory = new ModelFactory();
        OSAXModel model = factory.generateModel(MODEL_FILE);
        assertNotNull(model);

        // Second, run the test
        DependencyInjector.get().setProductionMode(false);
        TestUpdater updater = new TestUpdater(false);
        TestUpdater.BREAK_FLAG = false;
        FamilySearchImporter test = new FamilySearchImporter(generateOptions("bad_gender"), updater);
        try {
            test.populate(model);
            fail();
        }catch(DataImportException e){}

        DependencyInjector.get().setProductionMode(true);
        assertTrue(DependencyInjector.get().isProductionMode());
    }

    /**
     * Ensures that when an invalid generation is encountered that
     * the importer doesn't throw an exception, but it is handled correctly.
     */
    @Test
    public void badGeneration() throws Exception{
        // First, get the model
        ModelFactory factory = new ModelFactory();
        OSAXModel model = factory.generateModel(MODEL_FILE);
        assertNotNull(model);

        // Second, run the test
        DependencyInjector.get().setProductionMode(false);
        TestUpdater updater = new TestUpdater(false);
        TestUpdater.BREAK_FLAG = false;
        FamilySearchImporter test = new FamilySearchImporter(generateOptions("bad_generation"), updater);
        test.populate(model);

        DependencyInjector.get().setProductionMode(true);
        assertTrue(DependencyInjector.get().isProductionMode());
    }

    /**
     * Ensures that date extraction from date objects works.
     */
    @Test
    public void extractDateStringFromDateObject() {
        // First, test formal dates
        Date test = new Date();
        test.setFormal("A-1900");
        FamilySearchImporter importer = new FamilySearchImporter(null, null);
        String date = importer.extractDateStringFromDateObject(test);
        assertTrue(date.equals("-1900"));

        test.setFormal("A+1900");
        date = importer.extractDateStringFromDateObject(test);
        assertTrue(date.equals("1900"));

        test.setFormal("A+10");
        date = importer.extractDateStringFromDateObject(test);
        assertNull(date);

        // Second, test original dates
        test.setFormal(null);
        test.setOriginal("May; 3, 19");
        date = importer.extractDateStringFromDateObject(test);
        assertNull(date);

        // Third, test all dates missing
        test.setOriginal(null);
        date = importer.extractDateStringFromDateObject(test);
        assertNull(date);
    }

    /**
     * Generates ImportOptions for testing purposes.
     */
    private ImportOptions generateOptions(String version){
        if(version.equals("standard")) {
            ImportOptions options = new ImportOptions();
            options.setAuthorization(null);
            options.setConstraints(new String[]{"constraint"});
            options.setNumGenerations(7);
            options.setPid("pid");
            return options;
        } else if(version.equals("not_logged_in")){
            ImportOptions options = new ImportOptions();
            options.setAuthorization("authorization");
            options.setConstraints(new String[]{"constraint"});
            options.setNumGenerations(7);
            options.setPid("no_log");
            return options;
        } else if(version.equals("null_ancestry")){
            ImportOptions options = new ImportOptions();
            options.setAuthorization(null);
            options.setConstraints(new String[]{"constraint"});
            options.setNumGenerations(7);
            options.setPid("null_ancestry");
            return options;
        } else if(version.equals("null_family")){
            ImportOptions options = new ImportOptions();
            options.setAuthorization(null);
            options.setConstraints(new String[]{"constraint"});
            options.setNumGenerations(7);
            options.setPid("null_family");
            return options;
        } else if(version.equals("bad_gender")){
            ImportOptions options = new ImportOptions();
            options.setAuthorization(null);
            options.setConstraints(new String[]{"constraint"});
            options.setNumGenerations(7);
            options.setPid("bad_gender");
            return options;
        } else if(version.equals("bad_generation")){
            ImportOptions options = new ImportOptions();
            options.setAuthorization(null);
            options.setConstraints(new String[]{"constraint"});
            options.setNumGenerations(7);
            options.setPid("bad_generation");
            return options;
        }
        else return null;
    }
}