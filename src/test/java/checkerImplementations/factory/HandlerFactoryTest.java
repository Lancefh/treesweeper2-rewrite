package checkerImplementations.factory;

import checkerImplementations.calculators.notDistributionBased.SpouseAgeGapCalculator;
import checkerImplementations.handlers.marriage.AgeGapBetweenSpousesHandler;
import checkerImplementations.handlers.research.DuplicateChildrenHandler;
import checkerImplementations.handlers.research.DuplicateParentHandler;
import checkerImplementations.handlers.research.DuplicateSpouseHandler;
import checkerImplementations.handlers.research.MissingVitalDatesHandler;
import org.junit.Test;

import static org.junit.Assert.*;

public class HandlerFactoryTest {

    /**
     * Ensures that the handler factory returns all handlers it should.
     */
    @Test
    public void getHandler() throws Exception {
        HandlerFactory test = new HandlerFactory(null);
        assertTrue(test.getHandler("christening") != null);
        assertTrue(test.getHandler("christeningBirth") != null);
        assertTrue(test.getHandler("christeningDeath") != null);
        assertTrue(test.getHandler("negativeAge") != null);
        assertTrue(test.getHandler("lifespan") != null);
        assertTrue(test.getHandler("ageAtChild") != null);
        assertTrue(test.getHandler("ageAtLastChild") != null);
        assertTrue(test.getHandler("bornAtBirth") != null);
        assertTrue(test.getHandler("deadAtBirth") != null);
        assertTrue(test.getHandler("ageAtMarriage") != null);
        assertTrue(test.getHandler("bornAtMarriage") != null);
        assertTrue(test.getHandler("marriageToChild") != null);
        assertTrue(test.getHandler("marriageToLastChild") != null);
        assertTrue(test.getHandler("sources") != null);
        assertTrue(test.getHandler("merge") != null);
        assertTrue(test.getHandler("ownAncestor") != null);
        assertTrue(test.getHandler("missingChildren") != null);
        assertTrue(test.getHandler("noChildren") != null);
        assertTrue(test.getHandler("aliveAtMarriage") != null);
        assertTrue(test.getHandler("missingEarlyChildren") != null);
        assertTrue(test.getHandler("missingLateChildren") != null);
        assertTrue(test.getHandler("missingMiddleChildren") != null);
        assertTrue(test.getHandler("missingSpouse") != null);
        assertTrue(test.getHandler("unknownGender") != null);
        assertTrue(test.getHandler("excessiveParents") != null);
        assertTrue(test.getHandler("missingVitals") instanceof MissingVitalDatesHandler);
        assertTrue(test.getHandler("spouseAgeGap") instanceof AgeGapBetweenSpousesHandler);
        assertTrue(test.getHandler("duplicateChildren") instanceof DuplicateChildrenHandler);
        assertTrue(test.getHandler("duplicateParents") instanceof DuplicateParentHandler);
        assertTrue(test.getHandler("duplicateSpouses") instanceof DuplicateSpouseHandler);
    }
}