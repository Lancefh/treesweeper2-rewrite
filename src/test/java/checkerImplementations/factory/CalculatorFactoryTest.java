package checkerImplementations.factory;

import checkerImplementations.calculators.notDistributionBased.SpouseAgeGapCalculator;
import interfaces.PluginNotFoundException;
import org.junit.Test;

import static org.junit.Assert.*;

public class CalculatorFactoryTest {

    /**
     * Ensures that the calculator factory can access all the distributions it's supposed to.
     */
    @Test
    public void getDistribution() throws Exception{
        CalculatorFactory factory = new CalculatorFactory();
        assertTrue(factory.getDistribution("christening").available() > 0);
        assertTrue(factory.getDistribution("fatherFirstChild").available() > 0);
        assertTrue(factory.getDistribution("fatherLastChild").available() > 0);
        assertTrue(factory.getDistribution("fatherMarriage").available() > 0);
        assertTrue(factory.getDistribution("husbandLifespan").available() > 0);
        assertTrue(factory.getDistribution("marriageFirstChild").available() > 0);
        assertTrue(factory.getDistribution("marriageLastChild").available() > 0);
        assertTrue(factory.getDistribution("motherFirstChild").available() > 0);
        assertTrue(factory.getDistribution("motherLastChild").available() > 0);
        assertTrue(factory.getDistribution("motherMarriage").available() > 0);
        assertTrue(factory.getDistribution("wifeLifespan").available() > 0);
        try{
            factory.getDistribution("none");
            fail();
        }catch(PluginNotFoundException e){}
    }

    /**
     * Ensures that the calculator factory throws an exception when asked for a
     * distribution it doesn't have.
     */
    @Test
    public void brokenDistribution() throws Exception{
        CalculatorFactory test = new CalculatorFactory();
        try{
            test.getDistribution("badtestbrokentest");
            fail();
        }catch(PluginNotFoundException e){
            assertTrue(e.getMessage().equals(" (No such file or directory)"));
        }
    }

    @Test
    public void testGetCalculator() throws Exception{
        CalculatorFactory test = new CalculatorFactory();
        assertTrue(test.getCalculator("spouseAgeGap") instanceof SpouseAgeGapCalculator);
    }
}