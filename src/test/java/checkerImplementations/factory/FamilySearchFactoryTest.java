package checkerImplementations.factory;

import checkerImplementations.importer.FamilySearchImporter;
import interfaces.IDataImporter;
import interfaces.IHandler;
import interfaces.PluginNotFoundException;
import org.junit.Test;

import static org.junit.Assert.*;

public class FamilySearchFactoryTest {

    /**
     * Ensures that the family search factory throws an exception when asked for a handler
     * it doesn't know about.
     */
    @Test
    public void getHandler() throws Exception{
        FamilySearchFactory factory = new FamilySearchFactory(null, null);
        try {
            factory.getHandler("");
            fail();
        }catch(PluginNotFoundException e){
            assertTrue(e.getMessage().equals("Unrecognized handler: "));
        }
    }

    /**
     * Ensures that the family search factory can get calculators it knows about, and
     * throws an exception when asked for a calculator it doesn't know about.
     */
    @Test
    public void getCalculator() throws Exception {
        FamilySearchFactory factory = new FamilySearchFactory(null,null);
        assertTrue(factory.getCalculator("death") != null);
        assertTrue(factory.getCalculator("ageAtBirth") != null);
        assertTrue(factory.getCalculator("ageAtMarriage") != null);
        assertTrue(factory.getCalculator("marriageChild") != null);
        assertTrue(factory.getCalculator("christening") != null);
        try{
            factory.getCalculator("none");
            fail();
        }catch(PluginNotFoundException e){}
    }

    /**
     * Ensures that the family search factory gets the correct type of importer.
     */
    @Test
    public void getDataImporter() throws Exception {
        FamilySearchFactory factory = new FamilySearchFactory(null,null);
        IDataImporter importer = factory.getDataImporter();
        assertTrue(importer instanceof FamilySearchImporter);
    }

    /**
     * Ensures that the family search factory can get distributions it should know about, and
     * that it throws an exception when asked for a distribution it doesn't know about.
     */
    @Test
    public void getDistribution() throws Exception{
        FamilySearchFactory factory = new FamilySearchFactory(null,null);
        assertTrue(factory.getDistribution("christening").available() > 0);
        assertTrue(factory.getDistribution("fatherFirstChild").available() > 0);
        assertTrue(factory.getDistribution("fatherLastChild").available() > 0);
        assertTrue(factory.getDistribution("fatherMarriage").available() > 0);
        assertTrue(factory.getDistribution("husbandLifespan").available() > 0);
        assertTrue(factory.getDistribution("marriageFirstChild").available() > 0);
        assertTrue(factory.getDistribution("marriageLastChild").available() > 0);
        assertTrue(factory.getDistribution("motherFirstChild").available() > 0);
        assertTrue(factory.getDistribution("motherLastChild").available() > 0);
        assertTrue(factory.getDistribution("motherMarriage").available() > 0);
        assertTrue(factory.getDistribution("wifeLifespan").available() > 0);
        try{
            factory.getDistribution("none");
            fail();
        }catch(PluginNotFoundException e){}
    }
}