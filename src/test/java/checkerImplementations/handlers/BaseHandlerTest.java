package checkerImplementations.handlers;

import dummy.BrokenUpdater;
import dummy.DummyModel;
import dummy.DummyUpdater;
import interfaces.DataImportException;
import org.junit.Test;
import osax.model.OSAXModel;
import osax.object.ModelObject;
import osax.relationSet.Tuple;

import java.io.IOException;

import static org.junit.Assert.*;

public class BaseHandlerTest {

    /**
     * Ensures that the handle() works.
     */
    @Test
    public void handle() throws Exception{
        DummyUpdater dummy = new DummyUpdater();
        DummyModel factory = new DummyModel();
        factory.initialize();
        OSAXModel model = factory.getModel();
        BaseHandler test = new BaseHandler(dummy) {
            @Override
            protected void handleUnlikely(double probability, Tuple tuple, OSAXModel model) throws IOException {
                getUpdater().sendMessageUpdate("handled","well");
            }

            @Override
            protected void handleImpossible(double probability, Tuple tuple, OSAXModel model) throws IOException {
                getUpdater().sendMessageUpdate("handled", "well");
            }
        };
        test.setUnlikelyBound(0.5);
        test.handle(0.4, null, model);
        test.setImpossibleBound(0.3);
        test.handle(0.2, null, model);
        assertTrue(dummy.getMessages().size() == 2);
    }

    /**
     * Ensures that when the handler has an exception when sending an update that it throws
     * a DataImport exception
     */
    @Test
    public void brokenUpdater() {
        BrokenUpdater dummy = new BrokenUpdater();
        DummyModel factory = new DummyModel();
        factory.initialize();
        OSAXModel model = factory.getModel();
        BaseHandler test = new BaseHandler(dummy) {
            @Override
            protected void handleUnlikely(double probability, Tuple tuple, OSAXModel model) throws IOException {
                getUpdater().sendMessageUpdate("handled","well");
            }

            @Override
            protected void handleImpossible(double probability, Tuple tuple, OSAXModel model) throws IOException {
                getUpdater().sendMessageUpdate("handled", "well");
            }
        };
        test.setUnlikelyBound(0.4);
        try{
            test.handle(0.3, null, model);
            fail();
        }catch(DataImportException e){

        }
    }

    /**
     * Ensures that when requested data is missing that findPersonAttribute()
     * returns null.
     */
    @Test
    public void missingData() throws Exception{
        BrokenUpdater dummy = new BrokenUpdater();
        DummyModel factory = new DummyModel();
        factory.initialize();
        OSAXModel model = factory.getModel();
        BaseHandler test = new BaseHandler(dummy) {
            @Override
            protected void handleUnlikely(double probability, Tuple tuple, OSAXModel model) throws IOException {
                getUpdater().sendMessageUpdate("handled","well");
            }

            @Override
            protected void handleImpossible(double probability, Tuple tuple, OSAXModel model) throws IOException {
                getUpdater().sendMessageUpdate("handled", "well");
            }
        };
        ModelObject person = factory.getPerson1();
        assertTrue(test.findPersonAttribute(person, model, 1, "Person has Name") == null);
    }
}