package checkerImplementations.handlers.results;

import org.junit.Test;

import static org.junit.Assert.*;

public class MessageUpdateTest {

    /**
     * Ensures that getType() works
     */
    @Test
    public void getType() {
        MessageUpdate test = new MessageUpdate(null, null, null);
        assertTrue(test.getType() == null);
    }

    /**
     * Ensures that getMessage() works
     */
    @Test
    public void getMessage() {
        MessageUpdate test = new MessageUpdate(null, null, null);
        assertTrue(test.getMessage() == null);
    }

    /**
     * Ensure that getSubmessage() works
     */
    @Test
    public void getSubmessage() {
        MessageUpdate test = new MessageUpdate(null, null, null);
        assertTrue(test.getSubmessage() == null);
    }

    /**
     * Ensure that getTarget() works and gives -1 if uninitialized
     */
    @Test
    public void getTarget() {
        MessageUpdate test = new MessageUpdate(null, null, null);
        assertTrue(test.getTarget() == -1);
    }

    /**
     * Ensures that setTarget() works
     */
    @Test
    public void setTarget() {
        MessageUpdate test = new MessageUpdate(null, null, null);
        test.setTarget(100);
        assertTrue(test.getTarget() == 100);
    }

    /**
     * Ensures that getProgress() works and gives -1 if uninitialized
     */
    @Test
    public void getProgress() {
        MessageUpdate test = new MessageUpdate(null, null, null);
        assertTrue(test.getProgress() == -1);
    }

    /**
     * Ensures that setProgress() works
     */
    @Test
    public void setProgress() {
        MessageUpdate test = new MessageUpdate(null, null, null);
        test.setProgress(100);
        assertTrue(test.getProgress() == 100);
    }

    /**
     * Ensures that toJson() works.
     */
    @Test
    public void toJson() {
        MessageUpdate test = new MessageUpdate(null, null, null);
        assertTrue(test.toJson() != null);
        assertTrue(test.toJson().equals("{\"target\":-1,\"progress\":-1}"));
    }
}