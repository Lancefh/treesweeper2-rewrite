package checkerImplementations.handlers.results;

import org.junit.Test;

import static org.junit.Assert.*;

public class MessageTypeTest {
    /**
     * This is a completionist test, to run every line in testing. It doesn't
     * actually test anything because MessageType is just an enumerated type.
     */
    @Test
    public void listTest(){
        MessageType test = MessageType.UPDATE;
        test = MessageType.ERROR;
        test = MessageType.RESULT;
        assertTrue(test != null);
    }

}