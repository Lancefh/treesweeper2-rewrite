package checkerImplementations.handlers.results;

import org.junit.Test;

import static org.junit.Assert.*;

public class FlaggedResultTest {

    /**
     * Ensures that getSeverity() works
     */
    @Test
    public void getSeverity() {
        FlaggedResult test = new FlaggedResult(0, null, null, 0);
        assertTrue(test.getSeverity() == null);
    }

    /**
     * Ensures that getMessage() works
     */
    @Test
    public void getMessage() {
        FlaggedResult test = new FlaggedResult(0, null, null, 0);
        assertTrue(test.getMessage() == null);
    }

    /**
     * Ensures that the fixes is array is initialized and empty after creation
     */
    @Test
    public void getFixes() {
        FlaggedResult test = new FlaggedResult(0, null, null, 0);
        assertTrue(test.getFixes() != null && test.getFixes().length == 0);
    }
}