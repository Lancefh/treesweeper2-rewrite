package checkerImplementations.handlers.results;

import org.junit.Test;

import static org.junit.Assert.*;

public class ResultUpdateTest {

    /**
     * Ensures that toJson() works
     */
    @Test
    public void toJson() {
        ResultUpdate test = new ResultUpdate(null);
        assertTrue(test.toJson() != null);
        assertTrue(test.toJson().equals("{\"type\":\"RESULT\"}"));
    }
}