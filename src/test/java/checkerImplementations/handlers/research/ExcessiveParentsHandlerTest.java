package checkerImplementations.handlers.research;

import dummy.DummyModel;
import dummy.DummyUpdater;
import org.junit.Test;
import osax.model.OSAXModel;
import osax.object.ModelObject;
import osax.object.nonlexical.NonLexicalObject;
import osax.relationSet.Tuple;

import static org.junit.Assert.*;

public class ExcessiveParentsHandlerTest {

    /**
     * Ensures that handling impossible results works.
     */
    @Test
    public void handleImpossible() throws Exception{
        DummyModel dummy = new DummyModel();
        dummy.initialize();
        OSAXModel model = dummy.getModel();
        DummyUpdater updater = new DummyUpdater();
        ExcessiveParentsHandler test = new ExcessiveParentsHandler(updater);
        ModelObject person = dummy.getPerson1();
        Tuple t = new Tuple(person);
        test.handleImpossible(0.5, t, model);
        assertTrue(updater.getResults().size() == 1);
    }
}