package checkerImplementations.handlers.research;

import dummy.DummyModel;
import dummy.DummyUpdater;
import org.junit.Test;
import osax.model.OSAXModel;
import osax.object.ModelObject;
import osax.object.lexical.DOUBLE;
import osax.relationSet.Tuple;

import static org.junit.Assert.*;

public class DuplicateChildrenHandlerTest {

    /**
     * Ensures that handling impossible results works.
     */
    @Test
    public void handleImpossible() throws Exception{
        DummyModel dummy = new DummyModel();
        dummy.initialize();
        OSAXModel model = dummy.getModel();
        DummyUpdater updater = new DummyUpdater();
        DuplicateChildrenHandler test = new DuplicateChildrenHandler(updater);
        ModelObject person1 = dummy.getPerson1();
        Tuple tuple = new Tuple(person1, person1, person1);
        test.handleImpossible(0.0, tuple, model);
        assertTrue(updater.getResults().size() == 1);
    }
}