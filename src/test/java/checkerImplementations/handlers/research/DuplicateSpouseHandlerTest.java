package checkerImplementations.handlers.research;

import dummy.DummyModel;
import dummy.DummyUpdater;
import org.junit.Test;
import osax.model.OSAXModel;
import osax.object.ModelObject;
import osax.relationSet.Tuple;

import static org.junit.Assert.*;

public class DuplicateSpouseHandlerTest {

    /**
     * Ensures that handling impossible results works.
     */
    @Test
    public void handleImpossible() throws Exception{
        DummyModel dummy = new DummyModel();
        dummy.initialize();
        OSAXModel model = dummy.getModel();
        DummyUpdater updater = new DummyUpdater();
        DuplicateSpouseHandler test = new DuplicateSpouseHandler(updater);
        ModelObject person1 = dummy.getPerson1();
        Tuple tuple = new Tuple(person1, person1, person1);
        test.handleImpossible(0.0, tuple, model);
        assertTrue(updater.getResults().size() == 1);
    }
}