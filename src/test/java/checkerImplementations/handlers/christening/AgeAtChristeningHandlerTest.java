package checkerImplementations.handlers.christening;

import dummy.DummyModel;
import dummy.DummyUpdater;
import org.junit.Test;
import osax.model.OSAXModel;
import osax.object.ModelObject;
import osax.object.lexical.INTEGER;
import osax.object.nonlexical.NonLexicalObject;
import osax.relationSet.Tuple;

import static org.junit.Assert.*;

public class AgeAtChristeningHandlerTest {

    /**
     * Ensures that handling unlikely results works.
     */
    @Test
    public void handleUnlikely() throws Exception {
        DummyModel dummy = new DummyModel();
        dummy.initialize();
        OSAXModel model = dummy.getModel();
        DummyUpdater updater = new DummyUpdater();
        AgeAtChristeningHandler test = new AgeAtChristeningHandler(updater);
        ModelObject person = dummy.getPerson1();
        ModelObject child = new NonLexicalObject();
        ModelObject age = new INTEGER("20");
        Tuple t = new Tuple(person, child, age);
        test.handleUnlikely(0.5, t, model);
        assertTrue(updater.getResults().size() == 1);
    }

    /**
     * Ensures that handling impossible results works.
     */
    @Test
    public void handleImpossible() throws Exception{
        DummyModel dummy = new DummyModel();
        dummy.initialize();
        OSAXModel model = dummy.getModel();
        DummyUpdater updater = new DummyUpdater();
        AgeAtChristeningHandler test = new AgeAtChristeningHandler(updater);
        ModelObject person = dummy.getPerson1();
        ModelObject child = new NonLexicalObject();
        ModelObject age = new INTEGER("20");
        Tuple t = new Tuple(person, child, age);
        test.handleImpossible(0.5, t, model);
        assertTrue(updater.getResults().size() == 1);
    }
}