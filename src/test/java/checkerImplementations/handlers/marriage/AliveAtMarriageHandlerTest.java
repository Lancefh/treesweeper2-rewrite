package checkerImplementations.handlers.marriage;

import dummy.DummyModel;
import dummy.DummyUpdater;
import org.junit.Test;
import osax.model.OSAXModel;
import osax.object.ModelObject;
import osax.object.lexical.INTEGER;
import osax.object.nonlexical.NonLexicalObject;
import osax.relationSet.Tuple;

import static org.junit.Assert.*;

public class AliveAtMarriageHandlerTest {

    /**
     * Ensures that handling impossible results works.
     */
    @Test
    public void handleImpossible() throws Exception{
        DummyModel dummy = new DummyModel();
        dummy.initialize();
        OSAXModel model = dummy.getModel();
        DummyUpdater updater = new DummyUpdater();
        AliveAtMarriageHandler test = new AliveAtMarriageHandler(updater);
        ModelObject person = dummy.getPerson1();
        ModelObject spouse = new NonLexicalObject();
        ModelObject age = new INTEGER("20");
        ModelObject date = new INTEGER("100");
        Tuple t = new Tuple(person, age, spouse, date);
        test.handleImpossible(0.5, t, model);
        assertTrue(updater.getResults().size() == 1);
    }
}