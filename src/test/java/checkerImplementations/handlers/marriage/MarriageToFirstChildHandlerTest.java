package checkerImplementations.handlers.marriage;

import dummy.DummyModel;
import dummy.DummyUpdater;
import org.junit.Test;
import osax.model.OSAXModel;
import osax.object.ModelObject;
import osax.object.lexical.INTEGER;
import osax.object.nonlexical.NonLexicalObject;
import osax.relationSet.Tuple;

import static org.junit.Assert.*;

public class MarriageToFirstChildHandlerTest {

    /**
     * Ensures that handling unlikely results works.
     */
    @Test
    public void handleUnlikely() throws Exception{
        DummyModel dummy = new DummyModel();
        dummy.initialize();
        OSAXModel model = dummy.getModel();
        DummyUpdater updater = new DummyUpdater();
        MarriageToFirstChildHandler test = new MarriageToFirstChildHandler(updater);
        ModelObject person = dummy.getPerson1();
        ModelObject spouse = new NonLexicalObject();
        ModelObject child = new NonLexicalObject();
        ModelObject years = new INTEGER("1000");
        ModelObject date = new INTEGER("100");
        Tuple t = new Tuple(person, spouse, child, years, date);
        test.handleUnlikely(0.5, t, model);
        assertTrue(updater.getResults().size() == 2);
    }

    /**
     * Ensures that handling impossible results works.
     */
    @Test
    public void handleImpossible() throws Exception{
        DummyModel dummy = new DummyModel();
        dummy.initialize();
        OSAXModel model = dummy.getModel();
        DummyUpdater updater = new DummyUpdater();
        MarriageToFirstChildHandler test = new MarriageToFirstChildHandler(updater);
        ModelObject person = dummy.getPerson1();
        ModelObject spouse = new NonLexicalObject();
        ModelObject child = new NonLexicalObject();
        ModelObject years = new INTEGER("1000");
        ModelObject date = new INTEGER("100");
        Tuple t = new Tuple(person, spouse, child, years, date);
        test.handleImpossible(0.5, t, model);
        assertTrue(updater.getResults().size() == 2);
    }
}