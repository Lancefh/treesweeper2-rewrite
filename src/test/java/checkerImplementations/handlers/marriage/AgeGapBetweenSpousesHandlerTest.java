package checkerImplementations.handlers.marriage;

import dummy.DummyModel;
import dummy.DummyUpdater;
import org.junit.Test;
import osax.model.OSAXModel;
import osax.object.ModelObject;
import osax.object.lexical.DOUBLE;
import osax.relationSet.Tuple;

import static org.junit.Assert.*;

public class AgeGapBetweenSpousesHandlerTest {

    /**
     * Ensures that handling unlikely results works.
     */
    @Test
    public void handleUnlikely() throws Exception{
        DummyModel dummy = new DummyModel();
        dummy.initialize();
        OSAXModel model = dummy.getModel();
        DummyUpdater updater = new DummyUpdater();
        AgeGapBetweenSpousesHandler test = new AgeGapBetweenSpousesHandler(updater);
        ModelObject person1 = dummy.getPerson1();
        Tuple tuple = new Tuple(person1, person1, new DOUBLE("50"));
        test.handleUnlikely(0.5, tuple, model);
        assertTrue(updater.getResults().size() == 2);
    }

    /**
     * Ensures that handling impossible results works.
     */
    @Test
    public void handleImpossible() throws Exception{
        DummyModel dummy = new DummyModel();
        dummy.initialize();
        OSAXModel model = dummy.getModel();
        DummyUpdater updater = new DummyUpdater();
        AgeGapBetweenSpousesHandler test = new AgeGapBetweenSpousesHandler(updater);
        ModelObject person1 = dummy.getPerson1();
        Tuple tuple = new Tuple(person1, person1, new DOUBLE("101"));
        test.handleImpossible(0.0, tuple, model);
        assertTrue(updater.getResults().size() == 2);
    }
}