package server;

/**
 * These are some shared constants used around the program.
 */
public class SharedConstants {
    public static final String WEB_ROOT = "treesweeper2-front-end/dist/treesweeper2-front-end/";
    public static final String DEFAULT_PAGE = "index.html";
    public static final String PAGE_404 = "404.html";
}
