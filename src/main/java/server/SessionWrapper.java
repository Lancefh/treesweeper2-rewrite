package server;

import org.eclipse.jetty.websocket.api.Session;

import java.io.IOException;

/**
 * Contains a Session to make it easier to manage communication with the user.
 */
public class SessionWrapper implements ISessionWrapper {
    private Session session;

    public SessionWrapper(Session session){
        this.session = session;
    }

    @Override
    public boolean isOpen() {
        return session != null && session.isOpen();
    }

    @Override
    public void close() {
        if(session != null) session.close();
    }

    @Override
    public void send(String msg) throws IOException {
        if(session != null && session.getRemote() != null) session.getRemote().sendString(msg);
    }
}
