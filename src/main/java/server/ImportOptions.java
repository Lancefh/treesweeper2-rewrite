package server;

/**
 * Just a dumb data holder for communication between client and server.  Sets options for the import process.
 */
public class ImportOptions {
    private String[] disabledConstraints;
    private int numGenerations;
    private String token;
    private String rootPid;

    public ImportOptions(){
        this.disabledConstraints = null;
        this.numGenerations = -1;
        this.token = null;
        this.rootPid = null;
    }

    public String[] getConstraints() {
        return disabledConstraints;
    }

    public void setConstraints(String[] constraints) {
        this.disabledConstraints = constraints;
    }

    public int getNumGenerations() {
        return numGenerations;
    }

    public void setNumGenerations(int numGenerations) {
        this.numGenerations = numGenerations;
    }

    public String getAuthorization() {
        return token;
    }

    public void setAuthorization(String authorization) {
        this.token = authorization;
    }

    public String getPid() {
        return rootPid;
    }

    public void setPid(String pid) {
        this.rootPid = pid;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder("{ constraints:[");
        if(disabledConstraints != null) for(String constraint : disabledConstraints) sb.append(constraint).append(",");
        sb.append("], numGenerations: ").append(numGenerations).append(", authorization: ");
        sb.append(token).append(", pid: ").append(rootPid);
        sb.append("}");
        return sb.toString();
    }
}
