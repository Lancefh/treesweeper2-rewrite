package server;

import injector.DependencyInjector;

/**
 * Main method for the server.  Starts up a JettyServer.
 */
public class ServerCommunicator {
    private static JettyServer server = null;

    /**
     * The default port number for the server.
     */
    public static final int DEFAULT_PORT = 8080;

    private static int PORT_NUMBER = DEFAULT_PORT;

    /**
     * Returns the current port number for the server
     * @return Obvious.
     */
    public static int getPortNumber(){return PORT_NUMBER;}

    /**
     * Sets the current port number for the server
     * @param portNumber The number of the port the server should use.
     */
    public static void setPortNumber(int portNumber){PORT_NUMBER = portNumber;}

    /**
     * Starts a JettyServer on a given port, or on the default port if none is provided.
     * @param args Command line arguments.  All are optional, but order does matter.
     *             First argument: port number to run the server on.
     *             Second argument: -testing if you want the server to run in testing mode.
     *             Third argument: a different model file to use
     *             Fourth argument: a different configuration file to use. Can only use this if a model file is provided as well.
     * @throws Exception If anything goes wrong.
     */
    public static void main(String[] args)throws Exception{
        if(args.length >= 1){
            try{
                int portNumber = Integer.parseInt(args[0]);
                setPortNumber(portNumber);
            }catch(NumberFormatException e){
                throw new Exception("Invalid port number");
            }
        }

        if(args.length >= 2){
            if(args[1].equals("-testing")) DependencyInjector.get().setProductionMode(false);
        }
        if(args.length >= 3) ImporterSocket.setModelFile(args[2]);
        if(args.length >= 4) ImporterSocket.setConfigurationFile(args[3]);

        server = new JettyServer();
        server.start(PORT_NUMBER);
    }

    public static void shutdown() throws Exception{
        if(server != null) server.stop();
        server = null;
    }
}
