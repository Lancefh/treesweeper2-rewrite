package server;

import checker.ConstraintChecker;
import checker.ConstraintErrorException;
import checkerImplementations.factory.FamilySearchFactory;
import checkerImplementations.handlers.results.FlaggedResult;
import checkerImplementations.handlers.results.MessageType;
import checkerImplementations.handlers.results.MessageUpdate;
import checkerImplementations.handlers.results.ResultUpdate;
import configuration.configurationVisitor.ConfigurationException;
import distribution.DistributionReadFailureException;
import injector.DependencyInjector;
import interfaces.DataImportException;
import interfaces.IPluginFactory;
import interfaces.PluginNotFoundException;
import modelVisitor.ModelBuildException;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.WebSocketListener;
import serialization.SerializeException;
import serialization.Serializer;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A WebSocket that triggers the process to import data and run constraints.  It also communicates the results back to the client.
 */
public class ImporterSocket implements WebSocketListener,IUpdater {

    /**
     * The directory containing the model and configuration files.
     */
    private static final String MODEL_CONFIG_ROOT = "model_and_configuration/";

    /**
     * The path to the OSA-X model file.
     */
    private static String MODEL_FILE = MODEL_CONFIG_ROOT + "FamilySearchModel.txt";

    /**
     * The path to the ConstraintChecker configuration file.
     */
    private static String CONFIGURATION_FILE = MODEL_CONFIG_ROOT + "FamilySearchConfiguration.txt";

    /**
     * Sets the path to the OSA-X model file.
     * @param file The file to use, inside the model and configuration directory.
     */
    public static void setModelFile(String file){
        MODEL_FILE = MODEL_CONFIG_ROOT + file;
    }

    /**
     * Resets the model file to the default.
     */
    public static void resetModelFile(){ MODEL_FILE = MODEL_CONFIG_ROOT + "FamilySearchModel.txt"; }

    /**
     * Sets the path to the ConstraintChecker configuration file.
     * @param file The file to use, inside the model and configuration directory.
     */
    public static void setConfigurationFile(String file){
        CONFIGURATION_FILE = MODEL_CONFIG_ROOT + file;
    }

    /**
     * Resets the configuration file to the default.
     */
    public static void resetConfigurationFile(){ CONFIGURATION_FILE = MODEL_CONFIG_ROOT + "FamilySearchConfiguration.txt"; }

    /**
     * A session to use to communicate with the client.
     */
    private ISessionWrapper outbound;

    /**
     * The default constructor has no Session.
     */
    public ImporterSocket(){
        outbound = null;
    }

    /**
     * Responds to binary data from the client; does nothing since it should only receive text data from clients.
     */
    @Override
    public void onWebSocketBinary(byte[] bytes, int offset, int length) {
        /* only interested in text messages */
    }

    /**
     * Sends a message update to the user, which will be displayed on their view.
     * @param message The message to send.
     * @throws IOException If unable to send the update.
     */
    @Override
    public void sendMessageUpdate(String message, String submessage) throws IOException{
        MessageUpdate update = new MessageUpdate(MessageType.UPDATE, message, submessage);
        send(update.toJson());
    }

    /**
     * Sends a progress update; specifically, progress in downloading information
     * @param message The primary message to be displayed during downloading
     * @param submessage The secondary message to be displayed; presently, this is not displayed, just checked if it is present.
     * @param target The current target number to be reached
     * @param progress The progress towards the target number
     * @throws IOException
     */
    @Override
    public void sendProgressUpdate(String message, String submessage, int target, int progress) throws IOException {
        MessageUpdate update = new MessageUpdate(MessageType.UPDATE, message, submessage);
        update.setTarget(target);
        update.setProgress(progress);
        send(update.toJson());
    }

    /**
     * Sends an error update to the user, which will be displayed on their view.
     * @param message The error message to send.
     * @throws IOException If unable to send the update.
     */
    @Override
    public void sendErrorUpdate(String message) throws IOException{
        MessageUpdate update = new MessageUpdate(MessageType.ERROR, message, null);
        send(update.toJson());
    }

    /**
     * Sends a PersonResults object to the user, which will be added to the results they can see.
     * @param result The FlaggedResult to send.
     * @throws IOException If unable to send the update.
     */
    @Override
    public void sendPersonResults(FlaggedResult result) throws IOException {
        ResultUpdate update = new ResultUpdate(result);
        send(update.toJson());
    }

    /**
     * Sends a message to be printed to the user's console. Mostly useful for debugging at the client end.
     * @param message The message to print.
     * @throws IOException If unable to send the message.
     */
    @Override
    public void sendConsoleMessage(String message) throws IOException{
        send(message);
    }

    /**
     * Responds to text data from the client.  Starts the import process.
     * @param message A JSON string representing an ImportOptions object.
     */
    @Override
    public void onWebSocketText(String message) {
        if ((outbound != null) && (outbound.isOpen()))
        {
            try{
                // Deserialize the ImportOptions object
                ImportOptions options = (ImportOptions)Serializer.get().deserialize(message, ImportOptions.class);

                // Build the plugin factory
                IPluginFactory factory = DependencyInjector.get().pluginFactory(options, this);

                // Get list of constraints to skip
                List<String> toSkip = new ArrayList<>(Arrays.asList(options.getConstraints()));

                // Run constraint checker
                ConstraintChecker core = new ConstraintChecker();
                core.run(MODEL_FILE,CONFIGURATION_FILE,factory,toSkip);

                sendMessageUpdate("",null);
                this.outbound.close();
                this.outbound = null;
            }
            catch(ConfigurationException e){
                experiencingDifficulties("Configuration file failed to parse");
            }
            catch(ModelBuildException e){
                experiencingDifficulties("Model file failed to parse");
            }
            catch(ConstraintErrorException e){
                experiencingDifficulties("Constraint broke: " + e.getMessage());
            }
            catch(PluginNotFoundException e){
                experiencingDifficulties("Plugin not found: " + e.getMessage());
            }
            catch(DataImportException e){
                if(e.getMessage().startsWith("Ancestry for") && e.getMessage().endsWith("came back null.")){
                    try{
                        sendErrorUpdate("Tree Sweeper couldn't access necessary information for this sweep. Are you authorized to get information on your starting point?");
                        if(DependencyInjector.get().isProductionMode()) System.err.println("Data importing failed: " + e.getMessage());
                        sendConsoleMessage("Data importing failed: " + e.getMessage());
                    }catch(IOException err){
                        if(DependencyInjector.get().isProductionMode()) err.printStackTrace();
                    }
                } else {
                    experiencingDifficulties("Data importing failed: " + e.getMessage());
                }
            }
            catch(DistributionReadFailureException e){
                experiencingDifficulties("Distribution read failed: " + e.getMessage());
            }
            catch(IOException e){
                if(DependencyInjector.get().isProductionMode()) e.printStackTrace();
                if(DependencyInjector.get().isProductionMode()) System.err.println("ImporterSocket: connection lost.");
            }
            catch(SerializeException e){
                if(message.equals("")){
                    // Do nothing, just a ping
                }
                else {
                    // Received an unexpected message.  Sends an error back to the client.
                    if(DependencyInjector.get().isProductionMode()) e.printStackTrace();
                    try {
                        sendConsoleMessage("Not options, your message was " + message);
                    }catch(IOException err){
                        if(DependencyInjector.get().isProductionMode()) System.err.println("ImporterSocket: Connection lost.");
                    }
                    if(DependencyInjector.get().isProductionMode()) System.out.println("Did not receive options; message = " + message);
                }
            }
            catch(Exception e){
                if(DependencyInjector.get().isProductionMode()) e.printStackTrace();
                experiencingDifficulties(e.getMessage());
            }
        }
    }

    /**
     * Sends a standard "we are experiencing technical difficulties" message to users when tree sweeper breaks.
     * @param errorMessage The error message to print to consoles on the server and the client
     */
    private void experiencingDifficulties(String errorMessage){
        try{
            sendErrorUpdate("TreeSweeper is experiencing difficulties. We apologize for the inconvenience. Please try again later.");
            if(DependencyInjector.get().isProductionMode()) System.err.println(errorMessage);
            sendConsoleMessage(errorMessage);
        }catch(IOException err){
            if(DependencyInjector.get().isProductionMode()) System.err.println("Trying to send message, but socket is closed.");
        }
    }

    /**
     * Triggered when the client closes the connection. Removes the session and deletes the WebSocket.
     * @param statusCode Disregarded in this implementation.
     * @param reason Disregarded in this implementation.
     */
    @Override
    public void onWebSocketClose(int statusCode, String reason) {
        this.outbound = null;
    }

    /**
     * Triggered when the client first establishes a connection.  Stores the new Session for later use.
     * @param session The new Session related to the client.
     */
    @Override
    public void onWebSocketConnect(Session session) {
        this.outbound = DependencyInjector.get().session(session);
    }

    /**
     * Triggered when there is an error in the connection with the client.
     * @param cause The cause of the error.
     */
    @Override
    public void onWebSocketError(Throwable cause) {
        if(DependencyInjector.get().isProductionMode()) cause.printStackTrace(System.err);
    }

    /**
     * Sends a string to a user
     * @param message The message to send
     * @throws IOException If the socket connection has been lost
     */
    private void send(String message) throws IOException{
        synchronized(this){
            if(outbound == null) throw new IOException("Socket closed.");
            outbound.send(message);
        }
    }
}
