package server;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import static server.SharedConstants.DEFAULT_PAGE;
import static server.SharedConstants.WEB_ROOT;

/**
 * Serves up the index and 404 pages for the app.
 */
public class DefaultServlet extends HttpServlet {

    /**
     * Handles a GET request to the servlet, serving up the desired page or 404 if the page is not found.
     * @param request The request being handled
     * @param response The response being sent back to the client
     * @throws ServletException
     * @throws IOException
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Check if the path is recognized
        String pathString = request.getRequestURI();
        if(pathString.matches("/") || pathString.startsWith("/home") || pathString.startsWith("/start") ||
                pathString.startsWith("/help") || pathString.startsWith("/contact") || pathString.startsWith("/logout"))
        {
            pathString = DEFAULT_PAGE;
        }
        int responseCode;

        byte[] result = new byte[0];

        // Return the target file
        try{
            result = Files.readAllBytes(Paths.get(WEB_ROOT + pathString));
            responseCode = HttpServletResponse.SC_OK;
        }catch(IOException e){
            // File is unknown
            responseCode = HttpServletResponse.SC_NOT_FOUND;
        }

        // Send response
        response.setStatus(responseCode);
        OutputStream os = response.getOutputStream();
        os.write(result);
        os.close();
    }
}
