package server;

import checkerImplementations.handlers.results.FlaggedResult;

import java.io.IOException;

/**
 * Interface for updating a user. Can send different types of updates.
 */
public interface IUpdater {
    /**
     * Sends a text message and submessage.
     * @param message The message to send.
     * @param submessage The submessage to send (usually details about the message).
     * @throws IOException If the connection is lost.
     */
    public void sendMessageUpdate(String message, String submessage) throws IOException;

    /**
     * Sends a progress update, along with a message.
     * @param message The message to send.
     * @param submessage The submessage to send (usually details about the message).
     * @param target The value being progressed towards.
     * @param progress The current progress towards the target value.
     * @throws IOException If the connection is lost.
     */
    public void sendProgressUpdate(String message, String submessage, int target, int progress) throws IOException;

    /**
     * Sends an error update.
     * @param message The error message to send.
     * @throws IOException If the connection is lost.
     */
    public void sendErrorUpdate(String message) throws IOException;

    /**
     * Sends a FlaggedResult to the user.
     * @param results The FlaggedResult to send.
     * @throws IOException If the connection is lost.
     */
    public void sendPersonResults(FlaggedResult results) throws IOException;

    /**
     * Sends a plain text message to the user, for their console.
     * @param message The console message.
     * @throws IOException If the connection is lost.
     */
    public void sendConsoleMessage(String message) throws IOException;
}
