package server;

import java.io.IOException;

public interface ISessionWrapper {
    /**
     * Tells whether the connection to the client is open.
     * @return True if the connection is open, false otherwise.
     */
    public boolean isOpen();

    /**
     * Closes the connection to the client.
     */
    public void close();

    /**
     * Sends a message to the client.
     * @param msg The message to send.
     * @throws IOException If the connection has been lost.
     */
    public void send(String msg) throws IOException;
}
