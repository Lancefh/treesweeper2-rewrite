package server;

import java.io.IOException;

/**
 * This is a class used for testing purposes. Its methods are designed to facilitate specific tests, so changing them
 * may break some tests.
 */
public class TestWrapper implements ISessionWrapper {
    private static boolean FAIL = false;
    public static void enableFail(){FAIL = true;}
    public static void disableFail(){FAIL = false;}

    @Override
    public boolean isOpen() {
        return true;
    }

    @Override
    public void close() {

    }

    @Override
    public void send(String msg) throws IOException {
        if(FAIL){
            if(msg.equals("Configuration file failed to parse")) throw new IOException("haha");
            else if(msg.startsWith("Data importing failed: ")) throw new IOException("haha");
            else if(msg.equals("{\"type\":\"UPDATE\",\"message\":\"\",\"target\":-1,\"progress\":-1}"))
                throw new IOException("haha");
            else if(msg.startsWith("Not options, your message was ")) throw new IOException("haha");
//            else System.out.println(msg);
        }
    }
}
