package server;

import org.eclipse.jetty.http.pathmap.ServletPathSpec;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.resource.PathResource;
import org.eclipse.jetty.util.thread.QueuedThreadPool;
import org.eclipse.jetty.websocket.server.WebSocketUpgradeFilter;

import java.io.File;
import java.nio.file.Path;

import static server.SharedConstants.DEFAULT_PAGE;
import static server.SharedConstants.WEB_ROOT;

/**
 * Handles all the details of getting a Jetty Server up and running.
 */
public class JettyServer {
    private static final String IMPORT_PATH = "/import";
    private static final String DEFAULT_PATH ="/";

    private static final int MAX_THREADS = 100;
    private static final int MIN_THREADS = 10;
    private static final int IDLE_TIMEOUT = 120;

    private Server server;

    /**
     * Starts a new server.
     * @param portNumber The port on which the server should be listening
     * @throws Exception If anything goes wrong.
     */
    public void start(int portNumber) throws Exception{
        // Establish a thread pool to handle incoming requests
        QueuedThreadPool threadPool = new QueuedThreadPool(MAX_THREADS, MIN_THREADS, IDLE_TIMEOUT);

        // Create a server using the thread pool
        server = new Server(threadPool);
        ServerConnector connector = new ServerConnector(server);
        connector.setPort(portNumber);
        server.setConnectors(new Connector[] {connector});

        // Set the path to the root of the server
        Path webRootPath = new File(WEB_ROOT).toPath().toRealPath();

        // Set up contexts
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");
        context.setBaseResource(new PathResource(webRootPath));
        context.setWelcomeFiles(new String[] { DEFAULT_PAGE });
        server.setHandler(context);

        // Add the websocket filter
        WebSocketUpgradeFilter wsfilter = WebSocketUpgradeFilter.configureContext(context);
        wsfilter.getFactory().getPolicy().setIdleTimeout(11000);
        wsfilter.addMapping(new ServletPathSpec(IMPORT_PATH), new ImportSocketCreator());

        // NOTE! If you don't add the DefaultServlet, your
        // resources won't get served!
        ServletHolder holderDefault = new ServletHolder("default", DefaultServlet.class);
        holderDefault.setInitParameter("dirAllowed", "true");
        context.addServlet(holderDefault, DEFAULT_PATH);

        // Start the server
        server.start();
//        server.join();
    }

    /**
     * Stops the server.
     * @throws Exception If something goes wrong.
     */
    void stop() throws Exception {
        new Thread(() -> {
            try {
                server.stop();
            }catch(Exception e){
//                e.printStackTrace();
            }
        }).start();
    }
}
