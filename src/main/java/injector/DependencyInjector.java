package injector;

import checkerImplementations.factory.FamilySearchFactory;
import checkerImplementations.importer.IImportThread;
import checkerImplementations.importer.ImportThread;
import checkerImplementations.importer.proxy.*;
import checkerImplementations.importer.proxy.test.*;
import interfaces.IPluginFactory;
import org.eclipse.jetty.websocket.api.Session;
import server.*;

/**
 * This class is responsible for providing the correct version of
 * different interfaces, depending on whether it's in testing or production mode.
 */
public class DependencyInjector {

    /**
     * This is the singleton instance of the class.
     */
    private static DependencyInjector SINGLETON = null;

    /**
     * Gets the singleton instance of the class.
     * @return Obvious.
     */
    public static DependencyInjector get(){
        if(SINGLETON == null) SINGLETON = new DependencyInjector();
        return SINGLETON;
    }

    /**
     * The mode the injector is in.
     */
    private boolean productionMode;

    /**
     * Constructor is private because this is a singleton. In production mode by default.
     */
    private DependencyInjector(){
        this.productionMode = true;
    }

    /**
     * Puts the injector into production or testing mode.
     * @param productionMode True to enable production mode, false to enable testing mode.
     */
    public void setProductionMode(boolean productionMode){
        this.productionMode = productionMode;
    }

    /**
     * Tells if the injector is in production mode.
     * @return True if in production mode, false if in testing mode.
     */
    public boolean isProductionMode(){
        return this.productionMode;
    }

    /**
     * Gets an IClient, which is used for connecting to the web and getting information.
     * @return Obvious.
     */
    public IClient client(){
        if(productionMode) return new HttpClient();
        else return new TestClient();
    }

    /**
     * Gets an IConnectionFactory, which generates connections.
     * @return Obvious.
     */
    public IConnectionFactory connectionFactory(){
        if(productionMode) return new WebFactory();
        else return new TestFactory();
    }

    /**
     * Generates an IImportThread, which is used for gathering family information about an ancestor.
     * @param pid The Family Search PID of the ancestor.
     * @param token A current access token with permission for the given PID.
     * @param generation The generation of the ancestor, based on how many generations back they are from a root person.
     * @return Obvious.
     */
    public IImportThread importThread(String pid, String token, int generation){
        if(productionMode) return new ImportThread(pid, token, generation);
        else return new TestThread(pid, generation);
    }

    /**
     * Gets an IProxy, which is used for getting information from Family Search.
     * @return Obvious.
     */
    public IProxy proxy(){
        if(productionMode) return new FamilySearchProxy();
        else return new TestProxy();
    }

    /**
     * Gets an IPluginFactory, which is used by ConstraintChecker.
     * @param options The options to be used for importing data.
     * @param updater An updater to send information.
     * @return Obvious.
     */
    public IPluginFactory pluginFactory(ImportOptions options, IUpdater updater){
        if(productionMode) return new FamilySearchFactory(options, updater);
        else return new checkerImplementations.factory.test.TestFactory(options);
    }

    /**
     * Gets an ISessionWrapper, which assists with communicating via socket.
     * @param session The session to wrap in the wrapper.
     * @return Obvious.
     */
    public ISessionWrapper session(Session session){
        if(productionMode) return new SessionWrapper(session);
        else return new TestWrapper();
    }
}
