package checkerImplementations.handlers;

import checkerImplementations.handlers.results.FlaggedResult;
import checkerImplementations.handlers.results.ResultSeverity;
import interfaces.DataImportException;
import interfaces.IHandler;
import osax.model.OSAXModel;
import osax.object.ModelObject;
import osax.object.lexical.NULL;
import osax.relationSet.RelationshipSet;
import osax.relationSet.Tuple;
import server.IUpdater;

import java.io.IOException;
import java.util.Iterator;

/**
 * This is the base class for handlers.
 */
public abstract class BaseHandler implements IHandler {
    private double unlikelyBound;
    private double impossibleBound;
    private IUpdater updater;

    public BaseHandler(IUpdater updater){
        unlikelyBound = 0;
        impossibleBound = 0;
        this.updater = updater;
    }

    protected IUpdater getUpdater(){return updater;}

    protected abstract void handleUnlikely(double probability, Tuple tuple, OSAXModel model) throws IOException;
    protected abstract void handleImpossible(double probability, Tuple tuple, OSAXModel model) throws IOException;

    @Override
    public void handle(double v, Tuple tuple, OSAXModel model) throws DataImportException {
        try {
            if (v < 0) return;   // If probability is negative, that means the probability is unknown
            if (v <= unlikelyBound && v > impossibleBound) handleUnlikely(v, tuple, model);
            else if (v <= impossibleBound) handleImpossible(v, tuple, model);
        }catch(IOException e){
            throw new DataImportException("Socket lost, can't finish handling.");
        }
    }

    @Override
    public void setUnlikelyBound(double v) {
        unlikelyBound = v;
    }

    @Override
    public void setImpossibleBound(double v) {
        impossibleBound = v;
    }

    /**
     * Gets an attribute for a person.
     * @param modelPerson The ModelObject representing the person.
     * @param model The OSAX model to use.
     * @param attributeIndex The index of the desired attribute in the schema in the target Relationship Set.
     * @param relationshipSetName The name of the relationship set containing the target attribute.
     * @return A string representing the target attribute, or null if it cannot be found.
     */
    protected String findPersonAttribute(ModelObject modelPerson, OSAXModel model, int attributeIndex, String relationshipSetName){
        RelationshipSet relationshipSet = model.getRelationshipSet(relationshipSetName);
        Iterator<Tuple> iterator = relationshipSet.iterator();
        while(iterator.hasNext()){
            Tuple tuple = iterator.next();
            if(tuple.get(0).equals(modelPerson)){
                if(tuple.get(attributeIndex) instanceof NULL) return null;
                ModelObject value = (ModelObject)tuple.get(attributeIndex);
                return value.toString();
            }
        }
        return null;
    }

    /**
     * Generates and sends a FlaggedResult to the user.
     * @param error The main message for the Flagged Result.
     * @param fixes The list of possible fixes associated with the result.
     * @param probability The probability of the result.
     * @param pid The PID of the person the result should be attached to.
     * @param name The name of the person the result should be attached to.
     * @param generation The generation of the person the result should be attached to.
     * @param severity The severity of the result.
     * @throws IOException If the connection to the user has been lost.
     */
    protected void sendResult(String error, String[] fixes, double probability, String pid, String name, int generation, ResultSeverity severity)
        throws IOException
    {
        FlaggedResult result = new FlaggedResult(probability,pid,name,generation);
        result.setMessage(error);
        result.setFixes(fixes);
        result.setSeverity(severity);
        getUpdater().sendPersonResults(result);
    }

    /**
     * Gets the proper pronoun for a gender.
     * @param gender The gender
     * @return The pronoun referring to the gender.
     */
    protected String pronoun(String gender){
        if(gender.matches("Male")) return "he";
        else return "she";
    }

    /**
     * Gets the capitalized version of the pronoun for a gender.
     * @param gender The gender.
     * @return The capitalized pronoun referring to the gender.
     */
    protected String capitalPronoun(String gender){
        if(gender.matches("Male")) return "He";
        else return "She";
    }

    /**
     * Gets the possessive pronoun for a gender.
     * @param gender Obvious.
     * @return Obvious.
     */
    protected String possesive(String gender){
        if(gender.matches("Male")) return "his";
        else return "her";
    }

    /**
     * Gets the name for a person.
     * @param modelPerson The ModelObject representing a person.
     * @param model The OSAX model containing the name.
     * @return The person's name, or null if none could be found.
     */
    protected String name(ModelObject modelPerson, OSAXModel model){
        return findPersonAttribute(modelPerson, model, 1, "Person has Name");
    }

    /**
     * Gets the PID for a person.
     * @param modelPerson The ModelObject representing a person.
     * @param model The OSAX model containing the PID.
     * @return The person's PID, or null if none could be found.
     */
    protected String pid(ModelObject modelPerson, OSAXModel model){
        return findPersonAttribute(modelPerson, model, 1, "Person has Pid");
    }

    /**
     * Gets the gender for a person.
     * @param modelPerson The ModelObject representing a person.
     * @param model The OSAX model containing the gender.
     * @return The person's gender, or null if none could be found.
     */
    protected String gender(ModelObject modelPerson, OSAXModel model){
        return findPersonAttribute(modelPerson, model, 1, "Person has Gender");
    }

    /**
     * Gets the birth date for a person.
     * @param modelPerson The ModelObject representing a person.
     * @param model The OSAX model containing the birth date.
     * @return The person's birth date, or null if none could be found.
     */
    protected String birth(ModelObject modelPerson, OSAXModel model){
        return findPersonAttribute(modelPerson, model, 1, "Person has Birthdate");
    }

    /**
     * Gets the death date for a person.
     * @param modelPerson The ModelObject representing a person.
     * @param model The OSAX model containing the death date.
     * @return The person's death date, or null if none could be found.
     */
    protected String death(ModelObject modelPerson, OSAXModel model){
        return findPersonAttribute(modelPerson, model, 1, "Person has Deathdate");
    }

    /**
     * Gets the christening date for a person.
     * @param modelPerson The ModelObject representing a person.
     * @param model The OSAX model containing the christening date.
     * @return The person's christening date, or null if none could be found.
     */
    protected String christening(ModelObject modelPerson, OSAXModel model){
        return findPersonAttribute(modelPerson, model, 1, "Person has Christeningdate");
    }

    /**
     * Gets the generation for a person.
     * @param modelPerson The ModelObject representing a person.
     * @param model The OSAX model containing the generation.
     * @return The person's generation, or -1 if none could be found.
     */
    protected int generation(ModelObject modelPerson, OSAXModel model){
        String generationString = findPersonAttribute(modelPerson, model, 1, "Person has Generation");
        int generation;
        if(generationString != null) generation = Integer.parseInt(generationString);
        else generation = -1;
        return generation;
    }
}
