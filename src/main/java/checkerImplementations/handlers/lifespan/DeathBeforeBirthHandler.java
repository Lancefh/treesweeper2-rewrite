package checkerImplementations.handlers.lifespan;

import checkerImplementations.handlers.BaseHandler;
import checkerImplementations.handlers.results.FlaggedResult;
import checkerImplementations.handlers.results.ResultSeverity;
import osax.model.OSAXModel;
import osax.object.ModelObject;
import osax.relationSet.Tuple;
import server.IUpdater;

import java.io.IOException;

/**
 * This handles the constraint that checks whether a person died before they were born
 */
public class DeathBeforeBirthHandler extends BaseHandler {
    public DeathBeforeBirthHandler(IUpdater updater) {
        super(updater);
    }

    @Override
    protected void handleUnlikely(double probability, Tuple tuple, OSAXModel model) throws IOException {
        // Gather information for the user
        ModelObject person = (ModelObject)tuple.get(0);
        String name = name(person, model);
        String pid = pid(person, model);
        String gender = gender(person, model);
        String death = death(person, model);
        String birth = birth(person, model);
        int generation = generation(person, model);

        // Create FlaggedResults object
        String error = name + " died before " + pronoun(gender) + " was born.";
        sendResult(error, new String[]{
                "Was " + name + " born in " + birth + "?",
                "Did " + name + " die in " + death + "?"
        }, probability, pid, name, generation, ResultSeverity.IMPOSSIBLE);
    }

    @Override
    protected void handleImpossible(double probability, Tuple tuple, OSAXModel model) throws IOException {
        this.handleUnlikely(probability,tuple,model);
    }
}
