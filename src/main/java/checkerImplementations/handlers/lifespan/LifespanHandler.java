package checkerImplementations.handlers.lifespan;

import checkerImplementations.handlers.BaseHandler;
import checkerImplementations.handlers.results.FlaggedResult;
import checkerImplementations.handlers.results.ResultSeverity;
import osax.model.OSAXModel;
import osax.object.ModelObject;
import osax.relationSet.Tuple;
import server.IUpdater;

import java.io.IOException;

/**
 * Handles constraints concerning lifespans.
 */
public class LifespanHandler extends BaseHandler {
    public LifespanHandler(IUpdater socket) {
        super(socket);
    }

    @Override
    protected void handleUnlikely(double probability, Tuple tuple, OSAXModel model) throws IOException{
        // Gather information for the user
        ModelObject person = (ModelObject)tuple.get(0);
        String name = name(person, model);
        String pid = pid(person, model);
        String gender = gender(person, model);
        String birth = birth(person, model);
        String death = death(person, model);
        int generation = generation(person, model);
        String age = tuple.get(1).toString();

        // Create FlaggedResults object
        String error = name + " died when " + pronoun(gender) + " was " + age + " years old.";
        sendResult(error, new String[]{
                "Was " + name + " born in " + birth + "?",
                "Did " + name + " die in " + death + "?"
        }, probability, pid, name, generation, ResultSeverity.UNLIKELY);
    }

    @Override
    protected void handleImpossible(double probability, Tuple tuple, OSAXModel model) throws IOException {
        // Gather information for the user
        ModelObject person = (ModelObject)tuple.get(0);
        String name = name(person, model);
        String pid = pid(person, model);
        String gender = gender(person, model);
        String birth = birth(person, model);
        String death = death(person, model);
        int generation = generation(person, model);
        String age = tuple.get(1).toString();

        // Create FlaggedResults object
        String error = name + " died when " + pronoun(gender) + " was " + age + " years old.";
        sendResult(error, new String[]{
                "Was " + name + " born in " + birth + "?",
                "Did " + name + " die in " + death + "?"
        }, probability, pid, name, generation, ResultSeverity.IMPOSSIBLE);
    }
}
