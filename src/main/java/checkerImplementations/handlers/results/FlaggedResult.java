package checkerImplementations.handlers.results;

import java.util.UUID;

/**
 * Contains a result that has been flagged by a constraint.  Just a dumb data holder.
 */
public class FlaggedResult {
    private ResultSeverity type;
    private boolean okay;
    private String id;
    private String message;
    private String[] fixes;
    private double probability;
    private String pid;
    private String name;
    private int generation;


    public FlaggedResult(double probability, String pid, String name, int generation){
        this.type = null;
        this.message = null;
        this.fixes = new String[]{};
        this.okay = false;
        this.id = UUID.randomUUID().toString();
        this.probability = probability;
        this.pid = pid;
        this.name = name;
        this.generation = generation;
    }

    public void setSeverity(ResultSeverity type) {
        this.type = type;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setFixes(String[] fixes) {
        this.fixes = fixes;
    }

    public ResultSeverity getSeverity() {
        return type;
    }

    public String getMessage() {
        return message;
    }

    public String[] getFixes() {
        return fixes;
    }
}
