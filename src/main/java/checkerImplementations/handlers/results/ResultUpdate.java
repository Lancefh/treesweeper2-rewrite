package checkerImplementations.handlers.results;

import serialization.Serializer;

/**
 * Contains a FlaggedResult to send to the client
 */
public class ResultUpdate {
    private MessageType type;
    private FlaggedResult result;

    public ResultUpdate(FlaggedResult result){
        this.type = MessageType.RESULT;
        this.result = result;
    }

    public String toJson(){
        return Serializer.get().serialize(this);
    }
}
