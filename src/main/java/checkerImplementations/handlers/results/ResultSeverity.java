package checkerImplementations.handlers.results;

/**
 * The different types of results.
 */
public enum ResultSeverity {
    UNLIKELY, IMPOSSIBLE, RESEARCH
}
