package checkerImplementations.handlers.results;

import com.google.gson.Gson;

/**
 * Contains a message to be sent to the client.  Just a dumb data holder.
 */
public class MessageUpdate {
    private MessageType type;
    private String message;
    private String submessage;
    private int target;
    private int progress;

    public MessageUpdate(MessageType type, String message, String submessage){
        this.type = type;
        this.message = message;
        this.submessage = submessage;
        this.target = -1;
        this.progress = -1;
    }

    public MessageType getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }

    public String getSubmessage() {return submessage;}

    public int getTarget() {
        return target;
    }

    public void setTarget(int target) {
        this.target = target;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    /**
     * Converts the message into JSON.
     * @return A JSON string representing the message.
     */
    public String toJson(){
        return new Gson().toJson(this);
    }
}
