package checkerImplementations.handlers.results;

/**
 * Different types of messages to be sent to the client.
 */
public enum MessageType {
    UPDATE, ERROR, RESULT
}
