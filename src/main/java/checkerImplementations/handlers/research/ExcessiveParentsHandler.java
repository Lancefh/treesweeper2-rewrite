package checkerImplementations.handlers.research;

import checkerImplementations.handlers.BaseHandler;
import checkerImplementations.handlers.results.ResultSeverity;
import osax.model.OSAXModel;
import osax.object.ModelObject;
import osax.relationSet.Tuple;
import server.IUpdater;

import java.io.IOException;

/**
 * Handles results from the constraint that checks if people have too many
 * biological parents.
 */
public class ExcessiveParentsHandler extends BaseHandler {
    public ExcessiveParentsHandler(IUpdater updater) {
        super(updater);
    }

    @Override
    protected void handleUnlikely(double probability, Tuple tuple, OSAXModel model) throws IOException {
        // Gather information
        ModelObject person = (ModelObject)tuple.get(0);
        String name = name(person, model);
        String gender = gender(person, model);
        String pid = pid(person, model);
        int generation = generation(person, model);

        // Create FlaggedResults object
        String error = name + " has more than two biological parents.";
        sendResult(error, new String[]{
                "Are any of " + possesive(gender) + " parents duplicates?"
        }, probability, pid, name, generation, ResultSeverity.IMPOSSIBLE);
    }

    @Override
    protected void handleImpossible(double probability, Tuple tuple, OSAXModel model) throws IOException {
        this.handleUnlikely(probability,tuple,model);
    }
}
