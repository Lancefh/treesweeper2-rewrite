package checkerImplementations.handlers.research;

import checkerImplementations.handlers.BaseHandler;
import checkerImplementations.handlers.results.ResultSeverity;
import osax.model.OSAXModel;
import osax.object.ModelObject;
import osax.relationSet.Tuple;
import server.IUpdater;

import java.io.IOException;

/**
 * Handles a constraint that checks if a person has any vital dates:
 * birth date, christening date, death date.
 */
public class MissingVitalDatesHandler extends BaseHandler {
    public MissingVitalDatesHandler(IUpdater updater) {
        super(updater);
    }

    @Override
    protected void handleUnlikely(double probability, Tuple tuple, OSAXModel model) throws IOException {
        // Gather information
        ModelObject person = (ModelObject)tuple.get(0);
        String name = name(person, model);
        String pid = pid(person, model);
        int generation = generation(person, model);
        String gender = gender(person, model);

        // Create FlaggedResults object
        String error = name + " has no vital dates.";
        sendResult(error, new String[]{
                "Does " + pronoun(gender) + " have a recorded birth, christening, or death date?"
        }, probability, pid, name, generation, ResultSeverity.RESEARCH);
    }

    @Override
    protected void handleImpossible(double probability, Tuple tuple, OSAXModel model) throws IOException {
        this.handleUnlikely(probability,tuple,model);
    }
}
