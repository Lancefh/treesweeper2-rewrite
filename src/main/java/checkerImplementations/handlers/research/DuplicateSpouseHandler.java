package checkerImplementations.handlers.research;

import checkerImplementations.handlers.BaseHandler;
import checkerImplementations.handlers.results.ResultSeverity;
import osax.model.OSAXModel;
import osax.object.ModelObject;
import osax.relationSet.Tuple;
import server.IUpdater;

import java.io.IOException;

/**
 * Handles a constraint that checks if a person as duplicate spouses.
 */
public class DuplicateSpouseHandler extends BaseHandler {
    public DuplicateSpouseHandler(IUpdater updater) {
        super(updater);
    }

    @Override
    protected void handleUnlikely(double probability, Tuple tuple, OSAXModel model) throws IOException {
        ModelObject person = (ModelObject)tuple.get(0);
        ModelObject spouse1 = (ModelObject)tuple.get(1);
        ModelObject spouse2 = (ModelObject)tuple.get(2);

        String personName = name(person, model);
        String personPid = pid(person, model);
        int personGeneration = generation(person, model);

        String spouse1name = name(spouse1, model);
        String spouse1pid = pid(spouse1, model);
        String spouse2name = name(spouse2, model);
        String spouse2pid = pid(spouse2, model);

        String error = personName + " may have duplicate spouses. Look at " + spouse1name + " (" + spouse1pid + ") and " + spouse2name + " (" + spouse2pid + ").";
        sendResult(error, new String[]{}, probability, personPid, personName, personGeneration, ResultSeverity.RESEARCH);
    }

    @Override
    protected void handleImpossible(double probability, Tuple tuple, OSAXModel model) throws IOException {
        this.handleUnlikely(probability, tuple, model);
    }
}
