package checkerImplementations.handlers.research;

import checkerImplementations.handlers.BaseHandler;
import checkerImplementations.handlers.results.FlaggedResult;
import checkerImplementations.handlers.results.ResultSeverity;
import osax.model.OSAXModel;
import osax.object.ModelObject;
import osax.relationSet.Tuple;
import server.IUpdater;

import java.io.IOException;

/**
 * Handles the constraint that checks whether a couple is possibly missing children.
 */
public class MissingChildHandler extends BaseHandler {
    public MissingChildHandler(IUpdater updater) {
        super(updater);
    }

    @Override
    protected void handleUnlikely(double probability, Tuple tuple, OSAXModel model) throws IOException {
        // Gather information for the user
        ModelObject husband = (ModelObject)tuple.get(0);
        String husbandName = name(husband, model);
        String husbandPid = pid(husband, model);
        int husbandGeneration = generation(husband, model);

        ModelObject wife = (ModelObject)tuple.get(1);
        String wifeName = name(wife, model);
        String wifePid = pid(wife, model);
        int wifeGeneration = generation(wife, model);

        // Create FlaggedResults object for husband
        String error = husbandName + " and " + wifeName + " got married when " + wifeName + " was young enough to have children," +
                " but they don't have any. They may be missing children.";
        sendResult(error, new String[]{}, probability, husbandPid, husbandName, husbandGeneration, ResultSeverity.RESEARCH);

        // Create FlaggedResults object for wife
        error = wifeName + " and " + husbandName + " got married when " + wifeName + " was young enough to have children," +
                " but they don't have any. They may be missing children.";
        sendResult(error, new String[]{}, probability, wifePid, wifeName, wifeGeneration, ResultSeverity.RESEARCH);
    }

    @Override
    protected void handleImpossible(double probability, Tuple tuple, OSAXModel model) throws IOException {
        this.handleUnlikely(probability, tuple, model);
    }
}
