package checkerImplementations.handlers.research;

import checkerImplementations.handlers.BaseHandler;
import checkerImplementations.handlers.results.ResultSeverity;
import osax.model.OSAXModel;
import osax.object.ModelObject;
import osax.relationSet.Tuple;
import server.IUpdater;

import java.io.IOException;

/**
 * Handles a constraint that checks if a parent has duplicate children
 */
public class DuplicateChildrenHandler extends BaseHandler {
    public DuplicateChildrenHandler(IUpdater updater) {
        super(updater);
    }

    @Override
    protected void handleUnlikely(double probability, Tuple tuple, OSAXModel model) throws IOException {
        ModelObject parent = (ModelObject)tuple.get(0);
        ModelObject child1 = (ModelObject)tuple.get(1);
        ModelObject child2 = (ModelObject)tuple.get(2);

        String parentName = name(parent, model);
        String child1Name = name(child1, model);
        String child1pid = pid(child1, model);
        String child2Name = name(child2, model);
        String child2pid = pid(child2, model);

        String parentPid = pid(parent, model);
        int parentGeneration = generation(parent, model);

        String error = parentName + " may have duplicate children. Look at " + child1Name + " (" + child1pid + ") and " + child2Name + " (" + child2pid + ").";
        sendResult(error, new String[]{}, probability, parentPid, parentName, parentGeneration, ResultSeverity.RESEARCH);
    }

    @Override
    protected void handleImpossible(double probability, Tuple tuple, OSAXModel model) throws IOException {
        this.handleUnlikely(probability, tuple, model);
    }
}
