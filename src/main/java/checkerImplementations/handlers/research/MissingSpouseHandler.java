package checkerImplementations.handlers.research;

import checkerImplementations.handlers.BaseHandler;
import checkerImplementations.handlers.results.FlaggedResult;
import checkerImplementations.handlers.results.ResultSeverity;
import osax.model.OSAXModel;
import osax.object.ModelObject;
import osax.relationSet.Tuple;
import server.IUpdater;

import java.io.IOException;

/**
 * Handles the constraint that checks whether someone may be missing a spouse
 */
public class MissingSpouseHandler extends BaseHandler {
    public MissingSpouseHandler(IUpdater updater) {
        super(updater);
    }

    @Override
    protected void handleUnlikely(double probability, Tuple tuple, OSAXModel model) throws IOException {
        // Gather information
        ModelObject person = (ModelObject)tuple.get(0);
        String name = name(person, model);
        String gender = gender(person, model);
        String pid = pid(person, model);
        int generation = generation(person, model);

        // Create FlaggedResults object
        String error = name + " was less than 50 years old when " + possesive(gender) + " last spouse died. "
                + capitalPronoun(gender) + " may be missing a spouse.";
        sendResult(error, new String[]{}, probability, pid, name, generation, ResultSeverity.RESEARCH);
    }

    @Override
    protected void handleImpossible(double probability, Tuple tuple, OSAXModel model) throws IOException {
        this.handleUnlikely(probability,tuple,model);
    }
}
