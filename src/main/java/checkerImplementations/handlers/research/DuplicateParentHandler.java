package checkerImplementations.handlers.research;

import checkerImplementations.handlers.BaseHandler;
import checkerImplementations.handlers.results.ResultSeverity;
import osax.model.OSAXModel;
import osax.object.ModelObject;
import osax.relationSet.Tuple;
import server.IUpdater;

import java.io.IOException;

/**
 * Handles a constraint that checks for duplicate parents for a child
 */
public class DuplicateParentHandler extends BaseHandler {
    public DuplicateParentHandler(IUpdater updater) {
        super(updater);
    }

    @Override
    protected void handleUnlikely(double probability, Tuple tuple, OSAXModel model) throws IOException {
        ModelObject child = (ModelObject)tuple.get(0);
        ModelObject parent1 = (ModelObject)tuple.get(1);
        ModelObject parent2 = (ModelObject)tuple.get(2);

        String childName = name(child, model);
        String parent1Name = name(parent1, model);
        String parent1pid = pid(parent1, model);
        String parent2Name = name(parent2, model);
        String parent2pid = pid(parent2, model);

        String childPid = pid(child, model);
        int childGeneration = generation(child, model);

        String error = childName + " may have duplicate parents. Look at " + parent1Name + " (" + parent1pid + ") and " + parent2Name + " (" + parent2pid + ").";
        sendResult(error, new String[]{}, probability, childPid, childName, childGeneration, ResultSeverity.RESEARCH);
    }

    @Override
    protected void handleImpossible(double probability, Tuple tuple, OSAXModel model) throws IOException {
        this.handleUnlikely(probability,tuple,model);
    }
}
