package checkerImplementations.handlers.research;

import checkerImplementations.handlers.BaseHandler;
import checkerImplementations.handlers.results.FlaggedResult;
import checkerImplementations.handlers.results.ResultSeverity;
import osax.model.OSAXModel;
import osax.object.ModelObject;
import osax.relationSet.Tuple;
import server.IUpdater;

import java.io.IOException;

/**
 * Handles the constraint that checks if a person's gender is known
 */
public class UnknownGenderHandler extends BaseHandler {
    public UnknownGenderHandler(IUpdater updater) {
        super(updater);
    }

    @Override
    protected void handleUnlikely(double probability, Tuple tuple, OSAXModel model) throws IOException {
        // Gather information
        ModelObject person = (ModelObject)tuple.get(0);
        String name = name(person, model);
        String pid = pid(person, model);
        int generation = generation(person, model);

        // Create FlaggedResults object
        String error = name + " has an unknown gender.";
        sendResult(error, new String[]{}, probability, pid, name, generation, ResultSeverity.RESEARCH);
    }

    @Override
    protected void handleImpossible(double probability, Tuple tuple, OSAXModel model) throws IOException {
        this.handleUnlikely(probability,tuple,model);
    }
}
