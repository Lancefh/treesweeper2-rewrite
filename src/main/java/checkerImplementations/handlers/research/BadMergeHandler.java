package checkerImplementations.handlers.research;

import checkerImplementations.handlers.BaseHandler;
import checkerImplementations.handlers.results.FlaggedResult;
import checkerImplementations.handlers.results.ResultSeverity;
import osax.model.OSAXModel;
import osax.object.ModelObject;
import osax.relationSet.Tuple;
import server.IUpdater;

import java.io.IOException;

/**
 * Handles the constraint that checks whether someone may be badly merged.
 */
public class BadMergeHandler extends BaseHandler {
    public BadMergeHandler(IUpdater socket) {
        super(socket);
    }

    @Override
    protected void handleUnlikely(double probability, Tuple tuple, OSAXModel model) throws IOException{
        // Gather information
        ModelObject person = (ModelObject)tuple.get(0);
        String name = name(person, model);
        String pid = pid(person, model);
        int generation = generation(person, model);

        String numChildren = tuple.get(1).toString();
        String numSpouses = tuple.get(2).toString();

        // Create FlaggedResults object
        String error = name + " has excessive children (" + numChildren + ") or spouses (" + numSpouses + "), and may be a bad merge.";
        sendResult(error, new String[]{}, probability, pid, name, generation, ResultSeverity.RESEARCH);
    }

    @Override
    protected void handleImpossible(double probability, Tuple tuple, OSAXModel model) throws IOException{
        this.handleUnlikely(probability, tuple, model);
    }
}
