package checkerImplementations.handlers.research;

import checkerImplementations.handlers.BaseHandler;
import checkerImplementations.handlers.results.FlaggedResult;
import checkerImplementations.handlers.results.ResultSeverity;
import osax.model.OSAXModel;
import osax.object.ModelObject;
import osax.relationSet.Tuple;
import server.IUpdater;

import java.io.IOException;

/**
 * Handles the constraint that determines whether someone is their own ancestor.
 */
public class OwnAncestorHandler extends BaseHandler {
    public OwnAncestorHandler(IUpdater socket) {
        super(socket);
    }

    @Override
    protected void handleUnlikely(double probability, Tuple tuple, OSAXModel model) throws IOException {
        // Gather information
        ModelObject person = (ModelObject)tuple.get(0);
        String name = name(person, model);
        String gender = gender(person, model);
        String pid = pid(person, model);
        int generation = generation(person, model);

        // Create FlaggedResults object
        String error = name + " is " + possesive(gender) + " own ancestor.";
        sendResult(error, new String[]{}, probability, pid, name, generation, ResultSeverity.IMPOSSIBLE);
    }

    @Override
    protected void handleImpossible(double probability, Tuple tuple, OSAXModel model) throws IOException {
        this.handleUnlikely(probability,tuple,model);
    }
}
