package checkerImplementations.handlers.marriage;

import checkerImplementations.handlers.BaseHandler;
import checkerImplementations.handlers.results.FlaggedResult;
import checkerImplementations.handlers.results.ResultSeverity;
import osax.model.OSAXModel;
import osax.object.ModelObject;
import osax.relationSet.Tuple;
import server.IUpdater;

import java.io.IOException;

/**
 * Handles the constraint that checks whether someone died before their marriage.
 */
public class AliveAtMarriageHandler extends BaseHandler {
    public AliveAtMarriageHandler(IUpdater updater) {
        super(updater);
    }

    @Override
    protected void handleUnlikely(double probability, Tuple tuple, OSAXModel model) throws IOException {
        // Gather details for user
        ModelObject person = (ModelObject)tuple.get(0);
        String name = name(person, model);
        String gender = gender(person, model);
        String pid = pid(person, model);
        int generation = generation(person, model);
        String deathdate = death(person, model);

        ModelObject spouse = (ModelObject)tuple.get(1);
        String spouseName = name(spouse, model);

        String marriagdate = tuple.get(2).toString();

        // Create FlaggedResults object
        String error = name + " got married to " + spouseName + " after " + pronoun(gender) + " died.";
        sendResult(error, new String[]{
                "Did " + name + " get married to " + spouseName + "?",
                "Did " + name + " die in " + deathdate + "?",
                "Did " + name + " marry " + spouseName + " in " + marriagdate + "?"
        }, probability, pid, name, generation, ResultSeverity.IMPOSSIBLE);
    }

    @Override
    protected void handleImpossible(double probability, Tuple tuple, OSAXModel model) throws IOException {
        this.handleUnlikely(probability,tuple,model);
    }
}
