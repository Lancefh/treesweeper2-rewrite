package checkerImplementations.handlers.marriage;

import checkerImplementations.handlers.BaseHandler;
import checkerImplementations.handlers.results.FlaggedResult;
import checkerImplementations.handlers.results.ResultSeverity;
import osax.model.OSAXModel;
import osax.object.ModelObject;
import osax.relationSet.Tuple;
import server.IUpdater;

import java.io.IOException;

/**
 * Handles the constraint that checks whether a person was born before their marriage
 */
public class BornAfterMarriageHandler extends BaseHandler {
    public BornAfterMarriageHandler(IUpdater updater) {
        super(updater);
    }

    @Override
    protected void handleUnlikely(double probability, Tuple tuple, OSAXModel model) throws IOException {
        // Gather details for user
        ModelObject person = (ModelObject)tuple.get(0);
        String name = name(person, model);
        String gender = gender(person, model);
        String pid = pid(person, model);
        int generation = generation(person, model);
        String birth = birth(person, model);

        ModelObject spouse = (ModelObject)tuple.get(1);
        String spouseName = name(spouse, model);

        String marriagdate = tuple.get(2).toString();

        // Create FlaggedResults object
        String error = name + " got married to " + spouseName + " before " + pronoun(gender) + " was born.";
        sendResult(error, new String[]{
                "Did " + name + " marry " + spouseName + "?",
                "Did " + name + " marry " + spouseName + " in " + marriagdate + "?",
                "Was " + name + " born in " + birth + "?"
        }, probability, pid, name, generation, ResultSeverity.IMPOSSIBLE);
    }

    @Override
    protected void handleImpossible(double probability, Tuple tuple, OSAXModel model) throws IOException {
        this.handleUnlikely(probability,tuple,model);
    }
}
