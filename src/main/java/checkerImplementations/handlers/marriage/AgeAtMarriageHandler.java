package checkerImplementations.handlers.marriage;

import checkerImplementations.handlers.BaseHandler;
import checkerImplementations.handlers.results.FlaggedResult;
import checkerImplementations.handlers.results.ResultSeverity;
import osax.model.OSAXModel;
import osax.object.ModelObject;
import osax.relationSet.Tuple;
import server.IUpdater;

import java.io.IOException;

/**
 * Handler for the constraint that checks the age at which someone got married for the first time.
 */
public class AgeAtMarriageHandler extends BaseHandler {
    public AgeAtMarriageHandler(IUpdater socket) {
        super(socket);
    }

    @Override
    protected void handleUnlikely(double probability, Tuple tuple, OSAXModel model) throws IOException {
        // Gather information for the user
        ModelObject person = (ModelObject)tuple.get(0);
        String personName = name(person, model);
        String personPid = pid(person, model);
        String personGender = gender(person, model);
        String personBirth = birth(person, model);
        int generation = generation(person, model);
        String personAge = tuple.get(1).toString();
        String marriageDate = tuple.get(3).toString();

        ModelObject spouse = (ModelObject)tuple.get(2);
        String spouseName = name(spouse, model);

        // Create FlaggedResults object
        String error = personName + " married " + spouseName + " when " + pronoun(personGender) + " was " + personAge +
                " years old.";
        sendResult(error, new String[]{
                "Did " + personName + " marry " + spouseName + "?",
                "Was " + personName + " born in " + personBirth + "?",
                "Did " + personName + " marry " + spouseName + " in " + marriageDate + "?"
        }, probability, personPid, personName, generation, ResultSeverity.UNLIKELY);
    }

    @Override
    protected void handleImpossible(double probability, Tuple tuple, OSAXModel model) throws IOException{
        // Gather information for the user
        ModelObject person = (ModelObject)tuple.get(0);
        String personName = name(person, model);
        String personPid = pid(person, model);
        String personGender = gender(person, model);
        String personBirth = birth(person, model);
        int generation = generation(person, model);
        String personAge = tuple.get(1).toString();
        String marriageDate = tuple.get(3).toString();

        ModelObject spouse = (ModelObject)tuple.get(2);
        String spouseName = name(spouse, model);

        // Create FlaggedResults object
        String error = personName + " married " + spouseName + " when " + pronoun(personGender) + " was " + personAge +
                " years old.";
        sendResult(error, new String[]{
                "Did " + personName + " marry " + spouseName + "?",
                "Was " + personName + " born in " + personBirth + "?",
                "Did " + personName + " marry " + spouseName + " in " + marriageDate + "?"
        }, probability, personPid, personName, generation, ResultSeverity.IMPOSSIBLE);
    }
}
