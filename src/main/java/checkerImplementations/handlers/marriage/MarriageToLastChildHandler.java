package checkerImplementations.handlers.marriage;

import checkerImplementations.handlers.BaseHandler;
import checkerImplementations.handlers.results.FlaggedResult;
import checkerImplementations.handlers.results.ResultSeverity;
import osax.model.OSAXModel;
import osax.object.ModelObject;
import osax.relationSet.Tuple;
import server.IUpdater;

import java.io.IOException;

/**
 * Handles the constraint that checks years after marriage to last child
 */
public class MarriageToLastChildHandler extends BaseHandler {
    public MarriageToLastChildHandler(IUpdater updater) {
        super(updater);
    }

    @Override
    protected void handleUnlikely(double probability, Tuple tuple, OSAXModel model) throws IOException {
        // Gather information for the user
        ModelObject husband = (ModelObject)tuple.get(0);
        String husbandName = name(husband, model);
        String husbandPid = pid(husband, model);
        int husbandGeneration = generation(husband, model);

        ModelObject wife = (ModelObject)tuple.get(1);
        String wifeName = name(wife, model);
        String wifePid = pid(wife, model);
        int wifeGeneration = generation(wife, model);

        ModelObject child = (ModelObject)tuple.get(2);
        String childName = name(child, model);
        String childBirth = birth(child, model);

        String years = tuple.get(3).toString();

        String marriage = tuple.get(4).toString();

        // Create FlaggedResults object for husband
        String error = husbandName + " and " + wifeName + " had their last child with a recorded birth date (" + childName + ") after being married for " +
                years + " years.";
        sendResult(error, new String[]{
                "Are any of their children missing birth dates?",
                "Are " + husbandName + " and " + wifeName + " the parents of " + childName + "?",
                "Did " + husbandName + " and " + wifeName + " get married in " + marriage + "?",
                "Was " + childName + " born in " + childBirth + "?"
        }, probability, husbandPid, husbandName, husbandGeneration, ResultSeverity.UNLIKELY);

        // Create FlaggedResults object for wife
        error = wifeName + " and " + husbandName + " had their last child with a recorded birth date (" + childName + ") after being married for " +
                years + " years.";
        sendResult(error, new String[]{
                "Are any of their children missing birth dates?",
                "Are " + wifeName + " and " + husbandName + " the parents of " + childName + "?",
                "Did " + wifeName + " and " + husbandName + " get married in " + marriage + "?",
                "Was " + childName + " born in " + childBirth + "?"
        }, probability, wifePid, wifeName, wifeGeneration, ResultSeverity.UNLIKELY);
    }

    @Override
    protected void handleImpossible(double probability, Tuple tuple, OSAXModel model) throws IOException {
        // Gather information for the user
        ModelObject husband = (ModelObject)tuple.get(0);
        String husbandName = name(husband, model);
        String husbandPid = pid(husband, model);
        int husbandGeneration = generation(husband, model);

        ModelObject wife = (ModelObject)tuple.get(1);
        String wifeName = name(wife, model);
        String wifePid = pid(wife, model);
        int wifeGeneration = generation(wife, model);

        ModelObject child = (ModelObject)tuple.get(2);
        String childName = name(child, model);
        String childBirth = birth(child, model);

        String years = tuple.get(3).toString();

        String marriage = tuple.get(4).toString();

        // Create FlaggedResults object for husband
        String error = husbandName + " and " + wifeName + " had their last child with a recorded birth date (" + childName + ") after being married for " +
                years + " years.";
        sendResult(error, new String[]{
                "Are any of their children missing birth dates?",
                "Are " + husbandName + " and " + wifeName + " the parents of " + childName + "?",
                "Did " + husbandName + " and " + wifeName + " get married in " + marriage + "?",
                "Was " + childName + " born in " + childBirth + "?"
        }, probability, husbandPid, husbandName, husbandGeneration, ResultSeverity.IMPOSSIBLE);

        // Create FlaggedResults object for wife
        error = wifeName + " and " + husbandName + " had their last child with a recorded birth date (" + childName + ") after being married for " +
                years + " years.";
        sendResult(error, new String[]{
                "Are any of their children missing birth dates?",
                "Are " + wifeName + " and " + husbandName + " the parents of " + childName + "?",
                "Did " + wifeName + " and " + husbandName + " get married in " + marriage + "?",
                "Was " + childName + " born in " + childBirth + "?"
        }, probability, wifePid, wifeName, wifeGeneration, ResultSeverity.IMPOSSIBLE);
    }
}
