package checkerImplementations.handlers.marriage;

import checkerImplementations.handlers.BaseHandler;
import checkerImplementations.handlers.results.ResultSeverity;
import osax.model.OSAXModel;
import osax.object.ModelObject;
import osax.object.lexical.DOUBLE;
import osax.relationSet.Tuple;
import server.IUpdater;

import java.io.IOException;

/**
 * Handles a constraint that checks the age gap between spouses
 */
public class AgeGapBetweenSpousesHandler extends BaseHandler {
    public AgeGapBetweenSpousesHandler(IUpdater updater) {
        super(updater);
    }

    @Override
    protected void handleUnlikely(double probability, Tuple tuple, OSAXModel model) throws IOException {
        ModelObject husband = (ModelObject)tuple.get(0);
        ModelObject wife = (ModelObject)tuple.get(1);
        ModelObject gap = (ModelObject)tuple.get(2);

        String husbandName = name(husband, model);
        String husbandPid = pid(husband, model);
        int husbandGeneration = generation(husband, model);

        String wifeName = name(wife, model);
        String wifePid = pid(wife, model);
        int wifeGeneration = generation(wife, model);

        double value = ((DOUBLE)gap).getValue();

        String error = "There was a " + value + " year age gap between " + husbandName + " and " + wifeName + " when they got married.";
        sendResult(error, new String[]{
                "Is " + husbandName + "'s birth year correct?",
                "Is " + wifeName + "'s birth year correct?",
                "Is their marriage date correct?"
        }, probability, husbandPid, husbandName, husbandGeneration, ResultSeverity.UNLIKELY);

        sendResult(error, new String[]{
                "Is " + husbandName + "'s birth year correct?",
                "Is " + wifeName + "'s birth year correct?",
                "Is their marriage date correct?"
        }, probability, wifePid, wifeName, wifeGeneration, ResultSeverity.UNLIKELY);
    }

    @Override
    protected void handleImpossible(double probability, Tuple tuple, OSAXModel model) throws IOException {
        ModelObject husband = (ModelObject)tuple.get(0);
        ModelObject wife = (ModelObject)tuple.get(1);
        ModelObject gap = (ModelObject)tuple.get(2);

        String husbandName = name(husband, model);
        String husbandPid = pid(husband, model);
        int husbandGeneration = generation(husband, model);

        String wifeName = name(wife, model);
        String wifePid = pid(wife, model);
        int wifeGeneration = generation(wife, model);

        double value = ((DOUBLE)gap).getValue();
        if(value < 0) value *= -1;

        String error = "There was a " + value + " year age gap between " + husbandName + " and " + wifeName + " when they got married.";
        sendResult(error, new String[]{
                "Is " + husbandName + "'s birth year correct?",
                "Is " + wifeName + "'s birth year correct?",
                "Is their marriage date correct?"
        }, probability, husbandPid, husbandName, husbandGeneration, ResultSeverity.IMPOSSIBLE);

        sendResult(error, new String[]{
                "Is " + husbandName + "'s birth year correct?",
                "Is " + wifeName + "'s birth year correct?",
                "Is their marriage date correct?"
        }, probability, wifePid, wifeName, wifeGeneration, ResultSeverity.IMPOSSIBLE);
    }
}
