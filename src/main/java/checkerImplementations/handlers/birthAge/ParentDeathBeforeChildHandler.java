package checkerImplementations.handlers.birthAge;

import checkerImplementations.handlers.BaseHandler;
import checkerImplementations.handlers.results.FlaggedResult;
import checkerImplementations.handlers.results.ResultSeverity;
import osax.model.OSAXModel;
import osax.object.ModelObject;
import osax.relationSet.Tuple;
import server.IUpdater;

import java.io.IOException;

/**
 * Handles the constraint that checks if a parent died before their child was born
 */
public class ParentDeathBeforeChildHandler extends BaseHandler {
    public ParentDeathBeforeChildHandler(IUpdater updater) {
        super(updater);
    }

    @Override
    protected void handleUnlikely(double probability, Tuple tuple, OSAXModel model) throws IOException {
        // Gather information for the user
        ModelObject parent = (ModelObject)tuple.get(0);
        String parentName = name(parent, model);
        String parentPid = pid(parent, model);
        String parentGender = gender(parent, model);
        String parentDeath = death(parent, model);
        int generation = generation(parent, model);

        ModelObject child = (ModelObject)tuple.get(1);
        String childName = name(child, model);
        String childBirth = birth(child, model);

        // Create FlaggedResults object
        String error = parentName + " had " + possesive(parentGender) + " child " + childName + " after " +
                pronoun(parentGender) + " died.";
        sendResult(error, new String[]{
                "Did " + parentName + " die in " + parentDeath + "?",
                "Was " + childName + " born in " + childBirth + "?"
        }, probability, parentPid, parentName, generation, ResultSeverity.IMPOSSIBLE);
    }

    @Override
    protected void handleImpossible(double probability, Tuple tuple, OSAXModel model) throws IOException {
        this.handleUnlikely(probability,tuple,model);
    }
}
