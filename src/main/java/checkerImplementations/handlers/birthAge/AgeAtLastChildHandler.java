package checkerImplementations.handlers.birthAge;

import checkerImplementations.handlers.BaseHandler;
import checkerImplementations.handlers.results.FlaggedResult;
import checkerImplementations.handlers.results.ResultSeverity;
import osax.model.OSAXModel;
import osax.object.ModelObject;
import osax.relationSet.Tuple;
import server.IUpdater;

import java.io.IOException;

/**
 * Handles the constraint that checks age at which someone had their last child
 */
public class AgeAtLastChildHandler extends BaseHandler {
    public AgeAtLastChildHandler(IUpdater updater) {
        super(updater);
    }

    @Override
    protected void handleUnlikely(double probability, Tuple tuple, OSAXModel model) throws IOException {
        // Gather information for the user
        ModelObject parent = (ModelObject)tuple.get(0);
        String parentName = name(parent, model);
        String parentPid = pid(parent, model);
        String parentGender = gender(parent, model);
        String parentBirth = birth(parent, model);
        int generation = generation(parent, model);
        String parentAge = tuple.get(2).toString();

        ModelObject child = (ModelObject)tuple.get(1);
        String childName = name(child, model);
        String childBirth = birth(child, model);

        // Create FlaggedResults object
        String error = parentName + " had " + possesive(parentGender) + " last child, " + childName + ", when " + pronoun(parentGender) +
                " was " + parentAge + " years old.";
        sendResult(error, new String[]{
                "Was " + parentName + " born in " + parentBirth + "?",
                "Was " + childName + " born in " + childBirth + "?"
        },probability, parentPid, parentName, generation, ResultSeverity.UNLIKELY);
    }

    @Override
    protected void handleImpossible(double probability, Tuple tuple, OSAXModel model) throws IOException {
        // Gather information for the user
        ModelObject parent = (ModelObject)tuple.get(0);
        String parentName = name(parent, model);
        String parentPid = pid(parent, model);
        String parentGender = gender(parent, model);
        String parentBirth = birth(parent, model);
        int generation = generation(parent, model);
        String parentAge = tuple.get(2).toString();

        ModelObject child = (ModelObject)tuple.get(1);
        String childName = name(child, model);
        String childBirth = birth(child, model);

        // Create FlaggedResults object
        String error = parentName + " had " + possesive(parentGender) + " last child, " + childName + ", when " + pronoun(parentGender) +
                " was " + parentAge + " years old.";
        sendResult(error, new String[]{
                "Was " + parentName + " born in " + parentBirth + "?",
                "Was " + childName + " born in " + childBirth + "?"
        },probability, parentPid, parentName, generation, ResultSeverity.IMPOSSIBLE);
    }
}
