package checkerImplementations.handlers.birthAge;

import checkerImplementations.handlers.BaseHandler;
import checkerImplementations.handlers.results.FlaggedResult;
import checkerImplementations.handlers.results.ResultSeverity;
import osax.model.OSAXModel;
import osax.object.ModelObject;
import osax.relationSet.Tuple;
import server.IUpdater;

import java.io.IOException;

/**
 * Handles constraints to do with the age at which a parent had a child.
 */
public class AgeAtFirstChildHandler extends BaseHandler {
    public AgeAtFirstChildHandler(IUpdater socket) {
        super(socket);
    }

    @Override
    protected void handleUnlikely(double probability, Tuple tuple, OSAXModel model) throws IOException{
        // Gather information for the user
        ModelObject parent = (ModelObject)tuple.get(0);
        String parentName = name(parent, model);
        String parentPid = pid(parent, model);
        String parentGender = gender(parent, model);
        String parentBirth = birth(parent, model);
        int generation = generation(parent, model);
        String parentAge = tuple.get(2).toString();

        ModelObject child = (ModelObject)tuple.get(1);
        String childName = name(child, model);
        String childBirth = birth(child, model);

        // Create FlaggedResults object
        String error = parentName + " had " + possesive(parentGender) + " first child, " + childName + ", when " + pronoun(parentGender) +
                " was " + parentAge + " years old.";
        sendResult(error, new String[]{
                "Was " + parentName + " born in " + parentBirth + "?",
                "Was " + childName + " born in " + childBirth + "?"
        }, probability, parentPid, parentName, generation, ResultSeverity.UNLIKELY);
    }

    @Override
    protected void handleImpossible(double probability, Tuple tuple, OSAXModel model) throws IOException{
        // Gather information for the user
        ModelObject parent = (ModelObject)tuple.get(0);
        String parentName = name(parent, model);
        String parentPid = pid(parent, model);
        String parentGender = gender(parent, model);
        String parentBirth = birth(parent, model);
        int generation = generation(parent, model);
        String parentAge = tuple.get(2).toString();

        ModelObject child = (ModelObject)tuple.get(1);
        String childName = name(child, model);
        String childBirth = birth(child, model);


        // Create FlaggedResults object
        String error = parentName + " had " + possesive(parentGender) + " first child, " + childName + ", when " + pronoun(parentGender) +
                " was " + parentAge + " years old.";
        sendResult(error, new String[]{
                "Was " + parentName + " born in " + parentBirth + "?",
                "Was " + childName + " born in " + childBirth + "?"
        }, probability, parentPid, parentName, generation, ResultSeverity.IMPOSSIBLE);
    }
}
