package checkerImplementations.handlers.christening;

import checkerImplementations.handlers.BaseHandler;
import checkerImplementations.handlers.results.FlaggedResult;
import checkerImplementations.handlers.results.ResultSeverity;
import osax.model.OSAXModel;
import osax.object.ModelObject;
import osax.relationSet.Tuple;
import server.IUpdater;

import java.io.IOException;

/**
 * Handles the christening age constraint.
 */
public class AgeAtChristeningHandler extends BaseHandler {
    public AgeAtChristeningHandler(IUpdater socket) {
        super(socket);
    }

    @Override
    protected void handleUnlikely(double probability, Tuple tuple, OSAXModel model) throws IOException {
        // Gather information for the user
        ModelObject person = (ModelObject)tuple.get(0);
        String name = name(person, model);
        String pid = pid(person, model);
        String gender = gender(person, model);
        String christening = christening(person, model);
        String birth = birth(person, model);
        int generation = generation(person, model);
        String age = tuple.get(1).toString();

        // Create FlaggedResults object
        String error = name + " was christened when " + pronoun(gender) + " was " + age + " years old.";
        sendResult(error, new String[]{
                "Was " + name + " born in " + birth + "?",
                "Was " + name + " christened in " + christening + "?"
        }, probability, pid, name, generation, ResultSeverity.UNLIKELY);
    }

    @Override
    protected void handleImpossible(double probability, Tuple tuple, OSAXModel model) throws IOException{
        // Gather information for the user
        ModelObject person = (ModelObject)tuple.get(0);
        String name = name(person, model);
        String pid = pid(person, model);
        String gender = gender(person, model);
        String christening = christening(person, model);
        String birth = birth(person, model);
        int generation = generation(person, model);
        String age = tuple.get(1).toString();

        // Create FlaggedResults object
        String error = name + " was christened when " + pronoun(gender) + " was " + age + " years old.";
        sendResult(error, new String[]{
                "Was " + name + " born in " + birth + "?",
                "Was " + name + " christened in " + christening + "?"
        }, probability, pid, name, generation, ResultSeverity.IMPOSSIBLE);
    }
}
