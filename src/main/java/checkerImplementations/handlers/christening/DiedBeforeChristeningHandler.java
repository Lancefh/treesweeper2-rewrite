package checkerImplementations.handlers.christening;

import checkerImplementations.handlers.BaseHandler;
import checkerImplementations.handlers.results.FlaggedResult;
import checkerImplementations.handlers.results.ResultSeverity;
import osax.model.OSAXModel;
import osax.object.ModelObject;
import osax.relationSet.Tuple;
import server.IUpdater;

import java.io.IOException;

/**
 * Handles the constraint that checks whether someone died before they were christened
 */
public class DiedBeforeChristeningHandler extends BaseHandler {
    public DiedBeforeChristeningHandler(IUpdater updater) {
        super(updater);
    }

    @Override
    protected void handleUnlikely(double probability, Tuple tuple, OSAXModel model) throws IOException {
        // Gather information for the user
        ModelObject person = (ModelObject)tuple.get(0);
        String name = name(person, model);
        String pid = pid(person, model);
        String gender = gender(person, model);
        String christening = christening(person, model);
        String death = death(person, model);
        int generation = generation(person, model);

        // Create FlaggedResults object
        String error = name + " was christened after " + pronoun(gender) + " died.";
        sendResult(error, new String[]{
                "Did " + name + " die in " + death + "?",
                "Was " + name + " christened in " + christening + "?"
        }, probability, pid, name, generation, ResultSeverity.IMPOSSIBLE);
    }

    @Override
    protected void handleImpossible(double probability, Tuple tuple, OSAXModel model) throws IOException {
        this.handleUnlikely(probability,tuple,model);
    }
}
