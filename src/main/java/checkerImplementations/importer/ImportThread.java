package checkerImplementations.importer;

import checkerImplementations.importer.familySearchModel.extensibleData.hypermediaEnabledData.conclusion.subject.ChildAndParentsRelationship;
import checkerImplementations.importer.organization.Family;
import checkerImplementations.importer.familySearchModel.extensibleData.hypermediaEnabledData.gedcomx.FamilySearchPlatform;
import checkerImplementations.importer.organization.FamilyTreeNode;
import checkerImplementations.importer.familySearchModel.extensibleData.hypermediaEnabledData.conclusion.subject.Person;
import checkerImplementations.importer.organization.ParentInfo;
import checkerImplementations.importer.proxy.IProxy;
import injector.DependencyInjector;
import interfaces.DataImportException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Gathers all the information for a family--a person, their spouses, and their children.
 */
public class ImportThread implements IImportThread {
    private IProxy proxy;
    private String pid;
    private String token;
    private int generation;
    private Family family;
    private DataImportException error;
    private boolean done;
    private String status;
    private String details;
    private int throttled;
    private boolean keepRunning;

    private String name;

    private Thread thread;

    public ImportThread(String pid, String token, int generation){
        this.proxy = DependencyInjector.get().proxy();
        this.pid = pid;
        this.token = token;
        this.generation = generation;
        this.family = null;
        this.error = null;
        this.done = false;
        status = "Created";
        throttled = -1;
        details = null;
        keepRunning = true;
        thread = null;
        name = null;
    }

    /**
     * Tells the thread to stop running.
     */
    @Override
    public void kill(){
        keepRunning = false;
    }

    /**
     * Gets the Family object the thread was building.
     * @return a Family object, or null if the thread failed to run to completion (meaning an exception was thrown).
     */
    @Override
    public Family getFamily(){
        if(!done) return null;
        return family;
    }

    /**
     * Gets a string describing the status of the thread.
     * @return Obvious.
     */
    @Override
    public String getStatus(){
        StringBuilder sb = new StringBuilder();
        sb.append(pid).append(":");
        if(this.thread != null) sb.append(this.thread.getState()).append(":");
        sb.append(status);
        if(throttled >= 0) sb.append(":THROTTLED=").append(throttled);
        if(details != null) sb.append(":DETAILS=").append(details);
        return sb.toString();
    }

    /**
     * Sets the 'details' component of the status, used to describe in greater detail what the thread is doing.
     * @param details The string to set as 'details.'
     */
    @Override
    public void setDetails(String details){this.details = details;}

    /**
     * Tells how long the thread has been throttled for.
     * @return The number of seconds the thread has been throttled for, or -1 if it's not currently throttled.
     */
    @Override
    public int getThrottled(){return throttled;}

    /**
     * Sets the time the thread has been throttled for.
     * @param throttledLength The number of seconds the thread has been throttled for, or -1 if the thread is not currently throttled.
     */
    @Override
    public void setThrottled(int throttledLength){throttled = throttledLength;}

    /**
     * Returns a DataImport Exception that was thrown during the normal execution of the thread.
     * @return An error describing what went wrong during the import process, or null if no such error occurred.
     */
    @Override
    public DataImportException getError(){return error;}

    @Override
    public void setName(String name) {
        this.name = name;
    }

    public String getName(){return name;}

    /**
     * Downloads and stores the data on a family: an ancestor, all their spouses, and all their children.
     */
    @Override
    public void start() {
        thread = new Thread(() -> {
            try {
                if(!keepRunning) return;
                this.proxy.setThread(this);
                status = "Creating root node";
                // Create root node (the person whose family this is)
                FamilySearchPlatform rootDetails = proxy.getDetails(pid, token);
                if(rootDetails == null){
                    this.error = new DataImportException("Pid: " + pid + ", root details was null.");
                    return;
                }
                FamilyTreeNode ancestorNode = new FamilyTreeNode(rootDetails, generation);
                ancestorNode.setAncestor(true);

                // Create Family using ancestor node
                this.family = new Family(ancestorNode);

                // Create spouse nodes
                if(!keepRunning) return;
                status = "Creating spouse nodes";
                FamilySearchPlatform spouses = proxy.getSpouses(pid, token);
                if(spouses != null){
                    for(Person spouse : spouses.getPersons()){
                        if(!keepRunning) return;
                        FamilySearchPlatform details = proxy.getDetails(spouse.getId(), token);
                        if(details != null){
                            FamilyTreeNode spouseNode = new FamilyTreeNode(details, generation);
                            this.family.addSpouse(spouseNode);
                        }
                    }
                }

                status = "Creating children nodes";
                if(!keepRunning) return;
                // Create children nodes
                FamilySearchPlatform children = proxy.getChildren(pid, token);
                status = "Getting details for children";
                if(children != null){
                    Map<String, ParentInfo> childrenRelationships = new HashMap<>();
                    for(ChildAndParentsRelationship r : children.getChildAndParentsRelationships()){
                        // In some circumstances, the same child may be listed multiple times in the relationships
                        // with different relationship types. To account for this, we use the map to ensure that only
                        // a single copy of each child is registered.
                        String id = r.getChild().getResourceId();
                        boolean isFather = r.getFather() != null &&
                                r.getFather().getResourceId() != null &&
                                r.getFather().getResourceId().equals(pid);
                        if(isFather){
                            if(r.getFatherFacts() == null || r.getFatherFacts().length == 0){
                                // Father type is unknown; add Unknown relationship type if none is already present
                                if(childrenRelationships.get(id) == null) childrenRelationships.put(id, new ParentInfo("Unknown", "Father"));
                            } else {
                                // Father type is contained in first father fact
                                String type = r.getFatherFacts()[0].getType();
                                // Add no matter what--override any Unknown value that may already be there
                                childrenRelationships.put(id, new ParentInfo(type, "Father"));
                            }
                        } else {
                            if(r.getMotherFacts() == null || r.getMotherFacts().length == 0){
                                // Mother type is unknown; add Unknown relationship type if none is already present
                                if(childrenRelationships.get(id) == null) childrenRelationships.put(id, new ParentInfo("Unknown", "Mother"));
                            } else {
                                // Mother type is contained in first mother fact
                                String type = r.getMotherFacts()[0].getType();
                                // Add no matter what--override any Unknown value that may already be there
                                childrenRelationships.put(id, new ParentInfo(type, "Mother"));
                            }
                        }
                    }
                    for(String pid : childrenRelationships.keySet()){
                        if(!keepRunning) return;
                        status = "Getting details for child " + pid;
                        FamilySearchPlatform details = proxy.getDetails(pid, token);
                        if(details != null){
                            FamilyTreeNode childNode = new FamilyTreeNode(details, generation - 1);
                            this.family.addChild(childNode, childrenRelationships.get(pid));
                        }
                    }
                }

                // Get parents
                status = "Creating parent nodes";
                if(!keepRunning) return;
                FamilySearchPlatform parents = proxy.getParents(pid, token);
                status = "Getting details for parents";
                if(parents != null){
                    for(ChildAndParentsRelationship r : parents.getChildAndParentsRelationships()){
                        // Find mother type
                        if(r.getMother() != null){
                            String motherPid = r.getMother().getResourceId();
                            String type = "Unknown";
                            if(r.getMotherFacts() != null && r.getMotherFacts().length > 0) type = r.getMotherFacts()[0].getType();
                            status = "Getting details for mother " + motherPid;
                            FamilySearchPlatform details = proxy.getDetails(motherPid, token);
                            if(details != null){
                                FamilyTreeNode parentNode = new FamilyTreeNode(details, generation + 1);
                                this.family.addParent(parentNode, new ParentInfo(type, "mother"));
                            }
                        }
                        // Find father type
                        if(r.getFather() != null){
                            String fatherPid = r.getFather().getResourceId();
                            String type = "Unknown";
                            if(r.getFatherFacts() != null && r.getFatherFacts().length > 0) type = r.getFatherFacts()[0].getType();
                            status = "Getting details for father " + fatherPid;
                            FamilySearchPlatform details = proxy.getDetails(fatherPid, token);
                            if(details != null){
                                FamilyTreeNode parentNode = new FamilyTreeNode(details, generation + 1);
                                this.family.addParent(parentNode, new ParentInfo(type, "father"));
                            }
                        }
                    }
                }

                status = "Finished";
                done = true;
            }catch(DataImportException e){
                this.error = e;
            }
        });
        thread.start();
    }

    @Override
    public void join() throws InterruptedException {
        thread.join();
    }

    @Override
    public Thread.State getState() {
        return thread != null ? thread.getState() : null;
    }
}
