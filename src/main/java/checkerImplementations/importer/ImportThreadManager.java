package checkerImplementations.importer;

import server.IUpdater;

import java.io.IOException;
import java.util.*;

/**
 * Manages import threads and gives output to console and user about their status.
 */
public class ImportThreadManager {
    private long statusInterval;
    private Timer timer;
    private List<IImportThread> threadPool;
    private long numUpdates;
    private IUpdater updater;
    private boolean sendConsoleUpdates;
    private boolean sendUserUpdates;

    public ImportThreadManager(long statusInterval, boolean sendConsoleUpdates, boolean sendUserUpdates){
        this.statusInterval = statusInterval;
        this.timer = new Timer();
        this.threadPool = new ArrayList<>();
        this.timer.scheduleAtFixedRate(new StatusUpdate(this),0,statusInterval);
        numUpdates = 0;
        updater = null;
        this.sendConsoleUpdates = sendConsoleUpdates;
        this.sendUserUpdates = sendUserUpdates;
    }

    /**
     * Sets the updater that will deliver output to the user.
     * @param updater Obvious.
     */
    public void setUpdater(IUpdater updater){this.updater = updater;}

    /**
     * Adds a thread to the thread pool for observation. Also can activate updates.
     * @param thread The thread to add to the pool.
     * @param activateConsoleUpdates True if you want to receive console updates from now on, false otherwise.
     * @param activateUserUpdates True if you want to receive user updates from now on, false otherwise.
     */
    public void addThread(IImportThread thread, boolean activateConsoleUpdates, boolean activateUserUpdates){
        synchronized(this) {
            if(activateConsoleUpdates) sendConsoleUpdates = true;
            if(activateUserUpdates) sendUserUpdates = true;
            threadPool.add(thread);
        }
    }

    /**
     * Terminates all updates from the thread manager. Doesn't stop threads from running.
     */
    public void terminateUpdates(){
        timer.cancel();
        if(sendConsoleUpdates) System.out.println("** TERMINATING THREAD POOL UPDATES ****************");
    }

    /**
     * Gets an iterator to the thread pool.
     * @return
     */
    public Iterator<IImportThread> iterator(){return threadPool.iterator();}

    /**
     * Prints the current status of the thread pool. This method is synchronized.
     */
    public void printStatus(){
        synchronized(this) {
            ++numUpdates;
            // Print console update
            if(sendConsoleUpdates) System.out.println("** START THREAD POOL STATUS " + numUpdates + " *******************");
            if(sendConsoleUpdates) for(IImportThread thread : threadPool) if(!thread.getState().equals(Thread.State.TERMINATED)) System.out.println(thread.getStatus());
            if(sendConsoleUpdates) System.out.println("** END THREAD POOL STATUS *************************");

            // Send user update
            if(updater != null && sendUserUpdates) {
                int numCompleted = 0;
                int numRunning = 0;
                int numSleeping = 0;
                // Determine if the threads are being throttled, and if so, for how long. Also determine how many threads
                // are complete.
                int throttleTime = Integer.MAX_VALUE;
                for (IImportThread thread : threadPool) {
                    if(thread.getState() == null) return;
                    if (thread.getState().equals(Thread.State.TERMINATED)) ++numCompleted;
                    else if (thread.getState().equals(Thread.State.TIMED_WAITING)){
                        ++numSleeping;
                        if(thread.getThrottled() >= 0 && thread.getThrottled() < throttleTime) throttleTime = thread.getThrottled();
                    }
                    else if(thread.getState().equals(Thread.State.RUNNABLE)) ++numRunning;
                }
                // Generate a status message to send to the user.
                String status = "Received data for " + numCompleted + " out of " + threadPool.size() + " ancestors...";
                String submessage = null;
                if(numRunning == 0 && numSleeping > 0) submessage = "Waiting for Family Search servers...";
                try {
                    updater.sendProgressUpdate(status, submessage, threadPool.size(), numCompleted);
                    if(throttleTime < Integer.MAX_VALUE) updater.sendConsoleMessage("Throttled for " + throttleTime + "seconds.");
                }catch(IOException e){
                    // The socket is lost, terminate everything.
//                    e.printStackTrace();
                    terminateUpdates();
                    for(IImportThread thread : threadPool)thread.kill();
                }
                if(numRunning == 0 && numSleeping == 0){
                    // Stop updates if nothing is happening.
                    sendConsoleUpdates = false;
                    sendUserUpdates = false;
                }
            }
        }
    }

    /**
     * A class that triggers updates every time it fires.
     */
    private class StatusUpdate extends TimerTask {
        private ImportThreadManager parent;

        public StatusUpdate(ImportThreadManager parent){this.parent = parent;}

        @Override
        public void run() {
            parent.printStatus();
        }
    }
}
