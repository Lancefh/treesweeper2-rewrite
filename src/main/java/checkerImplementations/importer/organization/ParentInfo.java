package checkerImplementations.importer.organization;

/**
 * This is a dumb data holder that stores information about a parent
 */
public class ParentInfo {
    private String type;            // The type of parent: biological, adopted, etc.
    private String relationship;    // The type of relationship: mother, father, unknown.

    public ParentInfo(String type, String relationship){
        this.type = type;
        this.relationship = relationship;
    }

    public String getType() {
        return type;
    }

    public String getRelationship() {
        return relationship;
    }
}
