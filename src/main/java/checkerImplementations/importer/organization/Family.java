package checkerImplementations.importer.organization;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A Family contains an ancestor, all their spouses, and all their children.
 */
public class Family {
    private FamilyTreeNode ancestor;
    private List<FamilyTreeNode> spouses;
    private List<FamilyTreeNode> children;
    private Map<FamilyTreeNode, ParentInfo> parents;
    private boolean isFather;

    public Family(FamilyTreeNode ancestor){
        this.ancestor = ancestor;
        spouses = new ArrayList<>();
        children = new ArrayList<>();
        parents = new HashMap<>();
        isFather = ancestor.getDetails().getPersons()[0].getGender().getType().matches("http://gedcomx.org/Male");
    }

    public void addSpouse(FamilyTreeNode spouse){
        ancestor.addSpousePid(spouse.getPid());
        spouse.addSpousePid(ancestor.getPid());
        spouses.add(spouse);
    }

    public void addChild(FamilyTreeNode child, ParentInfo info){
        ancestor.addChildPid(child.getPid(), info.getType());
        if(isFather)child.addFatherPid(ancestor.getPid(), info.getType());
        else child.addMotherPid(ancestor.getPid(), info.getType());
        children.add(child);
    }

    public void addParent(FamilyTreeNode parent, ParentInfo info){
        boolean isFather = info.getRelationship().equals("father");
        if(isFather) ancestor.addFatherPid(parent.getPid(), info.getType());
        else ancestor.addMotherPid(parent.getPid(), info.getType());
        parent.addChildPid(ancestor.getPid(), info.getType());
        parents.put(parent, info);
    }

    public FamilyTreeNode getAncestor() {
        return ancestor;
    }

    public List<FamilyTreeNode> getSpouses() {
        return spouses;
    }

    public List<FamilyTreeNode> getChildren() {
        return children;
    }

    public List<FamilyTreeNode> getParents(){
        return new ArrayList<>(parents.keySet());
    }
}
