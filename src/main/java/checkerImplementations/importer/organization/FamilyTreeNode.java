package checkerImplementations.importer.organization;

import checkerImplementations.importer.familySearchModel.extensibleData.hypermediaEnabledData.gedcomx.FamilySearchPlatform;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This contains the information about a ancestry from FamilySearch, while making it easier to
 * get information about their family relationships.
 */
public class FamilyTreeNode {

    private FamilySearchPlatform details;
    private String pid;
    private ArrayList<String> fatherPids;
    private ArrayList<String> motherPids;
    private ArrayList<String> childrenPids;
    private ArrayList<String> spousePids;
    private int generation;
    private boolean isAncestor;
    private Map<String, String> relationshipTypes;
    private Map<String, String> childRelationshipTypes;

    /**
     * Builds a node with no parent or child relationships.
     * @param details The response for /platform/tree/persons/{pid} of the ancestry.
     */
    public FamilyTreeNode(FamilySearchPlatform details, int generation){
        this.details = details;
        this.pid = details.getPersons()[0].getId();
        this.fatherPids = new ArrayList<>();
        this.motherPids = new ArrayList<>();
        this.childrenPids = new ArrayList<>();
        this.spousePids = new ArrayList<>();
        this.generation = generation;
        this.isAncestor = false;
        this.relationshipTypes = new HashMap<>();
        this.childRelationshipTypes = new HashMap<>();
    }

    public FamilyTreeNode(FamilyTreeNode clone){
        this.details = clone.details;
        this.pid = clone.pid;
        this.fatherPids = clone.fatherPids;
        this.motherPids = clone.motherPids;
        this.childrenPids = clone.childrenPids;
        this.spousePids = clone.spousePids;
        this.generation = clone.generation;
        this.isAncestor = clone.isAncestor;
        this.relationshipTypes = clone.relationshipTypes;
        this.childRelationshipTypes = clone.childRelationshipTypes;
    }

    /**
     * Merges the data from another node into this one.
     * Please note that if there are errors where data that has been gathered
     * seems to be spontaneously lost or duplicated, this method is often at fault!
     * @param other The other node to merge with this one
     */
    public void merge(FamilyTreeNode other){
        if(other == null) return;
        if(this.pid.matches(other.pid)){
            for(String fatherPid : other.fatherPids){
                if(!this.fatherPids.contains(fatherPid)) this.fatherPids.add(fatherPid);
            }
            for(String motherPid : other.motherPids){
                if(!this.motherPids.contains(motherPid)) this.motherPids.add(motherPid);
            }
            for(String childPid : other.childrenPids){
                if(!this.childrenPids.contains(childPid)) this.childrenPids.add(childPid);
            }
            for(String spousePid : other.spousePids){
                if(!this.spousePids.contains(spousePid)) this.spousePids.add(spousePid);
            }
            if(!this.isAncestor && other.isAncestor) this.isAncestor = true;
            for(String pid : other.relationshipTypes.keySet()){
                String relationship = other.relationshipTypes.get(pid);
                this.relationshipTypes.putIfAbsent(pid, relationship);
            }
            for(String pid : other.childRelationshipTypes.keySet()){
                String relationship = other.childRelationshipTypes.get(pid);
                this.childRelationshipTypes.putIfAbsent(pid, relationship);
            }
        }
    }

    public boolean isParentOf(String pid){
        for(String childPid : childrenPids){
            if(childPid.matches(pid)) return true;
        }
        return false;
    }

    public boolean isSpouseOf(String pid){
        for(String spousePid : spousePids){
            if(spousePid.matches(pid)) return true;
        }
        return false;
    }

    public boolean isAncestor(){return isAncestor;}

    public void setAncestor(boolean isAncestor){this.isAncestor = isAncestor;}

    public int getGeneration() {
        return generation;
    }

    @Override
    public String toString(){
        return "[Pid: " + pid + " Fathers: " + fatherPids + " Mothers: " + motherPids + " Children: " + childrenPids + "]";
    }

    @Override
    public int hashCode(){
        return pid.hashCode();
    }

    @Override
    public boolean equals(Object o){
        if(o == null) return false;
        if(!(o instanceof FamilyTreeNode)) return false;
        FamilyTreeNode other = (FamilyTreeNode)o;
        return other.pid.matches(this.pid);
    }

    public void addChildPid(String pid, String type){
        childrenPids.add(pid);
        childRelationshipTypes.put(pid, type);
    }

    public void addSpousePid(String pid){ spousePids.add(pid);}

    public FamilySearchPlatform getDetails() {
        return details;
    }

    public String getPid() {
        return pid;
    }

    public List<String> getFatherPids() {
        return fatherPids;
    }

    public List<String> getMotherPids() {
        return motherPids;
    }

    public List<String> getChildrenPids() {
        return childrenPids;
    }

    public List<String> getSpousePids() { return spousePids; }

    public void addFatherPid(String fatherPid, String type){
        fatherPids.add(fatherPid);
        relationshipTypes.put(fatherPid, type);
    }

    public void addMotherPid(String motherPid, String type){
        motherPids.add(motherPid);
        relationshipTypes.put(motherPid, type);
    }

    public String getParentRelationshipType(String pid){
        return relationshipTypes.get(pid);
    }

    public String getChildRelationshipType(String pid){
        return childRelationshipTypes.get(pid);
    }
}
