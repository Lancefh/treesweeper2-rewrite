package checkerImplementations.importer.organization;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

/**
 * Holds a family tree made of FamilyTreeNode; this gives access to all ancestry information while simplifying
 * access to individual ancestry objects
 */
public class FamilyTreeData {
    private HashMap<String, FamilyTreeNode> persons;
    private String rootPersonPid;

    public FamilyTreeData(){
        persons = new HashMap<>();
        rootPersonPid = null;
    }

    /**
     * This constructor takes the root node of the tree.
     * @param rootNode The node containing the ancestry with ascendancy number = 1.
     */
    public FamilyTreeData(FamilyTreeNode rootNode){
        persons = new HashMap<>();
        persons.put(rootNode.getPid(), rootNode);
        rootPersonPid = rootNode.getPid();
    }

    public List<FamilyTreeNode> getFathersOfPerson(String pid){
        FamilyTreeNode person = persons.get(pid);
        if(person == null) return null;             // The ancestry isn't even in the data set
        List<FamilyTreeNode> fatherNodes = new ArrayList<>();
        for(String fatherPid: person.getFatherPids()){
            FamilyTreeNode father = persons.get(fatherPid);
            if(father != null) fatherNodes.add(father);
        }
        return fatherNodes;
    }

    public List<FamilyTreeNode> getMothersOfPerson(String pid){
        FamilyTreeNode person = persons.get(pid);
        if(person == null) return null;             // The ancestry isn't even in the data set
        List<FamilyTreeNode> motherNodes = new ArrayList<>();
        for(String motherPid : person.getMotherPids()){
            FamilyTreeNode mother = persons.get(motherPid);
            if(mother != null) motherNodes.add(mother);
        }
        return motherNodes;
    }

    public List<FamilyTreeNode> getChildrenOfPerson(String pid){
        FamilyTreeNode person = persons.get(pid);
        if(person == null) return null;             // The ancestry isn't even in the data set
        List<FamilyTreeNode> childrenNodes = new ArrayList<>();
        for(String childPid : person.getChildrenPids()){
            FamilyTreeNode child = persons.get(childPid);
            if(child != null) childrenNodes.add(child);
        }
        return childrenNodes;
    }

    public List<FamilyTreeNode> getSpousesOfPerson(String pid){
        FamilyTreeNode person = persons.get(pid);
        if(person == null) return null;             // The ancestry isn't even in the data set
        List<FamilyTreeNode> spouseNodes = new ArrayList<>();
        for(String spousePid : person.getSpousePids()){
            FamilyTreeNode spouse = persons.get(spousePid);
            if(spouse != null) spouseNodes.add(spouse);
        }
        return spouseNodes;
    }

    public FamilyTreeNode getRootPerson(){ return persons.get(rootPersonPid);}

    public FamilyTreeNode getPerson(String pid){
        return persons.get(pid);
    }

    public void addPerson(FamilyTreeNode node){
        FamilyTreeNode oldNode = persons.get(node.getPid());
        if (oldNode != null) oldNode.merge(node);
        else persons.put(node.getPid(), node);
    }

    public String getRootPersonPid() {
        return rootPersonPid;
    }

    public void setRootPersonPid(String rootPersonPid) {
        this.rootPersonPid = rootPersonPid;
    }

    public Collection<FamilyTreeNode> getAllNodes(){
        return persons.values();
    }

    public int size(){ return persons.size();}
}
