package checkerImplementations.importer;

import checkerImplementations.importer.organization.Family;
import checkerImplementations.importer.organization.FamilyTreeData;
import checkerImplementations.importer.organization.FamilyTreeNode;
import checkerImplementations.importer.familySearchModel.extensibleData.Date;
import checkerImplementations.importer.familySearchModel.extensibleData.hypermediaEnabledData.conclusion.Gender;
import checkerImplementations.importer.familySearchModel.extensibleData.hypermediaEnabledData.conclusion.subject.ChildAndParentsRelationship;
import checkerImplementations.importer.familySearchModel.extensibleData.hypermediaEnabledData.conclusion.Fact;
import checkerImplementations.importer.familySearchModel.extensibleData.hypermediaEnabledData.conclusion.subject.Person;
import checkerImplementations.importer.familySearchModel.extensibleData.hypermediaEnabledData.conclusion.subject.Relationship;
import checkerImplementations.importer.familySearchModel.extensibleData.hypermediaEnabledData.SourceReference;
import checkerImplementations.importer.familySearchModel.extensibleData.hypermediaEnabledData.gedcomx.FamilySearchPlatform;
import checkerImplementations.importer.organization.ParentInfo;
import checkerImplementations.importer.proxy.IProxy;
import dates.DateRange;
import dates.FormalDateConverter;
import dates.JulianDayConverter;
import injector.DependencyInjector;
import interfaces.DataImportException;
import interfaces.IDataImporter;
import osax.model.OSAXModel;
import osax.object.ModelObject;
import osax.object.lexical.INTEGER;
import osax.object.lexical.NULL;
import osax.object.lexical.STRING;
import osax.object.nonlexical.NonLexicalObject;
import osax.relationSet.Tuple;
import osax.relationSet.TupleMismatchException;
import server.IUpdater;
import server.ImportOptions;

import java.io.IOException;
import java.util.*;

/**
 * The importer for Family Search. Gathers a user's family history data and imports it into the OSA-X model.
 */
public class FamilySearchImporter implements IDataImporter {
    private static final String CHRISTENING_EVENT_TYPE = "http://gedcomx.org/Christening";
    private static final String BURIAL_EVENT_TYPE = "http://gedcomx.org/Burial";
    private static final String MARRIAGE_EVENT_TYPE = "http://gedcomx.org/Marriage";
    private static final String BIRTH_EVENT_TYPE = "http://gedcomx.org/Birth";
    private static final String DEATH_EVENT_TYPE = "http://gedcomx.org/Death";
    private static final String MALE_GENDER = "http://gedcomx.org/Male";
    private static final String FEMALE_GENDER = "http://gedcomx.org/Female";
    private static final String UNKNOWN_GENDER = "http://gedcomx.org/Unknown";
    private static final String COUPLE_RELATIONSHIP_TYPE = "http://gedcomx.org/Couple";
    private static final String BIOLOGICAL_PARENT = "http://gedcomx.org/BiologicalParent";
    private static final String ADOPTIVE_PARENT = "http://gedcomx.org/AdoptiveParent";
    private static final int MIN_YEAR_CUTOFF = 32;

    private HashMap<Person, ModelObject> personMap;
    private ImportOptions options;
    private IUpdater updater;

    public FamilySearchImporter(ImportOptions options, IUpdater updater){
        this.updater = updater;
        this.options = options;
    }

    public ImportOptions getOptions() {
        return options;
    }

    /**
     * Checks if the current token is still valid (i.e., hasn't expired).
     * @param token The Family Search token
     * @param proxy The proxy to use.
     * @return True if the token is still valid, false if not.
     * @throws DataImportException If an error occurs while communicating with Family Search.
     */
    private boolean loggedIn(String token, IProxy proxy) throws DataImportException{
        FamilySearchPlatform users = proxy.getCurrentUser(token);
        if(users == null || users.getUsers() == null) return false;
        return users.getUsers()[0] != null;
    }

    /**
     * Determines the generation of a person, based on their ascendancy number.
     * @param person The Person whose generation is being checked.
     * @return Obvious.
     */
    private int determineGeneration(Person person){
        String rawNumber = person.getDisplay().getAscendancyNumber();
        try{
            int ascendancyNumber = Integer.parseInt(rawNumber);
            if (ascendancyNumber <= 0) return -1;
            else if (ascendancyNumber == 1) return 0;
            else return (int) (Math.log(ascendancyNumber) / Math.log(2));
        }catch(NumberFormatException e){
            return 0;
        }
    }

    /**
     * Populates the model with data from Family Search.
     * @param model The model to populate.
     * @throws DataImportException If any of a great many things go wrong.
     */
    @Override
    public void populate(OSAXModel model) throws DataImportException {
        // Get the family search token from options
        String token = options.getAuthorization();

        // Get a Family Search Proxy
        IProxy proxy = DependencyInjector.get().proxy();

        // Verify that user is logged in
        if(!loggedIn(token, proxy)){
            try {
                updater.sendErrorUpdate("Your session has expired. Please log in again to continue.");
                return;
            }catch(IOException e){
                throw new DataImportException(e.getMessage());
            }
        }

        // Get ancestry of person
        FamilySearchPlatform ancestry = proxy.getAncestry(options.getPid(), token, options.getNumGenerations());
        if(ancestry == null || ancestry.getPersons() == null) {
            throw new DataImportException("Ancestry for " + options.getPid() + " came back null.");
        }

        // Build array of threads to gather family history data
        try {
            // Send message alerting transition
            updater.sendMessageUpdate("Getting details about ancestry...",null);
        }catch(IOException e){throw new DataImportException(e.getMessage());}

        // Start threads
        ImportThreadManager manager = new ImportThreadManager(1000,false,true);
        manager.setUpdater(updater);
        for(int i = 0; i < ancestry.getPersons().length; ++i){
            String pid = ancestry.getPersons()[i].getId();
            int generation = determineGeneration(ancestry.getPersons()[i]);
            IImportThread thread = DependencyInjector.get().importThread(pid, token, generation);
            thread.setName(pid);
            thread.start();
            manager.addThread(thread,false, true);
        }

        // Wait for each thread to finish
        FamilyTreeData tree = new FamilyTreeData();
        Iterator<IImportThread> iterator = manager.iterator();
        while(iterator.hasNext()){
            IImportThread thread = iterator.next();
            try{
                // Let the thread complete its work.
                thread.join();
                Family family = thread.getFamily();
                if(family == null){
                    // Something went wrong.
                    DataImportException error = thread.getError();
                    if(error != null) throw error;
                    else throw new DataImportException("Family is null.");
                }

                // Collect the data from the thread.
                tree.addPerson(family.getAncestor());
                for(FamilyTreeNode node : family.getSpouses()) tree.addPerson(node);
                for(FamilyTreeNode node : family.getChildren()) tree.addPerson(node);
                for(FamilyTreeNode node : family.getParents()) tree.addPerson(node);
            }catch(Exception e){
                if(e instanceof InterruptedException) Thread.currentThread().interrupt();
                else if(e instanceof DataImportException) throw (DataImportException)e;
            }
        }
        manager.terminateUpdates();

        // Send message that importing is done
        try {
            updater.sendMessageUpdate("All data gathered, organizing it now...",null);
        }catch(IOException e){throw new DataImportException(e.getMessage());}

        // Populate model with data from tree
        personMap = new HashMap<>();
        for(FamilyTreeNode node : tree.getAllNodes()){
            try {
                // Get model object for person
                Person person = node.getDetails().getPersons()[0];
                ModelObject modelPerson = personMap.get(person);

                // Add all data about the person to the model
                if (modelPerson == null) {
                    modelPerson = addPersonNameRelationship(person, null, model);
                } else modelPerson = addPersonNameRelationship(person, modelPerson, model);
                addPersonPidRelationship(person, modelPerson, model);
                addPersonGenderRelationship(person, modelPerson, model);
                addPersonGenerationRelationship(node.getGeneration(), modelPerson, model);
                addPersonSourceRelationship(node.getDetails(), modelPerson, model);
                addPersonBirthdateRelationship(person, modelPerson, model);
                addPersonDeathdateRelationship(person, modelPerson, model);
                addPersonChristeningdateRelationship(person, modelPerson, model);
                addParentChildRelationship(tree, person, modelPerson, model, node);
                addSpouseRelationships(tree, person, modelPerson, model);
                if(node.isAncestor()) addAncestor(modelPerson, model);
            }catch(TupleMismatchException e){
//                e.printStackTrace();
                throw new DataImportException(e.getMessage());
            }
        }

        try{
            updater.sendMessageUpdate("Done organizing data, beginning to check constraints!",null);
        }catch(IOException e){
//            e.printStackTrace();
            throw new DataImportException("Socket lost.");
        }
    }

    /**
     * Marks someone as an ancestor.
     */
    private void addAncestor(ModelObject modelPerson, OSAXModel model) throws TupleMismatchException{
        Tuple tuple = new Tuple(modelPerson);
        model.getRelationshipSet("Person is an ancestor").insert(tuple);
    }

    /**
     * Adds an entry to the Person Name relationship set.
     */
    private ModelObject addPersonNameRelationship(Person person, ModelObject modelPerson, OSAXModel model)
            throws TupleMismatchException
    {
        if(modelPerson == null){
            modelPerson = new NonLexicalObject();
            model.getNonLexicalObjectSet("Person").addObject(modelPerson);
            personMap.put(person, modelPerson);
        }
        String name = person.getDisplay().getName();
        name = cleanName(name);
        ModelObject modelName = new STRING(name);
        model.getLexicalObjectSet("Name").addObject(modelName);
        Tuple tuple = new Tuple(modelPerson, modelName);
        model.getRelationshipSet("Person has Name").insert(tuple);
        return modelPerson;
    }

    /**
     * Escapes any problematic characters in a name.
     * @param original The original version of the name
     * @return The cleaned version of the name
     */
    private String cleanName(String original){
        return original.replaceAll("[?]", "/?");
    }

    /**
     * Adds an entry to the Person Pid relationship set.
     */
    private void addPersonPidRelationship(Person person, ModelObject modelPerson, OSAXModel model)
        throws TupleMismatchException
    {
        String pid = person.getId();
        if(pid == null) return;
        ModelObject modelPid = new STRING(pid);
        model.getLexicalObjectSet("Pid").addObject(modelPid);
        Tuple tuple = new Tuple(modelPerson, modelPid);
        model.getRelationshipSet("Person has Pid").insert(tuple);
    }

    /**
     * Adds an entry to the Person Gender relationship set.
     */
    private void addPersonGenderRelationship(Person person, ModelObject modelPerson, OSAXModel model)
        throws TupleMismatchException
    {
        String gender = interpretGender(person.getGender());
        if(gender == null) throw new TupleMismatchException("Unknown gender: " + person.getGender().getType());
        ModelObject modelGender = new STRING(gender);
        model.getLexicalObjectSet("Gender").addObject(modelGender);
        Tuple tuple = new Tuple(modelPerson, modelGender);
        model.getRelationshipSet("Person has Gender").insert(tuple);
    }

    /**
     * Gets a standard string representation from a Gender object
     * @param gender The Gender object
     * @return The string representation of the gender
     */
    private String interpretGender(Gender gender){
        switch(gender.getType()){
            case MALE_GENDER: return "Male";
            case FEMALE_GENDER: return "Female";
            case UNKNOWN_GENDER: return "Unknown";
            default: return null;
        }
    }

    /**
     * Adds an entry to the Person Generation relationship set.
     */
    private void addPersonGenerationRelationship(int generation, ModelObject modelPerson, OSAXModel model)
        throws TupleMismatchException
    {
        ModelObject modelGeneration = new INTEGER(Integer.toString(generation));
        model.getLexicalObjectSet("Generation").addObject(modelGeneration);
        Tuple tuple = new Tuple(modelPerson, modelGeneration);
        model.getRelationshipSet("Person has Generation").insert(tuple);
    }

    /**
     * Adds an entry to the Person Source relationship set.
     */
    private void addPersonSourceRelationship(FamilySearchPlatform details, ModelObject modelPerson, OSAXModel model)
        throws TupleMismatchException
    {
        // Get all sources for the person
        List<SourceReference> sources = new ArrayList<>();

        // Get all general sources
        SourceReference[] temp = details.getPersons()[0].getSources();
        if (temp != null) Collections.addAll(sources, temp);

        // Get general relationship sources
        if (details.getRelationships() != null && details.getRelationships().length > 0) {
            for (Relationship relationship : details.getRelationships()) {
                if (relationship.getSources() != null && relationship.getSources().length > 0) {
                    Collections.addAll(sources, relationship.getSources());
                }
            }
        }

        // Get all the child and parent relationship sources
        if (details.getChildAndParentsRelationships() != null && details.getChildAndParentsRelationships().length > 0) {
            for (ChildAndParentsRelationship relationship : details.getChildAndParentsRelationships()) {
                if (relationship.getSources() != null && relationship.getSources().length > 0) {
                    Collections.addAll(sources, relationship.getSources());
                }
            }
        }

        // Add relationship to sources
        for(SourceReference source : sources){
            ModelObject modelSource = new STRING(source.getDescriptionId());
            model.getLexicalObjectSet("Source").addObject(modelSource);
            Tuple tuple = new Tuple(modelPerson, modelSource);
            model.getRelationshipSet("Person has Source").insert(tuple);
        }
    }

    /**
     * Adds an entry to the Person Birthdate relationship set.
     */
    private void addPersonBirthdateRelationship(Person person, ModelObject modelPerson, OSAXModel model)
        throws TupleMismatchException
    {
        // Get date for birth
        String date = getDateForEventType(person, BIRTH_EVENT_TYPE);
        if(date == null){
            // Date is missing or failed to parse
            model.getRelationshipSet("Person has Birthdate").insert(new Tuple(modelPerson, new NULL()));
            return;
        }

        // Build object
        ModelObject modelBirthdate = new INTEGER(date);
        model.getLexicalObjectSet("Birthdate").addObject(modelBirthdate);
        Tuple tuple = new Tuple(modelPerson, modelBirthdate);
        model.getRelationshipSet("Person has Birthdate").insert(tuple);
    }

    /**
     * Adds an entry to the Person Deathdate relationship set.
     */
    private void addPersonDeathdateRelationship(Person person, ModelObject modelPerson, OSAXModel model)
        throws TupleMismatchException
    {
        // Get date for death
        String date = getDateForEventType(person, DEATH_EVENT_TYPE);
        if(date == null) return;

        // Build object
        ModelObject modelDeathdate = new INTEGER(date);
        model.getLexicalObjectSet("Deathdate").addObject(modelDeathdate);
        Tuple tuple = new Tuple(modelPerson, modelDeathdate);
        model.getRelationshipSet("Person has Deathdate").insert(tuple);
    }

    /**
     * Adds an entry to the Person Christeningdate relationship set.
     */
    private void addPersonChristeningdateRelationship(Person person, ModelObject modelPerson, OSAXModel model)
        throws TupleMismatchException
    {
        // Get date for christening
        String date = getDateForEventType(person, CHRISTENING_EVENT_TYPE);
        if(date == null) return;

        // Build object
        ModelObject modelChristeningdate = new INTEGER(date);
        model.getLexicalObjectSet("Christeningdate").addObject(modelChristeningdate);
        Tuple tuple = new Tuple(modelPerson, modelChristeningdate);
        model.getRelationshipSet("Person has Christeningdate").insert(tuple);
    }

    /**
     * Adds an entry to the Parent Child relationship set.
     */
    private void addParentChildRelationship(FamilyTreeData tree, Person person, ModelObject modelPerson, OSAXModel model, FamilyTreeNode node)
        throws TupleMismatchException
    {
        // Get children of person
        List<FamilyTreeNode> childrenNodes = tree.getChildrenOfPerson(person.getId());
        if(childrenNodes != null && childrenNodes.size() > 0){
            // Add person to Parent object sets
            model.getNonLexicalObjectSet("Parent").addObject(modelPerson);
            if(person.getGender().getType().matches(MALE_GENDER)) model.getNonLexicalObjectSet("Father").addObject(modelPerson);
            else if(person.getGender().getType().matches(FEMALE_GENDER)) model.getNonLexicalObjectSet("Mother").addObject(modelPerson);

            // Add children of person
            for(FamilyTreeNode childNode : childrenNodes){
                String type = interpretParentRelationship(node.getChildRelationshipType(childNode.getPid()));
                addParentChildRelationships(childNode, modelPerson, new String[]{"Child"}, type, model, true);
            }
        }

        // Add parents of person
        List<FamilyTreeNode> fatherList = tree.getFathersOfPerson(person.getId());
        List<FamilyTreeNode> motherList = tree.getMothersOfPerson(person.getId());
        if(fatherList.size() > 0 || motherList.size() > 0){
            model.getNonLexicalObjectSet("Child").addObject(modelPerson);
            // Add fathers
            for(FamilyTreeNode fatherNode : fatherList){
                String type = interpretParentRelationship(node.getParentRelationshipType(fatherNode.getPid()));
                addParentChildRelationships(fatherNode, modelPerson, new String[]{"Parent", "Father"}, type, model, false);
            }
            // Add mothers
            for(FamilyTreeNode motherNode : motherList){
                String type = interpretParentRelationship(node.getParentRelationshipType(motherNode.getPid()));
                addParentChildRelationships(motherNode, modelPerson, new String[]{"Parent", "Mother"}, type, model, false);
            }
        }
    }

    private void addParentChildRelationships(FamilyTreeNode relatedNode, ModelObject person, String[] objectSets,
                                             String relationshipType, OSAXModel model, boolean personIsParent)
        throws TupleMismatchException
    {
        Person relation = relatedNode.getDetails().getPersons()[0];
        ModelObject modelRelation = personMap.get(relation);
        // If relative hasn't been created in model yet, create them
        if(modelRelation == null){
            modelRelation = new NonLexicalObject();
            model.getNonLexicalObjectSet("Person").addObject(modelRelation);
            personMap.put(relation, modelRelation);
        }
        // Add relation to all necessary object sets
        for(String setName : objectSets){
            model.getNonLexicalObjectSet(setName).addObject(modelRelation);
        }
        // Generate new tuple
        STRING type = model.string(relationshipType);
        model.getLexicalObjectSet("Type").addObject(type);
        if(personIsParent) model.getRelationshipSet("Type of Parent has Child").insert(new Tuple(type, person, modelRelation));
        else model.getRelationshipSet("Type of Parent has Child").insert(new Tuple(type, modelRelation, person));
    }

    private String interpretParentRelationship(String type){
        switch(type){
            case BIOLOGICAL_PARENT: return "Biological";
            case ADOPTIVE_PARENT: return "Adopted";
            case "Unknown": return "Unknown";
            default: return "Other";
        }
    }

    /**
     * Adds an entry to the Husband Spouse relationship set.
     */
    private void addSpouseRelationships(FamilyTreeData tree, Person person, ModelObject modelPerson, OSAXModel model)
        throws TupleMismatchException
    {
        // Get spouses of person
        List<FamilyTreeNode> spouseNodes = tree.getSpousesOfPerson(person.getId());
        if(spouseNodes == null || spouseNodes.size() == 0) {
            return;
        }

        // Determine gender of person
        boolean isHusband;
        if (person.getGender().getType().matches(MALE_GENDER)) {
            isHusband = true;
        } else if (person.getGender().getType().matches(FEMALE_GENDER)) {
            isHusband = false;
        } else return;  // Their gender is unknown, can't put them into husband-wife relationship properly

        // Add person to proper spouse set
        if(isHusband) model.getNonLexicalObjectSet("Husband").addObject(modelPerson);
        else model.getNonLexicalObjectSet("Wife").addObject(modelPerson);

        // Add Husband Wife relationships
        for(FamilyTreeNode spouseNode : spouseNodes){
            Person spouse = spouseNode.getDetails().getPersons()[0];
            ModelObject modelSpouse = personMap.get(spouse);
            // If spouse hasn't been created in model yet, create them.
            if(modelSpouse == null){
                modelSpouse = new NonLexicalObject();
                model.getNonLexicalObjectSet("Person").addObject(modelSpouse);
                personMap.put(spouse, modelSpouse);
            }
            // Add spouse to correct spouse set
            if(isHusband) model.getNonLexicalObjectSet("Wife").addObject(modelSpouse);
            else model.getNonLexicalObjectSet("Husband").addObject(modelSpouse);
            // Get marriage date and location
            Fact marriageEvent = getMarriageToPersonEvent(tree.getPerson(person.getId()), spouse.getId());
            String marriageLocation;
            if(marriageEvent != null && marriageEvent.getPlace() != null) marriageLocation = marriageEvent.getPlace().getOriginal();
            else marriageLocation = null;
            String date;
            if(marriageEvent != null) date = extractDateStringFromDateObject(marriageEvent.getDate());
            else date = null;
            // Generate model objects for date and location
            ModelObject modelLocation;
            if(marriageLocation == null) modelLocation = new NULL();
            else modelLocation = new STRING(marriageLocation);
            model.getLexicalObjectSet("Location").addObject(modelLocation);
            ModelObject modelDate;
            if(date == null) modelDate = new NULL();
            else modelDate = new INTEGER(date);
            model.getLexicalObjectSet("Marriagedate").addObject(modelDate);

            // Generate new tuple
            if(isHusband) model.getRelationshipSet("Husband has Wife at Location on Marriagedate")
                    .insert(new Tuple(modelPerson, modelSpouse, modelLocation, modelDate));
            else model.getRelationshipSet("Husband has Wife at Location on Marriagedate")
                    .insert(new Tuple(modelSpouse, modelPerson, modelLocation, modelDate));
        }
    }

    /**
     * Finds the event representing a marriage between two particular people.
     * @param personNode The node of one of the spouses.
     * @param spousePid The pid of the other spouse.
     * @return The marriage fact for the marriage of the two people, or null if none can be found.
     */
    private Fact getMarriageToPersonEvent(FamilyTreeNode personNode, String spousePid) {
        Relationship[] relationships = personNode.getDetails().getRelationships();
        if(relationships == null) return null;
        for (Relationship r : relationships) {
            if (r.getType().matches(COUPLE_RELATIONSHIP_TYPE)) {
                // Found a couple relationship
                if (r.getPerson1().getResourceId().matches(spousePid) || r.getPerson2().getResourceId().matches(spousePid)) {
                    // Verified that it is a couple with the target spouse
                    Fact[] facts = r.getFacts();
                    if (facts != null) {
                        for (Fact f : facts) {
                            if (f.getType().matches(MARRIAGE_EVENT_TYPE)) {
                                // Found the marriage event
                                return f;
                            }
                        }
                    }
                }
            }
        }

        return null;
    }

    /**
     * Gets the date for a given type of event. If there are multiple instances of that type of event, this will
     * return the last one.
     * @param person The person with the event.
     * @param eventType The type of event to find.
     * @return The date for the given event, or null if no date is to be found.
     */
    private String getDateForEventType(Person person, String eventType){
        // Find event
        if(person.getFacts() == null) return null;
        Fact targetFact = null;
        for(Fact fact : person.getFacts()) if(fact.getType().matches(eventType)) targetFact = fact;
        if(targetFact == null || targetFact.getDate() == null) return null;

        // Get date for event
        String date = extractDateStringFromDateObject(targetFact.getDate());
        if(date == null) return null;
        try{
            Double.parseDouble(date);
        }catch(NumberFormatException e){ return null; }
        return date;
    }

    /**
     * Extracts a date string from a Date object
     * @param date The Date object to parse
     * @return A String with the parsed date, or null if there is no date information to process.
     */
    public String extractDateStringFromDateObject(Date date){
        if(date == null) return null;
        String raw = date.getFormal();
        if(raw != null){
            return parseFormalDate(raw);
        } else {
            raw = date.getOriginal();
            if(raw != null){
                return parseOriginalDate(raw);
            } else return null;     // There is no date information to process
        }
    }

    /**
     * Parses a formal date, by the Family Search specification. Only gets the year.
     */
    private String parseFormalDate(String raw){

//        JulianDayConverter julianDayConverter = new JulianDayConverter();
        FormalDateConverter converter = new FormalDateConverter();
        DateRange range = converter.convert(raw);
//        System.out.println(range + " : " + raw + " : " + julianDayConverter.gregorianDate(range.getFirstDay()) + " - " + julianDayConverter.gregorianDate(range.getLastDay()));

        boolean bc = false;
        int index = raw.indexOf('+');
        if(index == -1){
            index = raw.indexOf('-');
            if(index == -1) return null;
            else bc = true;
        }
        try{
            String year = raw.substring(index + 1, index + 5);
            index = year.indexOf('/');
            if(index != -1) year = year.substring(0, index);
            if(bc) return "-" + year;
            else return year;
        }catch(IndexOutOfBoundsException e){
            return null;
        }
    }

    /**
     * Parses an informal date. Less precise than parsing a formal date. Only gets the year.
     */
    private String parseOriginalDate(String raw){
        // Process original date
        String[] dateParts = raw.split(" ");
        int year = 0;
        for(String part : dateParts){
            part = part.trim();
            if(part.endsWith(",")){
                part = part.replaceAll(",", "");
            }
            if(part.endsWith(";")){
                part = part.replaceAll(";", "");
            }
            try{
                int number = Integer.parseInt(part);
                if(number > year){
                    year = number;
                }
            }catch(NumberFormatException e){ /* Don't freak out, just a word, not a number */ }
        }
        if(year > MIN_YEAR_CUTOFF) return Integer.toString(year);
        else return null;
    }
}