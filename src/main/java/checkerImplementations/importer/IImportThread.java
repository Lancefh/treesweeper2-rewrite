package checkerImplementations.importer;

import checkerImplementations.importer.organization.Family;
import interfaces.DataImportException;

/**
 * Defines the methods that are unique to the ImportThread
 */
public interface IImportThread {
    public void kill();
    public Family getFamily();
    public String getStatus();
    public void setDetails(String details);
    public int getThrottled();
    public void setThrottled(int throttled);
    public DataImportException getError();
    public void setName(String name);
    public void start();
    public void join() throws InterruptedException;
    public Thread.State getState();
}
