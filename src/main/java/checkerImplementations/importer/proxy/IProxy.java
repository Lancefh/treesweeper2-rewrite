package checkerImplementations.importer.proxy;

import checkerImplementations.importer.IImportThread;
import checkerImplementations.importer.familySearchModel.extensibleData.hypermediaEnabledData.gedcomx.FamilySearchPlatform;
import interfaces.DataImportException;

public interface IProxy {
    public void setThread(IImportThread thread);
    public FamilySearchPlatform getAncestry(String pid, String token, int generations) throws DataImportException;
    public FamilySearchPlatform getSpouses(String pid, String token) throws DataImportException;
    public FamilySearchPlatform getChildren(String pid, String token) throws DataImportException;
    public FamilySearchPlatform getParents(String pid, String token) throws DataImportException;
    public FamilySearchPlatform getDetails(String pid, String token) throws DataImportException;
    public FamilySearchPlatform getCurrentUser(String token) throws DataImportException;
}
