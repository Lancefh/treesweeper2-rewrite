package checkerImplementations.importer.proxy.test;

import checkerImplementations.importer.proxy.IConnection;
import checkerImplementations.importer.proxy.IConnectionFactory;

import java.io.IOException;

/**
 * Testing class for the connection factory. Its methods are designed for some specific test cases,
 * so changing them may break some tests.
 */
public class TestFactory implements IConnectionFactory {
    @Override
    public IConnection openConnection(String url) throws IOException {
        return new TestConnection(url);
    }
}
