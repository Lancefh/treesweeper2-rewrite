package checkerImplementations.importer.proxy.test;

import checkerImplementations.importer.proxy.IConnection;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;

/**
 * This is a class for testing connections. Its methods are designed to facilitate specific test cases,
 * so changing them may break some tests.
 */
public class TestConnection implements IConnection {
    private String url;

    public TestConnection(String url){
        this.url = url;
    }

    @Override
    public void setRequestMethod(String method) throws IOException {

    }

    @Override
    public void setRequestProperty(String key, String property) {

    }

    @Override
    public void setConnectTimeout(int timeout) {

    }

    @Override
    public void setReadTimeout(int timeout) {

    }

    @Override
    public void connect() throws IOException {

    }

    @Override
    public int getResponseCode() throws IOException {
        switch(url){
            case "test1": return HttpURLConnection.HTTP_OK;
            case "test2": return HttpURLConnection.HTTP_NO_CONTENT;
            case "test5": throw new IOException("Haha!");
            default: return 0;
        }
    }

    @Override
    public InputStream getInputStream() throws IOException {
        String message = "This is a message.";
        return new ByteArrayInputStream(message.getBytes());
    }

    @Override
    public InputStream getErrorStream() throws IOException {
        if(url.equals("test3")) return null;
        String message = "This is an error message.";
        return new ByteArrayInputStream(message.getBytes());
    }

    @Override
    public String getHeaderField(String field) {
        return "10";
    }
}
