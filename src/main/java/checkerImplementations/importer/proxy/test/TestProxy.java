package checkerImplementations.importer.proxy.test;

import checkerImplementations.importer.IImportThread;
import checkerImplementations.importer.familySearchModel.ResourceReference;
import checkerImplementations.importer.familySearchModel.extensibleData.Date;
import checkerImplementations.importer.familySearchModel.extensibleData.DisplayProperties;
import checkerImplementations.importer.familySearchModel.extensibleData.PlaceReference;
import checkerImplementations.importer.familySearchModel.extensibleData.hypermediaEnabledData.SourceReference;
import checkerImplementations.importer.familySearchModel.extensibleData.hypermediaEnabledData.User;
import checkerImplementations.importer.familySearchModel.extensibleData.hypermediaEnabledData.conclusion.Fact;
import checkerImplementations.importer.familySearchModel.extensibleData.hypermediaEnabledData.conclusion.Gender;
import checkerImplementations.importer.familySearchModel.extensibleData.hypermediaEnabledData.conclusion.subject.ChildAndParentsRelationship;
import checkerImplementations.importer.familySearchModel.extensibleData.hypermediaEnabledData.conclusion.subject.Person;
import checkerImplementations.importer.familySearchModel.extensibleData.hypermediaEnabledData.conclusion.subject.Relationship;
import checkerImplementations.importer.familySearchModel.extensibleData.hypermediaEnabledData.gedcomx.FamilySearchPlatform;
import checkerImplementations.importer.proxy.IProxy;
import interfaces.DataImportException;

/**
 * Test class for testing family search proxy. Its methods are designed for some specific test cases,
 * so changing them may break some tests.
 */
public class TestProxy implements IProxy {
    @Override
    public void setThread(IImportThread thread) {

    }

    @Override
    public FamilySearchPlatform getAncestry(String pid, String token, int generations) throws DataImportException {
        if(pid.equals("null_ancestry")) return null;
        FamilySearchPlatform ancestry = new FamilySearchPlatform();
        if(pid.equals("pid")){
            Person person = new Person();
            person.setId("pid");
            DisplayProperties display = new DisplayProperties();
            display.setAscendancyNumber("1");
            person.setDisplay(display);

            Person father = new Person();
            father.setId("father");
            display = new DisplayProperties();
            display.setAscendancyNumber("2");
            father.setDisplay(display);

            Person mother = new Person();
            mother.setId("mother");
            display = new DisplayProperties();
            display.setAscendancyNumber("3");
            mother.setDisplay(display);

            Person grandFather = new Person();
            grandFather.setId("grandfather");
            display = new DisplayProperties();
            display.setAscendancyNumber("4");
            grandFather.setDisplay(display);

            Person grandMother = new Person();
            grandMother.setId("grandmother");
            display = new DisplayProperties();
            display.setAscendancyNumber("5");
            grandMother.setDisplay(display);

            ancestry.setPersons(new Person[]{person, father, mother, grandFather, grandMother});
        } else if(pid.equals("null_family")){
            Person person = new Person();
            person.setId("null_family");
            DisplayProperties display = new DisplayProperties();
            display.setAscendancyNumber("1");
            person.setDisplay(display);

            ancestry.setPersons(new Person[]{person});
        } else if(pid.equals("bad_gender")){
            Person person = new Person();
            person.setId("bad_gender");
            DisplayProperties display = new DisplayProperties();
            display.setAscendancyNumber("1");
            person.setDisplay(display);

            ancestry.setPersons(new Person[]{person});
        } else if(pid.equals("bad_generation")){
            Person person = new Person();
            person.setId("bad_generation");
            DisplayProperties display = new DisplayProperties();
            display.setAscendancyNumber("tim");
            person.setDisplay(display);

            ancestry.setPersons(new Person[]{person});
        }
        return ancestry;
    }

    @Override
    public FamilySearchPlatform getSpouses(String pid, String token) throws DataImportException {
        FamilySearchPlatform details = new FamilySearchPlatform();
        Person person = new Person();
        switch(pid){
            case "ancestor":
                person.setId("spouse");
                Gender gender = new Gender();
                gender.setType("http://gedcomx.org/Female");
                person.setGender(gender);
                break;
            case "unknown_father":
            case "known_mother":
            case "unknown_mother":
            case "parents_test":
                return null;
        }
        details.setPersons(new Person[]{person});
        return details;
    }

    @Override
    public FamilySearchPlatform getChildren(String pid, String token) throws DataImportException {
        FamilySearchPlatform children = new FamilySearchPlatform();
        Person person = new Person();
        Gender gender = new Gender();
        switch(pid){
            case "ancestor":
            case "spouse":
                person.setId("child");
                gender.setType("http://gedcomx.org/Female");

                ChildAndParentsRelationship relationship = new ChildAndParentsRelationship();
                ResourceReference childReference = new ResourceReference();
                childReference.setResourceId("child");
                relationship.setChild(childReference);
                ResourceReference parentReference = new ResourceReference();
                parentReference.setResourceId(pid);
                relationship.setFather(parentReference);
                Fact parentType = new Fact();
                parentType.setType("http://gedcomx.org/BiologicalParent");
                relationship.setFatherFacts(new Fact[]{parentType});
                children.setChildAndParentsRelationships(new ChildAndParentsRelationship[]{relationship});
                break;
            case "unknown_father":
                person.setId("child");
                gender.setType("http://gedcomx.org/Female");

                relationship = new ChildAndParentsRelationship();
                ResourceReference father = new ResourceReference();
                father.setResourceId("unknown_father");
                relationship.setFather(father);
                ResourceReference child = new ResourceReference();
                child.setResourceId("child");
                relationship.setChild(child);
                children.setChildAndParentsRelationships(new ChildAndParentsRelationship[]{relationship});
                break;
            case "known_mother":
                person.setId("child");
                gender.setType("http://gedcomx.org/Female");

                relationship = new ChildAndParentsRelationship();
                ResourceReference mother = new ResourceReference();
                mother.setResourceId("known_mother");
                relationship.setMother(mother);
                child = new ResourceReference();
                child.setResourceId("child");
                relationship.setChild(child);
                Fact motherType = new Fact();
                motherType.setType("http://gedcomx.org/AdoptiveParent");
                relationship.setMotherFacts(new Fact[]{motherType});
                children.setChildAndParentsRelationships(new ChildAndParentsRelationship[]{relationship});
                break;
            case "unknown_mother":
                person.setId("child");
                gender.setType("http://gedcomx.org/Female");

                relationship = new ChildAndParentsRelationship();
                mother = new ResourceReference();
                mother.setResourceId("unknown_mother");
                relationship.setMother(mother);
                child = new ResourceReference();
                child.setResourceId("child");
                relationship.setChild(child);
                children.setChildAndParentsRelationships(new ChildAndParentsRelationship[]{relationship});
                break;
            case "parents_test": return null;
        }

        person.setGender(gender);
        children.setPersons(new Person[]{person});



        return children;
    }

    @Override
    public FamilySearchPlatform getParents(String pid, String token) throws DataImportException {
        switch(pid){
            case "parents_test":
                FamilySearchPlatform parents = new FamilySearchPlatform();
                ChildAndParentsRelationship relationship = new ChildAndParentsRelationship();
                ResourceReference father = new ResourceReference();
                father.setResourceId("father");
                relationship.setFather(father);
                ResourceReference mother = new ResourceReference();
                mother.setResourceId("mother");
                relationship.setMother(mother);
                Fact fatherType = new Fact();
                fatherType.setType("Unknown");
                relationship.setFatherFacts(new Fact[]{fatherType});
                Fact motherType = new Fact();
                motherType.setType("Unknown");
                relationship.setMotherFacts(new Fact[]{motherType});
                parents.setChildAndParentsRelationships(new ChildAndParentsRelationship[]{relationship});
                return parents;
        }
        return null;
    }

    @Override
    public FamilySearchPlatform getDetails(String pid, String token) throws DataImportException {
        if(pid.equals("exception")) throw new DataImportException("haha");
        FamilySearchPlatform details = new FamilySearchPlatform();
        Person person = new Person();
        Gender gender = new Gender();
        switch(pid){
            case "ancestor":
                person.setId("ancestor");
                gender.setType("http://gedcomx.org/Male");
                person.setGender(gender);
                break;
            case "spouse":
                person.setId("spouse");
                gender.setType("http://gedcomx.org/Female");
                person.setGender(gender);
                break;
            case "child":
                person.setId("child");
                gender.setType("http://gedcomx.org/Female");
                person.setGender(gender);
                break;
            case "missing":
                return null;
            case "broken":
                throw new DataImportException("haha");
            case "pid":
                person.setId("pid");
                gender.setType("http://gedcomx.org/Female");
                person.setGender(gender);
                person.setDisplay(new DisplayProperties());
                person.getDisplay().setName("Person");
                break;
            case "father":
                person.setId("father");
                gender.setType("http://gedcomx.org/Male");
                person.setGender(gender);
                person.setDisplay(new DisplayProperties());
                person.getDisplay().setName("Father");

                // Add a marriage relationship with a source
                Relationship relationship = new Relationship();
                relationship.setType("http://gedcomx.org/Couple");
                ResourceReference person1 = new ResourceReference();
                person1.setResourceId("father");
                relationship.setPerson1(person1);
                ResourceReference person2 = new ResourceReference();
                person2.setResourceId("mother");
                relationship.setPerson2(person2);

                SourceReference source = new SourceReference();
                source.setDescriptionId("blahblah");
                relationship.setSources(new SourceReference[]{source});

                // Add a fact for the marriage
                Fact marriageFact = new Fact();
                marriageFact.setType("http://gedcomx.org/Marriage");
                Date date = new Date();
                date.setFormal("+1920");
                marriageFact.setDate(date);
                PlaceReference location = new PlaceReference();
                location.setOriginal("Timbuktu");
                marriageFact.setPlace(location);
                relationship.setFacts(new Fact[]{marriageFact});

                details.setRelationships(new Relationship[]{relationship});

                // Add a parent-child relationship with a source
                ChildAndParentsRelationship r = new ChildAndParentsRelationship();
                ResourceReference father = new ResourceReference();
                father.setResourceId("father");
                r.setFather(father);
                ResourceReference child = new ResourceReference();
                child.setResourceId("pid");
                r.setChild(child);
                source = new SourceReference();
                source.setDescriptionId("blahalaa");
                r.setSources(new SourceReference[]{source});
                details.setChildAndParentsRelationships(new ChildAndParentsRelationship[]{r});
                break;
            case "mother":
                person.setId("mother");
                gender.setType("http://gedcomx.org/Female");
                person.setGender(gender);
                person.setDisplay(new DisplayProperties());
                person.getDisplay().setName("Mother");

                // Add essential dates
                Fact birthFact = new Fact();
                birthFact.setType("http://gedcomx.org/Birth");
                date = new Date();
                date.setFormal("+1900");
                birthFact.setDate(date);

                Fact deathFact = new Fact();
                deathFact.setType("http://gedcomx.org/Death");
                date = new Date();
                date.setFormal("+2000");
                deathFact.setDate(date);

                Fact christening = new Fact();
                christening.setType("http://gedcomx.org/Christening");
                date = new Date();
                date.setFormal("+1900");
                christening.setDate(date);

                person.setFacts(new Fact[]{birthFact,deathFact,christening});
                break;
            case "grandfather":
                person.setId("grandfather");
                gender.setType("http://gedcomx.org/Male");
                person.setGender(gender);
                person.setDisplay(new DisplayProperties());
                person.getDisplay().setName("Grandfather");

                // Add a relationship, but not a marriage one
                relationship = new Relationship();
                relationship.setType("unknown!");
                details.setRelationships(new Relationship[]{relationship});
                break;
            case "grandmother":
                person.setId("grandmother");
                gender.setType("http://gedcomx.org/Unknown");
                person.setGender(gender);
                person.setDisplay(new DisplayProperties());
                person.getDisplay().setName("Grandmother");
                break;
            case "bad_gender":
                person.setId("bad_gender");
                gender.setType("Tim");
                person.setGender(gender);
                person.setDisplay(new DisplayProperties());
                person.getDisplay().setName("Bad Gender");
                break;
            case "bad_generation":
                person.setId("bad_generation");
                gender.setType("http://gedcomx.org/Female");
                person.setGender(gender);
                person.setDisplay(new DisplayProperties());
                person.getDisplay().setName("Bad Generation");
                break;
            case "unknown_father":
                person.setId("unknown_father");
                gender.setType("http://gedcomx.org/Male");
                person.setGender(gender);
                person.setDisplay(new DisplayProperties());
                person.getDisplay().setName("Unknown Father");
                break;
            case "known_mother":
                person.setId("known_mother");
                gender.setType("http://gedcomx.org/Female");
                person.setGender(gender);
                person.setDisplay(new DisplayProperties());
                person.getDisplay().setName("Known Mother");
                break;
            case "unknown_mother":
                person.setId("unknown_mother");
                gender.setType("http://gedcomx.org/Female");
                person.setGender(gender);
                person.setDisplay(new DisplayProperties());
                person.getDisplay().setName("Unknown Mother");
                break;
            case "parents_test":
                person.setId("parents_test");
                gender.setType("http://gedcomx.org/Female");
                person.setGender(gender);
                person.setDisplay(new DisplayProperties());
                person.getDisplay().setName("Parents Test");
                break;
            case "test_thread":
                person.setId("test_thread");
                gender.setType("http://gedcomx.org/Female");
                person.setGender(gender);
                person.setDisplay(new DisplayProperties());
                person.getDisplay().setName("Test Thread");
                break;
        }
        person.setGender(gender);
        details.setPersons(new Person[]{person});
        return details;
    }

    @Override
    public FamilySearchPlatform getCurrentUser(String token) throws DataImportException {
        if(token == null) {
            FamilySearchPlatform userPlatform = new FamilySearchPlatform();
            User user = new User();
            user.setPersonId("pid");
            userPlatform.setUsers(new User[]{user});
            return userPlatform;
        } else return null;
    }
}
