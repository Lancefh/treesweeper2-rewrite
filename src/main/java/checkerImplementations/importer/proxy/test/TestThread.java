package checkerImplementations.importer.proxy.test;

import checkerImplementations.importer.IImportThread;
import checkerImplementations.importer.organization.Family;
import checkerImplementations.importer.organization.FamilyTreeNode;
import checkerImplementations.importer.organization.ParentInfo;
import checkerImplementations.importer.proxy.IProxy;
import interfaces.DataImportException;

/**
 * This a class used for testing purposes. Its methods are designed to facilitate specific tests,
 * so changing them may break some tests.
 */
public class TestThread implements IImportThread {
    private String name;
    private Thread thread;
    private IProxy proxy;
    private int generation;
    private String pid;

    public TestThread(String pid, int generation){
        name = null;
        thread = null;
        proxy = new TestProxy();
        this.pid = pid;
        this.generation = generation;
    }

    @Override
    public void kill() {

    }

    @Override
    public Family getFamily() {
        try {
            if(pid.equals("null_family")) return null;
            FamilyTreeNode ancestor = new FamilyTreeNode(proxy.getDetails(pid, null), generation);
            Family family = new Family(ancestor);
            switch (pid) {
                case "pid":
                    family.getAncestor().setAncestor(true);
                    FamilyTreeNode father = new FamilyTreeNode(proxy.getDetails("father", null), generation + 1);
                    family.addParent(father, new ParentInfo("http://gedcomx.org/BiologicalParent", "Father"));
                    FamilyTreeNode mother = new FamilyTreeNode(proxy.getDetails("mother", null), generation + 1);
                    family.addParent(mother, new ParentInfo("http://gedcomx.org/AdoptiveParent", "Mother"));
                    break;
                case "father":
                    family.getAncestor().setAncestor(true);
                    FamilyTreeNode spouse = new FamilyTreeNode(proxy.getDetails("mother", null), generation);
                    family.addSpouse(spouse);
                    FamilyTreeNode child = new FamilyTreeNode(proxy.getDetails("pid",null), generation - 1);
                    family.addChild(child, new ParentInfo("http://gedcomx.org/BiologicalParent", "Father"));
                    father = new FamilyTreeNode(proxy.getDetails("grandfather", null), generation + 1);
                    family.addParent(father, new ParentInfo("Unknown", "Father"));
                    mother = new FamilyTreeNode(proxy.getDetails("grandmother", null), generation + 1);
                    family.addParent(mother, new ParentInfo("http://gedcomx.org/Guardian", "Mother"));
                    break;
                case "mother":
                    family.getAncestor().setAncestor(true);
                    spouse = new FamilyTreeNode(proxy.getDetails("father", null), generation);
                    family.addSpouse(spouse);
                    child = new FamilyTreeNode(proxy.getDetails("pid",null), generation - 1);
                    family.addChild(child, new ParentInfo("http://gedcomx.org/AdoptiveParent", "Mother"));
                    break;
                case "grandfather":
                    family.getAncestor().setAncestor(true);
                    spouse = new FamilyTreeNode(proxy.getDetails("grandmother", null), generation);
                    family.addSpouse(spouse);
                    child = new FamilyTreeNode(proxy.getDetails("father", null), generation - 1);
                    family.addChild(child, new ParentInfo("Unknown", "Father"));
                    break;
                case "grandmother":
                    family.getAncestor().setAncestor(true);
                    spouse = new FamilyTreeNode(proxy.getDetails("grandfather", null), generation);
                    family.addSpouse(spouse);
                    child = new FamilyTreeNode(proxy.getDetails("father", null), generation - 1);
                    family.addChild(child, new ParentInfo("http://gedcomx.org/Guardian", "Mother"));
                    break;
                case "bad_gender":
                case "bad_generation":
                    break;
                default:
                    return null;
            }
            return family;
        }catch(DataImportException e){
            return null;
        }
    }

    @Override
    public String getStatus() {
        return null;
    }

    @Override
    public void setDetails(String details) {

    }

    @Override
    public int getThrottled() {
        return 0;
    }

    @Override
    public void setThrottled(int throttled) {

    }

    @Override
    public DataImportException getError() {
        return null;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void start() {

    }

    @Override
    public void join() throws InterruptedException {

    }

    @Override
    public Thread.State getState() {
        return thread != null ? thread.getState() : null;
    }
}
