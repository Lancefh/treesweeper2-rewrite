package checkerImplementations.importer.proxy.test;

import checkerImplementations.importer.proxy.IClient;
import checkerImplementations.importer.proxy.Response;
import interfaces.DataImportException;

import java.net.SocketTimeoutException;

/**
 * This is a class used for testing purposes. Its methods are designed to facilitate specific test cases,
 * so modifying them may break tests.
 */
public class TestClient implements IClient {
    private boolean throttled;

    public TestClient(){
        throttled = false;
    }

    @Override
    public Response get(String url, String token) throws DataImportException, SocketTimeoutException {
        Response response;
        switch(url){
            case "https://api.familysearch.org/platform/tree/ancestry?access_token=token&generations=7&person=test1":
                if(throttled){
                    response = new Response("{}");
                    return response;
                }else {
                    throttled = true;
                    response = new Response(null);
                    response.setRetry(1);
                    return response;
                }
            case "https://api.familysearch.org/platform/tree/persons/test2/spouses?access_token=token":
                throw new SocketTimeoutException("test timed out");
            case "https://api.familysearch.org/platform/tree/persons/test3/children?access_token=token":
            case "https://api.familysearch.org/platform/tree/persons/test4?access_token=token":
            case "https://api.familysearch.org/platform/users/current?access_token=token":
            case "https://api.familysearch.org/platform/tree/persons/test3/parents?access_token=token":
                return new Response("{}");
            case "https://api.familysearch.org/platform/users/current?access_token=badserialize":
                return new Response("wazzup");
            case "https://api.familysearch.org/platform/tree/ancestry?access_token=token&generations=7&person=test5":
                throw new DataImportException("tada");
            default:
//                System.out.println(url);
                return null;
        }
    }
}
