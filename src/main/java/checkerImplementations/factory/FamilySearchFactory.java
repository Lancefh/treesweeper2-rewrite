package checkerImplementations.factory;

import checkerImplementations.importer.FamilySearchImporter;
import interfaces.*;
import server.IUpdater;
import server.ImportOptions;

import java.io.InputStream;

/**
 * This is the plugin factory for TreeSweeper. It pretty much only delegates since this project
 * has many calculators and handlers.
 */
public class FamilySearchFactory implements IPluginFactory {
    private HandlerFactory handlerFactory;
    private CalculatorFactory calculatorFactory;
    private ImportOptions options;
    private IUpdater updater;

    public FamilySearchFactory(ImportOptions options, IUpdater updater){
        handlerFactory = new HandlerFactory(updater);
        calculatorFactory = new CalculatorFactory();
        this.options = options;
        this.updater = updater;
    }

    @Override
    public IHandler getHandler(String s) throws PluginNotFoundException {
        return handlerFactory.getHandler(s);
    }

    @Override
    public IProbabilityCalculator getCalculator(String s) throws PluginNotFoundException {
        return calculatorFactory.getCalculator(s);
    }

    @Override
    public IDataImporter getDataImporter() throws PluginNotFoundException {
        return new FamilySearchImporter(options,updater);
    }

    @Override
    public InputStream getDistribution(String s) throws PluginNotFoundException {
        return calculatorFactory.getDistribution(s);
    }
}
