package checkerImplementations.factory;

import checkerImplementations.calculators.distributionBased.*;
import checkerImplementations.calculators.notDistributionBased.DeathProbabilityCalculator;
import checkerImplementations.calculators.notDistributionBased.SpouseAgeGapCalculator;
import interfaces.IProbabilityCalculator;
import interfaces.PluginNotFoundException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * Sub-factory that handles probability calculators and distributions. This class will need to be updated
 * if new distributions become available or project structure changes.
 */
public class CalculatorFactory {
    private static final String DISTRIBUTION_DIRECTORY = "distributions/";

    /**
     * Gets a probability calculator based on the id it gets.
     * @param s The identifier of the probability calculator.
     * @return The identified Probability Calculator.
     * @throws PluginNotFoundException If an unrecognized probability calculator is asked for, or
     * if something goes wrong while instantiating the desired calculator.
     */
    public IProbabilityCalculator getCalculator(String s) throws PluginNotFoundException {
        switch(s){
            case "death": return new DeathProbabilityCalculator();
            case "ageAtBirth": return new AgeAtBirthOfChild();
            case "ageAtMarriage": return new AgeAtMarriage();
            case "marriageChild": return new MarriageToChild();
            case "christening": return new BirthToChristening();
            case "spouseAgeGap": return new SpouseAgeGapCalculator();
            default: throw new PluginNotFoundException("Unrecognized calculator: " + s);
        }
    }

    /**
     * Maps distribution identifiers to actual files.
     * @param id The identifier of the distribution.
     * @return The path to the identified distribution.
     * @throws PluginNotFoundException If an unrecognized distribution is passed in.
     */
    private String getPathFromId(String id) throws PluginNotFoundException{
        switch(id){
            case "christening": return DISTRIBUTION_DIRECTORY + "BirthToChristening.csv";
            case "fatherFirstChild": return DISTRIBUTION_DIRECTORY + "FatherAgeAtFirstChild.csv";
            case "fatherLastChild": return DISTRIBUTION_DIRECTORY + "FatherAgeAtLastChild.csv";
            case "fatherMarriage": return DISTRIBUTION_DIRECTORY + "FatherAgeAtMarriage.csv";
            case "husbandLifespan": return DISTRIBUTION_DIRECTORY + "HusbandLifespan.csv";
            case "marriageFirstChild": return DISTRIBUTION_DIRECTORY + "MarriageToFirstChild.csv";
            case "marriageLastChild": return DISTRIBUTION_DIRECTORY + "MarriageToLastChild.csv";
            case "motherFirstChild": return DISTRIBUTION_DIRECTORY + "MotherAgeAtFirstChild.csv";
            case "motherLastChild": return DISTRIBUTION_DIRECTORY + "MotherAgeAtLastChild.csv";
            case "motherMarriage": return DISTRIBUTION_DIRECTORY + "MotherAgeAtMarriage.csv";
            case "wifeLifespan": return DISTRIBUTION_DIRECTORY + "WifeLifespan.csv";
            case "badtestbrokentest": return "";
            default: throw new PluginNotFoundException("Unrecognized distribution: " + id);
        }
    }

    /**
     * Gets a distribution based on the id it gets.
     * @param s The identifier of the distribution.
     * @return An InputStream with the contents of the identified distribution.
     * @throws PluginNotFoundException If an unrecognized distribution is given or the distribution file
     * can't be found.
     */
    public InputStream getDistribution(String s) throws PluginNotFoundException {
        String path = getPathFromId(s);
        File file = new File(path);
        try {
            return new FileInputStream(file);
        }catch(FileNotFoundException e){
            throw new PluginNotFoundException(e.getMessage());
        }
    }
}
