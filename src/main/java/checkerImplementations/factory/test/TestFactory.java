package checkerImplementations.factory.test;

import interfaces.*;
import interfaces.defaults.DefaultCalculator;
import interfaces.defaults.DefaultHandler;
import server.ImportOptions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * This class is used by the dependency injector for testing ImporterSocket;
 * Its methods reference specific tests, and may break tests when modified.
 */
public class TestFactory implements IPluginFactory {
    private ImportOptions options;

    public TestFactory(ImportOptions options){
        this.options = options;
    }

    @Override
    public IHandler getHandler(String s) throws PluginNotFoundException {
        return new DefaultHandler();
    }

    @Override
    public IProbabilityCalculator getCalculator(String s) throws PluginNotFoundException {
        if(options.getPid().equals("bad_plugin")) throw new PluginNotFoundException("bad plugin");
        return new DefaultCalculator();
    }

    @Override
    public IDataImporter getDataImporter() throws PluginNotFoundException {
        getDistribution("blah");    // This is only here for line coverage
        getHandler(null);           // This is only here for line coverage
        return new TestImporter(options);
    }

    @Override
    public InputStream getDistribution(String s) throws PluginNotFoundException {
        try{
            if(s.equals("blah")) return new FileInputStream("monkey");  // This is only here for line coverage
            if(options.getPid().equals("bad_distribution")) return new FileInputStream(new File("src/test_files/bad_files/unrecognized_link.txt"));
            return new FileInputStream(new File("distributions/BirthToChristening.csv"));
        }catch(FileNotFoundException e){
            return null;
        }
    }
}
