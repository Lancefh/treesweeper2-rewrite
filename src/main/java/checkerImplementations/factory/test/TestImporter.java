package checkerImplementations.factory.test;

import interfaces.DataImportException;
import interfaces.IDataImporter;
import osax.model.OSAXModel;
import osax.object.lexical.INTEGER;
import osax.object.lexical.STRING;
import osax.object.nonlexical.NonLexicalObject;
import osax.relationSet.Tuple;
import osax.relationSet.TupleMismatchException;
import server.ImportOptions;

/**
 * This class is used for testing purposes. Its methods are used by specific tests,
 * and changing them may break tests.
 */
public class TestImporter implements IDataImporter {
    private ImportOptions options;

    public TestImporter(ImportOptions options){
        this.options = options;
    }

    @Override
    public void populate(OSAXModel osaxModel) throws DataImportException {
        try {
            if (options.getPid().equals("broken_import")) {
                throw new DataImportException("broken import");
            }
            if (options.getPid().equals("missing_ancestry")) {
                throw new DataImportException("Ancestry for blah came back null.");
            }
            if (options.getPid().equals("null_error")) {
                throw new NullPointerException("haha");
            }
            if (options.getPid().equals("constraint_exception")) {
                NonLexicalObject person = new NonLexicalObject();
                osaxModel.getNonLexicalObjectSet("Person").addObject(person);

                INTEGER birthdate = new INTEGER("1900");
                osaxModel.getLexicalObjectSet("Birthdate").addObject(birthdate);

                STRING christeningdate = new STRING("1901");
                osaxModel.getLexicalObjectSet("Christeningdate").addObject(christeningdate);

                osaxModel.getRelationshipSet("Person has Birthdate").insert(new Tuple(person, birthdate));
                osaxModel.getRelationshipSet("Person has Christeningdate").insert(new Tuple(person, christeningdate));
            }
        }catch(Exception e){
            if(e instanceof DataImportException) throw (DataImportException)e;
            if(e instanceof NullPointerException) throw (NullPointerException)e;
        }
    }
}
