package checkerImplementations.factory;

import checkerImplementations.handlers.birthAge.AgeAtFirstChildHandler;
import checkerImplementations.handlers.birthAge.AgeAtLastChildHandler;
import checkerImplementations.handlers.birthAge.ParentBornBeforeChildHandler;
import checkerImplementations.handlers.birthAge.ParentDeathBeforeChildHandler;
import checkerImplementations.handlers.christening.AgeAtChristeningHandler;
import checkerImplementations.handlers.christening.BornAfterChristeningHandler;
import checkerImplementations.handlers.christening.DiedBeforeChristeningHandler;
import checkerImplementations.handlers.lifespan.DeathBeforeBirthHandler;
import checkerImplementations.handlers.lifespan.LifespanHandler;
import checkerImplementations.handlers.marriage.*;
import checkerImplementations.handlers.research.*;
import interfaces.IHandler;
import interfaces.PluginNotFoundException;
import server.IUpdater;

/**
 * This is a sub-factory that gets handlers.
 */
public class HandlerFactory {
    private IUpdater updater;

    public HandlerFactory(IUpdater updater){
        this.updater = updater;
    }

    public IHandler getHandler(String s) throws PluginNotFoundException {
        switch(s){
            case "christening": return new AgeAtChristeningHandler(updater);
            case "christeningBirth": return new BornAfterChristeningHandler(updater);
            case "christeningDeath": return new DiedBeforeChristeningHandler(updater);
            case "negativeAge": return new DeathBeforeBirthHandler(updater);
            case "lifespan": return new LifespanHandler(updater);
            case "ageAtChild": return new AgeAtFirstChildHandler(updater);
            case "ageAtLastChild": return new AgeAtLastChildHandler(updater);
            case "bornAtBirth": return new ParentBornBeforeChildHandler(updater);
            case "deadAtBirth": return new ParentDeathBeforeChildHandler(updater);
            case "ageAtMarriage": return new AgeAtMarriageHandler(updater);
            case "bornAtMarriage": return new BornAfterMarriageHandler(updater);
            case "marriageToChild": return new MarriageToFirstChildHandler(updater);
            case "marriageToLastChild": return new MarriageToLastChildHandler(updater);
            case "sources": return new SourcesHandler(updater);
            case "merge": return new BadMergeHandler(updater);
            case "ownAncestor": return new OwnAncestorHandler(updater);
            case "missingChildren": return new MissingChildHandler(updater);
            case "noChildren": return new PersonWithoutChildrenHandler(updater);
            case "aliveAtMarriage": return new AliveAtMarriageHandler(updater);
            case "missingEarlyChildren": return new MissingChildrenAtStartHandler(updater);
            case "missingLateChildren": return new MissingChildrenAtEndHandler(updater);
            case "missingMiddleChildren": return new MissingChildrenInMiddleHandler(updater);
            case "missingSpouse": return new MissingSpouseHandler(updater);
            case "unknownGender": return new UnknownGenderHandler(updater);
            case "excessiveParents": return new ExcessiveParentsHandler(updater);
            case "missingVitals": return new MissingVitalDatesHandler(updater);
            case "spouseAgeGap": return new AgeGapBetweenSpousesHandler(updater);
            case "duplicateChildren": return new DuplicateChildrenHandler(updater);
            case "duplicateParents": return new DuplicateParentHandler(updater);
            case "duplicateSpouses": return new DuplicateSpouseHandler(updater);
            default: throw new PluginNotFoundException("Unrecognized handler: " + s);
        }
    }
}
