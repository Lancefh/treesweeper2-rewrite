package checkerImplementations.calculators.distributionBased;

import distribution.Distribution;
import osax.object.ModelObject;
import osax.object.lexical.DOUBLE;
import osax.relationSet.Tuple;

/**
 * Calculates the probability of someone being christened after a number of years.
 */
public class BirthToChristening extends DistributionCalculator {

    /**
     * The age object should be at index 1.
     * @param tuple The tuple to extract a value from.
     * @return The object with the age at which someone was christened.
     */
    @Override
    protected ModelObject getValue(Tuple tuple) {
        return (ModelObject)tuple.get(1);
    }

    @Override
    protected double getProbability(Distribution distribution, ModelObject value) {
        // Get age from model object
        float age = (float)((DOUBLE)value).getValue();

        // If the age is negative, do nothing; another constraint will handle it and we
        // don't want double output.
        if(age < 0) return 1.0;

        // Calculate probability of being too young
        float probability = distribution.calculateProbabilityUpTo(age);

        // Calculate probability of being too old
        float temp = distribution.calculateProbabilityGreaterThan(age);
        if(temp < probability) probability = temp;

        return probability;
    }
}
