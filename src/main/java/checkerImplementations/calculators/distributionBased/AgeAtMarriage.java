package checkerImplementations.calculators.distributionBased;

import distribution.Distribution;
import osax.object.ModelObject;
import osax.object.lexical.DOUBLE;
import osax.object.lexical.NULL;
import osax.relationSet.Tuple;

/**
 * Calculates the probability of getting married at a given age.
 */
public class AgeAtMarriage extends DistributionCalculator {

    /**
     * Age should be at index 2.
     * @param tuple The tuple to extract a value from.
     * @return The age object from the tuple.
     */
    @Override
    protected ModelObject getValue(Tuple tuple) {
        return (ModelObject)tuple.get(1);
    }

    @Override
    protected double getProbability(Distribution distribution, ModelObject value) {
        // Get age from model object
        float age = (float)((DOUBLE)value).getValue();
        if(age < 0) return 1.0; // Negative ages are checked by a different constraint

        // Calculate probability of being too young
        float probability = distribution.calculateProbabilityUpTo(age);

        // Calculate probability of being too old
        float temp = distribution.calculateProbabilityGreaterThan(age);
        if(temp < probability) probability = temp;

        return probability;
    }
}
