package checkerImplementations.calculators.distributionBased;

import distribution.Distribution;
import interfaces.IProbabilityCalculator;
import osax.object.ModelObject;
import osax.object.lexical.NULL;
import osax.relationSet.Tuple;

/**
 * This is the base class for all probability calculators that use distributions to calculate probabilities.
 */
public abstract class DistributionCalculator implements IProbabilityCalculator {
    private Distribution distribution;

    public DistributionCalculator(){
        distribution = null;
    }

    /**
     * Gets the desired value from a tuple.
     * @param tuple The tuple to extract a value from.
     * @return The target value from the tuple.
     */
    protected abstract ModelObject getValue(Tuple tuple);

    /**
     * Calculates probability using the distribution, using whatever method is deemed necessary.
     * @param distribution The distribution to use.
     * @param value The value to use in the distribution.
     * @return The probability of the given value.
     */
    protected abstract double getProbability(Distribution distribution, ModelObject value);

    @Override
    public double calculateProbability(Tuple tuple) {
        // If no distribution has been given, behaves like the default probability calculator
        if(distribution == null) return 0;

        // Get target value from tuple
        ModelObject value = getValue(tuple);
        if(value instanceof NULL) return -1; // Can't calculate probability of NULL values.

        // Calculate probability
        return getProbability(distribution, value);
    }

    @Override
    public void useDistribution(Distribution distribution) {
        this.distribution = distribution;
    }

    @Override
    public void useLowerBound(double v) {
        if(this.distribution != null) this.distribution.applyLowerBound((float)v);
    }

    @Override
    public void useUpperBound(double v) {
        if(this.distribution != null) this.distribution.applyUpperBound((float)v);
    }
}
