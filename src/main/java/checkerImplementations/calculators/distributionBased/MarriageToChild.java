package checkerImplementations.calculators.distributionBased;

import distribution.Distribution;
import osax.object.ModelObject;
import osax.object.lexical.DOUBLE;
import osax.relationSet.Tuple;

/**
 * Calculates the probability of having a child after a given number of years.
 */
public class MarriageToChild extends DistributionCalculator {
    /**
     * The years index should be 3.
     * @param tuple The tuple to extract a value from.
     * @return The number of years the couple have been married until their first child.
     */
    @Override
    protected ModelObject getValue(Tuple tuple) {
        return (ModelObject)tuple.get(3);
    }

    @Override
    protected double getProbability(Distribution distribution, ModelObject value) {
        float years = (float)((DOUBLE)value).getValue();

        // Calculate probability of being too few years
        float probability = distribution.calculateProbabilityUpTo(years);

        // Calculate probability of being too many years
        float temp = distribution.calculateProbabilityGreaterThan(years);
        if(temp < probability) probability = temp;

        return probability;
    }
}
