package checkerImplementations.calculators.distributionBased;

import distribution.Distribution;
import osax.object.ModelObject;
import osax.object.lexical.DOUBLE;
import osax.relationSet.Tuple;

/**
 * Calculates the probability of dying at a particular age.
 */
public class LifespanCalculator extends DistributionCalculator {
    @Override
    protected ModelObject getValue(Tuple tuple) {
        return (ModelObject) tuple.get(1);
    }

    @Override
    protected double getProbability(Distribution distribution, ModelObject value) {
        // Get age from model object
        float age = (float)((DOUBLE)value).getValue();

        // Calculate probability of being too young
        float probability = distribution.calculateProbabilityUpTo(age);

        // Calculate probability of being too old
        float temp = distribution.calculateProbabilityGreaterThan(age);
        if(temp < probability) probability = temp;

        return probability;
    }
}
