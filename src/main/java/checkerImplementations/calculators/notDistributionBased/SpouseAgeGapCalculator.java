package checkerImplementations.calculators.notDistributionBased;

import distribution.Distribution;
import interfaces.IProbabilityCalculator;
import osax.object.ModelObject;
import osax.object.lexical.DOUBLE;
import osax.object.lexical.NULL;
import osax.relationSet.Tuple;

/**
 * Calculates the probability of the difference of age between two spouses
 */
public class SpouseAgeGapCalculator implements IProbabilityCalculator {
    @Override
    public double calculateProbability(Tuple tuple) {
        // Get the age difference
        ModelObject gap = (ModelObject)tuple.get(2);
        if(gap instanceof NULL) return 1.0;

        DOUBLE value = (DOUBLE)gap;
        double val = value.getValue();

        // If val is negative, make it positive
        if(val < 0) val *= -1;

        // If val is greater than or equal to 100, return 0.0; else, return 1.0; no distribution data at this time
        if(val < 100) return 1.0;
        else return 0.0;
    }

    @Override
    public void useDistribution(Distribution distribution) {
        // No need to do anything
    }

    @Override
    public void useLowerBound(double v) {
        // No need to do anything
    }

    @Override
    public void useUpperBound(double v) {
        // No need to do anything
    }
}
