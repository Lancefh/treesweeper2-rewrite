package checkerImplementations.calculators.notDistributionBased;

import distribution.Distribution;
import interfaces.IProbabilityCalculator;
import osax.object.ModelObject;
import osax.object.lexical.DOUBLE;
import osax.object.lexical.NULL;
import osax.relationSet.Tuple;


/**
 * Calculates the probability of death at a given age. Depends on age being the second value of the tuple.
 *
 * Uses data from the life table found at http://www.nber.org/papers/h0059.pdf,
 * accessed on 30 May 2018, which gives age charts for 1850 based on their own research.
 * The upper end of the scale is adjusted to account for record-holding ages; we acknowledge
 * that it is possible to live past 114 (but it is a very small portion of the population that does), while nobody
 * is recorded to have lived past 122.
 */
public class DeathProbabilityCalculator implements IProbabilityCalculator {
    private double lowerBound;
    private double upperBound;

    public DeathProbabilityCalculator(){
        lowerBound = Integer.MIN_VALUE;
        upperBound = Integer.MAX_VALUE;
    }

    @Override
    public double calculateProbability(Tuple tuple) {
        ModelObject m = (ModelObject)tuple.get(1);
        if(m instanceof NULL) return 1.0;
        DOUBLE value = (DOUBLE)m;
        double age = value.getValue();

        if(age > upperBound) return 0;
        if(age < lowerBound) return 0;
        if(age < 0) return 1.0; // Another constraint handles negative ages
        else if(age == 0) return 0.228727;
        else if(age <= 1) return 0.055301;
        else if(age <= 2) return 0.025287;
        else if(age <= 3) return 0.016207;
        else if(age <= 4) return 0.012331;
        else if(age <= 9) return 0.033241;
        else if(age <= 14) return 0.020820;
        else if(age <= 19) return 0.032862;
        else if(age <= 24) return 0.046985;
        else if(age <= 29) return 0.052057;
        else if(age <= 34) return 0.057127;
        else if(age <= 39) return 0.062158;
        else if(age <= 44) return 0.068531;
        else if(age <= 49) return 0.079687;
        else if(age <= 54) return 0.097841;
        else if(age <= 59) return 0.129621;
        else if(age <= 64) return 0.166283;
        else if(age <= 69) return 0.225295;
        else if(age <= 74) return 0.300630;
        else if(age <= 79) return 0.407498;
        else if(age <= 113) return 1.0;     // Normal very old ages
        else if(age <= 122) return 0.001;   // Extreme old age, very few people live to this age.
        else return 0.0;                    // Nobody has been recorded and verified to live past 122.

    }

    @Override
    public void useDistribution(Distribution distribution) {
        // This calculator has its distribution built in, so it won't use a given one.
    }

    @Override
    public void useLowerBound(double v) {
        lowerBound = v;
    }

    @Override
    public void useUpperBound(double v) {
        upperBound = v;
    }
}
