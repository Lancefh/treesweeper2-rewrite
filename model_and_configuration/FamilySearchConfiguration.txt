/-
This is the configuration file for FamilySearchModel.txt.
-/

GENERAL CONSTRAINTS
    CONSTRAINT "Person was christened after they were born"
        CALCULATOR
            ID: DEFAULT
        HANDLER
            ID: "christeningBirth"
    CONSTRAINT "Person was christened before they died"
        CALCULATOR
            ID: DEFAULT
        HANDLER
            ID: "christeningDeath"
    CONSTRAINT "Person was christened at a reasonable age"
        CALCULATOR
            ID: "christening"
            DISTRIBUTION: "christening"
        HANDLER
            ID: "christening"
            UNLIKELY: 0.01
    CONSTRAINT "Person died after they were born"
        CALCULATOR
            ID: DEFAULT
        HANDLER
            ID: "negativeAge"
    CONSTRAINT "Woman has a reasonable lifespan"
        CALCULATOR
            ID: "death"
            -- We don't use this distribution right now because it is only accurate for wives; very inaccurate for children.
            --DISTRIBUTION: "wifeLifespan"
        HANDLER
            ID: "lifespan"
            UNLIKELY: 0.01
    CONSTRAINT "Man has a reasonable lifespan"
        CALCULATOR
            ID: "death"
            -- We don't use this distribution right now because it is only accurate for husbands; very inaccurate for children.
            --DISTRIBUTION: "husbandLifespan"
        HANDLER
            ID: "lifespan"
            UNLIKELY: 0.01
    CONSTRAINT "Father has realistic age at birth of first child"
        CALCULATOR
            ID: "ageAtBirth"
            DISTRIBUTION: "fatherFirstChild"
        HANDLER
            ID: "ageAtChild"
            UNLIKELY: 0.01
    CONSTRAINT "Father has realistic age at birth of last child"
        CALCULATOR
            ID: "ageAtBirth"
            DISTRIBUTION: "fatherLastChild"
        HANDLER
            ID: "ageAtLastChild"
            UNLIKELY: 0.01
    CONSTRAINT "Mother has realistic age at birth of first child"
        CALCULATOR
            ID: "ageAtBirth"
            DISTRIBUTION: "motherFirstChild"
        HANDLER
            ID: "ageAtChild"
            UNLIKELY: 0.01
    CONSTRAINT "Mother has realistic age at birth of last child"
        CALCULATOR
            ID: "ageAtBirth"
            DISTRIBUTION: "motherLastChild"
        HANDLER
            ID: "ageAtLastChild"
            UNLIKELY: 0.01
    CONSTRAINT "Parent is born before their children"
        CALCULATOR
            ID: DEFAULT
        HANDLER
            ID: "bornAtBirth"
    CONSTRAINT "Parent dies after children are born"
        CALCULATOR
            ID: DEFAULT
        HANDLER
            ID: "deadAtBirth"
    CONSTRAINT "Husband has realistic age at marriage"
        CALCULATOR
            ID: "ageAtMarriage"
            DISTRIBUTION: "fatherMarriage"
        HANDLER
            ID: "ageAtMarriage"
            UNLIKELY: 0.01
    CONSTRAINT "Wife has realistic age at marriage"
        CALCULATOR
            ID: "ageAtMarriage"
            DISTRIBUTION: "motherMarriage"
        HANDLER
            ID: "ageAtMarriage"
            UNLIKELY: 0.01
    CONSTRAINT "Person was alive at marriage"
        CALCULATOR
            ID: DEFAULT
        HANDLER
            ID: "aliveAtMarriage"
    CONSTRAINT "Person was born at marriage"
        CALCULATOR
            ID: DEFAULT
        HANDLER
            ID: "bornAtMarriage"
    CONSTRAINT "Couple has been married a reasonable amount of time before first child"
        CALCULATOR
            ID: "marriageChild"
            DISTRIBUTION: "marriageFirstChild"
        HANDLER
            ID: "marriageToChild"
            UNLIKELY: 0.01
    CONSTRAINT "Couple has been married a reasonable amount of time before last child"
        CALCULATOR
            ID: "marriageChild"
            DISTRIBUTION: "marriageLastChild"
        HANDLER
            ID: "marriageToLastChild"
            UNLIKELY: 0.01
    CONSTRAINT "Person has sources"
        CALCULATOR
            ID: DEFAULT
        HANDLER
            ID: "sources"
    DISABLED CONSTRAINT "Person is not badly merged"
        CALCULATOR
            ID: DEFAULT
        HANDLER
            ID: "merge"
    CONSTRAINT "Person is not their own ancestor"
        CALCULATOR
            ID: DEFAULT
        HANDLER
            ID: "ownAncestor"
    DISABLED CONSTRAINT "Couples are not missing children"
        CALCULATOR
            ID: DEFAULT
        HANDLER
            ID: "missingChildren"
    DISABLED CONSTRAINT "Person is not missing children"
        CALCULATOR
            ID: DEFAULT
        HANDLER
            ID: "noChildren"
    CONSTRAINT "Couple is not missing children at beginning of their marriage"
        CALCULATOR
            ID: DEFAULT
        HANDLER
            ID: "missingEarlyChildren"
    CONSTRAINT "Couple is not missing children at the end of mothers child bearing years"
        CALCULATOR
            ID: DEFAULT
        HANDLER
            ID: "missingLateChildren"
    CONSTRAINT "Couple is not missing middle children"
        CALCULATOR
            ID: DEFAULT
        HANDLER
            ID: "missingMiddleChildren"
    CONSTRAINT "Person is not missing a spouse"
        CALCULATOR
            ID: DEFAULT
        HANDLER
            ID: "missingSpouse"
    CONSTRAINT "Person has a known gender"
        CALCULATOR
            ID: DEFAULT
        HANDLER
            ID: "unknownGender"
    CONSTRAINT "Person does not have too many parents"
        CALCULATOR
            ID: DEFAULT
        HANDLER
            ID: "excessiveParents"
    CONSTRAINT "Person has vital dates"
        CALCULATOR
            ID: DEFAULT
        HANDLER
            ID: "missingVitals"
    CONSTRAINT "Age gap between spouses is not too large"
        CALCULATOR
            ID: "spouseAgeGap"
        HANDLER
            ID: "spouseAgeGap"
    CONSTRAINT "Parent has no duplicate children"
        CALCULATOR
            ID: DEFAULT
        HANDLER
            ID: "duplicateChildren"
    CONSTRAINT "Child has no duplicate parents"
        CALCULATOR
            ID: DEFAULT
        HANDLER
            ID: "duplicateParents"
    CONSTRAINT "Person has no duplicate spouses"
        CALCULATOR
            ID: DEFAULT
        HANDLER
            ID: "duplicateSpouses"