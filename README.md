# Tree Sweeper
This is the new version of TreeSweeper, using the upgrades made to OSA-X and the new design
for Constraint Checker. It is a server that downloads information from family search, uses it to populate
an OSA-X Model, then looks for errors in the data.

It also serves a webapp to assist users in gathering and understanding this data.

## Getting Started
When you're just getting started working on Tree Sweeper, follow the following steps to get it up and running
on your system. These instructions assume you are using a Linux environment. You will need to have recent versions
of node, npm, and the angular cli all on your system.

1. Clone the project to your local system.
2. Navigate to the treesweeper2-front-end/ directory.
3. Run `npm install` to download dependencies.
4. Run `ng build` to build the front end.
5. Navigate back to the root of the project.
6. Run `./run.sh run 8080`.

The server should now be running on localhost. Verify this by going to localhost:8080; you should see the login screen for Tree Sweeper.

## Pull to the live server
1. ssh into the lab server with `ssh -i <your private key file> ubuntu@fhtl.byu.edu`
2. navigate to the treesweeper2 directory and run the following commands:
3. `git pull`
4. navigate to the treesweeper2-front-end directory
5. `npm install`
6. `ng build --prod`
7. navigate back to root of treesweeper2
8. `netstat -natp | grep 8080`
9. `kill <pid>` with the process id from step 4.
10. `nohup ./run.sh run 8080 &`

## Running the Server

In the root of the project is a bash script that starts the server.  Simply run the command:
`./run.sh run 8080`

8080 is the default port, although it can be changed by putting a different number in.

## Building the Front End

Navigate to the directory treesweeper2-front-end; this is an angular cli project. To build it, run:
`ng build`

To make a production build, run:
`ng build --prod`